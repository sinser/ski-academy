<?php
    $user_id = $this->session->userdata('user_id');
	 $current_page = $this->session->userdata('current_page');
	
    if($user_id == ""){
      header('Location: '.site_url());
    }

  ?>
<!DOCTYPE html>
<html lang="ru-RU">
  <head>
    <title>Главная страница
    </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="<?php echo site_url(); ?>resources/styles/bootstrap-grid.min.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>resources/styles/slick.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>resources/styles/jquery.fancybox.min.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>resources/styles/style.css" rel="stylesheet">
	<link href="<?php echo site_url(); ?>resources/styles/mobile.css" rel="stylesheet">
	<link href="<?php echo site_url(); ?>resources/styles/my-styles.css" rel="stylesheet">
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo site_url(); ?>resources/fav.png">
	<script type="text/javascript" src="<?php echo site_url(); ?>timer/counter/js/jquery-1.7.2.min.js"></script>
<!--script type="text/javascript" src="<?php echo site_url(); ?>timer/counter/js/script.js"></script-->	
<link rel="stylesheet" href="<?php echo site_url(); ?>timer/counter/style.css">


	<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
  <input type="hidden" id="URL" value="<?php echo site_url(); ?>">
 
    <div class="wrap-page  custom-styles">
      <div class="main">
        <header class="header">
          <div class="container">
            <div class="header-row"><a class="logo" href="<?php echo site_url()?>"><img class="img-fluid" src="<?php echo site_url()?>resources/images/logotype.png" alt="logo"></a>
                      <ul class="navi">
                        <li class="navi-item <?php if($current_page == "home"){ echo "active"; } ?>"><a class="navi-link" href="<?php echo site_url()?>">Главная  </a></li>
                        <li class="navi-item <?php if($current_page == "mycab"){ echo "active"; } ?>"><a class="navi-link" href="<?php echo site_url()?>mycab">Личный кабинет</a></li>
						<?php
						
							$this->db->from("countries");
							$this->db->order_by("priority", "asc");
							$data['countries'] = $this->db->get()->result_array();
							foreach ($data['countries'] as $newCountries):
								$this->db->from("regions");
								$this->db->order_by("priority", "asc");
								$this->db->where("country_id",  $newCountries['id']);
								$this->db->limit(1);
								$data['regions'] = $this->db->get()->result_array();
								foreach ($data['regions'] as $newRegions):
								endforeach;
									?>
									<li class="navi-item <?php if($current_page == "country".$newCountries['id']){ echo "active"; } ?>"><a class="navi-link" href="<?php echo site_url()?>region/item/<?php echo $newRegions['id']; ?>"><?php echo $newCountries["name"];?></a></li>
									<?php
							endforeach;
						?>
						
                        
                      <li class="navi-item navi-item-exit"><a class="navi-link" href="<?php echo site_url();?>main/exitt">Выйти</a></li>
                      </ul>
					  <?php //echo $name;?>
					  <a class="btn-primary w-border" href="<?php echo site_url();?>main/exitt">выйти</a>
					  <a class="btn-navi" href="#"></a>
            </div>
          </div>
        </header>
		
<?php $this->load->view("header"); 

$this->db->from("regions");
$this->db->where("id", $region_id);
$data['regions'] = $this->db->get()->result_array();
foreach ($data['regions'] as $newregions):
		$regionName = $newregions['name'];
		$country_id = $newregions['country_id'];
		$region_image = $newregions['region_image'];
		
		
			$this->db->from("countries");
			$this->db->where("id", $country_id);
			$data['country'] = $this->db->get()->result_array();
			foreach ($data['country'] as $newcountry):
					$countryName = $newcountry['name'];
			endforeach;
endforeach;



?> 

<div class="content">
        <div class="wrap-page"></div>
        <section class="country" >
		<div class="bg-overlay" style="background: url(<?php echo site_url();?>uploads/region_images/<?php echo $region_image; ?>) center top no-repeat;background-size: cover;"></div>
          <div class="container">
            <div class="country-header">
              <h1><?php echo $countryName;?></h1>
              <div class="country-header-subtitle"><?php echo $regionName;?></div>
            </div>
            <div class="country-things">
              <h2>чем заняться?</h2>
              <div class="row">
			  
				<?php
					$this->db->from("region_slides");
					$this->db->where("region_id", $region_id);
					$data['region_slides'] = $this->db->get()->result_array();
					foreach ($data['region_slides'] as $newSlide):
					?>
						<div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
						  <div class="country-things--item">
							<h4> <a class="fancybox" href="#popup<?php echo $newSlide['id']; ?>"><?php echo $newSlide['title']; ?></a></h4>
							<a class="country-things--item-pic fancybox" style="background: url(<?php echo site_url();?>uploads/region_slides/<?php echo $newSlide['image']; ?>) center top no-repeat;" href="#popup<?php echo $newSlide['id']; ?>"></a>
							<?php 
							$text = $newSlide['description'];
							$str=strpos($text, ".");
							echo $text=substr($text, 0, $str);
							 ?>
						  </div>
						  
						  <div class="popup popup-article" id="popup<?php echo $newSlide['id']; ?>">
							<div class="popup-article--pic"> <img class="img-fluid" src="<?php echo site_url();?>uploads/region_slides/<?php echo $newSlide['image']; ?>" alt=""></div>
							<div class="popup-article--text">
							  <h3><?php echo $newSlide['title']; ?></h3>
								<?php echo $newSlide['description']; ?>
							</div>
						  </div>
						</div>
					<?php
					endforeach;
				?>
                
				
			
              </div>
            </div>
            <div class="btn-more">
				<a class="btn-primary w-font" onClick="history.back();">назад</a>
				<a class="btn-primary w-font" href="<?php echo site_url();?>region/testing/<?php echo $region_id;?>">далее</a>
			</div>
          </div>
          
        </section>
      </div>
	  
			


<?php $this->load->view("footer"); ?>  
﻿		  <!--ГЛОБАЛЬНЫЙ РЕЙТИНГ-->
		         <div class="results-tests">
            <div class="container">
              <h3>Глобальный рейтинг</h3>
            </div>
            <div class="results-tests-row title">
              <div class="container">
                <div class="results-tests-wrap">
					<div class="results-tests-col col-md-1">Место</div>
					<div class="results-tests-col col-md-7">Имя участника</div>
					<div class="results-tests-col col-md-2">Правильных ответов</div>
					<div class="results-tests-col col-md-2">Затраченное время</div>
               
                </div>
              </div>
            </div>
			
			<?php
	$session_user_id = $this->session->userdata('user_id');
	$user = 0;
	$this->db->from("users");
	$get_all_users['get_all_users'] = $this->db->get()->result_array();
	foreach ($get_all_users['get_all_users'] as $newget_all_users):
	
			$USER_ID = $newget_all_users['user_id'];
			
				$this->db->from("users");
				$this->db->where("user_id",$USER_ID);
				$user['info'] = $this->db->get()->result_array();
				foreach ($user['info'] as $newUsersInfo):
					$name = $newUsersInfo['user_name'];
					$sname = $newUsersInfo['user_sname'];
				endforeach;
		

				$questions_arr[] = "";
				$i=0;
				$this->db->from("quiz_attempts");
				$this->db->where("user_id", $USER_ID);
				$data['quiz_attempts'] = $this->db->get()->result_array();
				foreach ($data['quiz_attempts'] as $newquiz_attempts):
					
					$questions_arr[] = $newquiz_attempts['quiz_id'];
					$i++;
				endforeach;	

$result_list_question = array_filter(array_unique($questions_arr));	

$attempts_id[] = "";
$correctAnswers = 0;
$allQuestion = 0;
foreach($result_list_question as $quiz_id){
	$this->db->from("quiz_attempts");
	$this->db->where("user_id", $USER_ID);
	$this->db->where("quiz_id", $quiz_id);
	$data['quiz_attemp_id'] = $this->db->get()->result_array();
	foreach ($data['quiz_attemp_id'] as $newquiz_attemp_id):
		$att_id = $newquiz_attemp_id['id'];
	endforeach;	
	$attempts_id[] = $att_id;
	
	
	
	
$this->db->from("quiz_questions");
$this->db->where("quiz_id", $quiz_id);
$data['quiz'] = $this->db->get()->result_array();
foreach ($data['quiz'] as $newquiz):
	$allQuestion++;
endforeach;


$this->db->from("quiz_attempts_answers");
$this->db->where("quiz_id", $quiz_id);
$this->db->where("quiz_attempt_id", $att_id);
$this->db->where('user_id', $USER_ID);
$data['quiz_answers'] = $this->db->get()->result_array();
foreach ($data['quiz_answers'] as $newquiz_answers):

		$this->db->from("quiz_answers");
		$this->db->where("question_id", $newquiz_answers['question_id']);
		$this->db->where("id", $newquiz_answers['answer_id']);
		$data['correctAnswers'] = $this->db->get()->result_array();
		foreach ($data['correctAnswers'] as $newcorrect_answers):
			if($newcorrect_answers['correct'] == 1){
				$correctAnswers++;
			}
		endforeach;

endforeach;


}

$res='00:00:00';
foreach($attempts_id as $att_id){
	$this->db->from("quiz_attempts");
	$this->db->where("id", $att_id);
	$data['quiz_attempts'] = $this->db->get()->result_array();
	foreach ($data['quiz_attempts'] as $newquiz_attempts):
		
		 $res = strtotime($newquiz_attempts['time_spent']) + $res;
		
	endforeach;	
}	
//$res = strtotime('13:00') + strtotime('00:25') -strtotime("00:00:00");
$spent_time = date('i:s',$res);
//echo  $USER_ID." / правильных ответов: ".$correctAnswers."<br>";
	$user++;
	$rating[$user]['user_id'] = $USER_ID;
	$rating[$user]['correct_answers'] = $correctAnswers;
	$rating[$user]['all_questions'] = $allQuestion;
	$rating[$user]['spent_time'] = $spent_time;
	unset($attempts_id);	
	unset($questions_arr);
	
	
	endforeach;	
	
	
?>
<?php
$arr = $rating;

$newArray = arrayOrderBy($arr, 'correct_answers desc,spent_time desc');

function arrayOrderBy(array &$arr, $order = null) {
    if (is_null($order)) {
        return $arr;
    }
    $orders = explode(',', $order);
    usort($arr, function($a, $b) use($orders) {
        $result = array();
        foreach ($orders as $value) {
            list($field, $sort) = array_map('trim', explode(' ', trim($value)));
            if (!(isset($a[$field]) && isset($b[$field]))) {
                continue;
            }
            if (strcasecmp($sort, 'desc') === 0) {
                $tmp = $a;
                $a = $b;
                $b = $tmp;
            }
            if (is_numeric($a[$field]) && is_numeric($b[$field]) ) {
                $result[] = $a[$field] - $b[$field];
            } else {
                $result[] = strcmp($a[$field], $b[$field]);
            }
        }
        return implode('', $result);
    });
    return $arr;
}

//var_dump($newArray );

$q = 0;
foreach($newArray as $infoLine){
		$style = "";
	//echo $infoLine['user_id']." / ".$infoLine['correct_answers']." / ".$infoLine['all_questions']." / ".$infoLine['spent_time']."<br>";
		if($infoLine['spent_time'] != "00:00"){
			
		$this->db->from("users");
        $this->db->where("user_id",$infoLine['user_id']);
        $userQW['userQW'] = $this->db->get()->result_array();
        foreach ($userQW['userQW'] as $newuserQW):
			$name = $newuserQW['user_name'];
			$sname = $newuserQW['user_sname'];
        endforeach;
		if($session_user_id == $infoLine['user_id']){
			$style="style='background: #bcd7ff;'";
		}
		?>
			<div class="results-tests-row" <?php echo $style;?>>
              <div class="container">
                <div class="results-tests-wrap">         
                  <div class="results-tests-wrap" style="width:100%;">         
					  <div class="results-tests-col col-md-1"><?php echo ++$q; ?></div>
					  <div class="results-tests-col col-md-7"><?php echo $name." ".$sname; ?></div>
					  <div class="results-tests-col col-md-2"><?php echo $infoLine['correct_answers']; ?> из <?php echo $infoLine['all_questions']; ?></div>
					  <div class="results-tests-col col-md-2"><?php echo $infoLine['spent_time']; ?></div>
					</div>
                </div>
              </div>
            </div>
		<?php
		}
}
?>
	  

			
			
           
          </div>
		  <!--ГЛОБАЛЬНЫЙ РЕЙТИНГ-->
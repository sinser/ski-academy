﻿  <!DOCTYPE html>
<html lang="ru-RU">
  <head>
    <title>Главная страница
    </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="<?php echo site_url(); ?>resources/styles/bootstrap-grid.min.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>resources/styles/slick.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>resources/styles/jquery.fancybox.min.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>resources/styles/style.css" rel="stylesheet">
		<link href="<?php echo site_url(); ?>resources/styles/mobile.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>resources/styles/my-styles.css" rel="stylesheet">
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo site_url(); ?>resources/fav.png">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
  <input type="hidden" id="URL" value="<?php echo site_url(); ?>">
  <?php
    $user_id = $this->session->userdata('user_id');
    if($user_id){
      header('Location: '.site_url('main'));
    }
  ?>
    <div class="main m-page">
      <section class="main-box">
        <div class="main-box-wrap">
          <div class="container">
            <div class="main-box-title">
              горнолыжная</br >
              академия<span>от</br ><strong>
                  Good</br >Time</br >
                  Travel</strong></span>
            </div>
            <div id="scene">
              <div class="main-box--cloud-l" data-depth="0.8"></div>
              <div class="main-box--cloud-r" data-depth="0.5"></div>
            </div>
            <div class="main-box-buttons"><a class="btn-primary fancybox" href="#enter-popup">вход</a><a class="btn-primary fancybox" href="#register-popup">регистрация</a></div>
          </div>
        </div>
		
		


        <div class="popup" id="enter-popup">
        <div class="spinner-content"></div>
          <div class="popup-form" id="popup-login-form">
            <div class="popup-title">вход на сайт</div>
            <div class="popup-subtitle">Спасибо что вернулись к нам!</div>
           
              <div class="form-b-row">
                <div class="form-b-label">Электронная почта</div>
                <input class="form-b-inp" type="text" placeholder="" id="login-email">
              </div>
              <div class="form-b-row">
                <div class="form-b-label">Пароль</div>
                <div class="form-b-wrap">
                  <input class="form-b-inp" type="password" placeholder="" id="login-password">
                  <div class="check-item">
                    <label class="check-item-label" for="id">
                      <input type="checkbox" id="id"><span class="check-item-dot"></span><span class="check-item-name">Запомнить  </span>
                    </label>
                  </div>
                </div>
              </div>
              <div class="popup-forgot"><a href="#" id="forget">Забыли пароль?</a></div>
              <div class="popup-buttons">
                <button class="btn-primary" id="login-btn">войти</button>
              </div>
              <div class="error-message"></div>
          </div>
		  
		  
		  <div class="popup-form" id="popup-forget-form">
            <div class="popup-title">Забыли пароль?</div>
           
           
              <div class="form-b-row">
                <div class="form-b-label">Электронная почта</div>
                <input class="form-b-inp" type="text" placeholder="" id="login-email-forget">
              </div>
              
              <div class="popup-forgot"><a href="#" id="forget-back">Вернуться на форму входа</a></div>
              <div class="popup-buttons">
                <button class="btn-primary" id="resend-btn">Вспомнить пароль</button>
              </div>
              <div class="error-message-forget"></div>
          </div>
		  
		  
        </div>


        <div class="popup" id="register-popup">
          <div class="spinner-content"></div>
          <div class="popup-form">
            <div class="popup-title">регистрация</div>
            <div class="popup-subtitle">Пройдите быструю регистрацию, <br /> чтобы получить доступ к порталу и тестам</div>
          
              <div class="form-b-row">
                <div class="form-b-label">Имя</div>
                <input class="form-b-inp" type="text" id="name" placeholder="Иван">
              </div>
              <div class="form-b-row">
                <div class="form-b-label">Фамилия</div>
                <input class="form-b-inp" type="text" id="sname" placeholder="Иванов">
              </div>
              <div class="form-b-row">
                <div class="form-b-label">Электронная почта</div>
                <input class="form-b-inp" type="text" id="email" placeholder="example@example.com">
              </div>
              <div class="form-b-row">
                <div class="form-b-label">Пароль <small>(минимум 8 символов)</small></div>
                <input class="form-b-inp" type="password" id="password" placeholder="Только латинские буквы и цифры">
              </div>
              <div class="form-b-row">
                <div class="form-b-label">Пароль ещё раз</div>
                <input class="form-b-inp" type="password" id="spassword" placeholder="Только латинские буквы и цифры">
              </div>
              <div class="popup-buttons">
                <button class="btn-primary" id="reg-btn" type="button">зарегистрироваться</button>
              </div>
              <div class="error-message"></div>
          </div>
        </div>


      </section>
      <footer class="footer">
        <div class="container">
          <div class="row">
            <div class="col-6 col-lg-6 col-xl-6">
              <ul class="f-navi">
					<li class="f-navi-item"><a class="f-navi-link" href="<?php echo site_url()?>about">О нас   </a></li>
					<li class="f-navi-item"><a class="f-navi-link" href="<?php echo site_url()?>tours">Туры</a></li>
					<li class="f-navi-item"><a class="f-navi-link" href="<?php echo site_url()?>country/all">Страны</a></li>
					<li class="f-navi-item active"><a class="f-navi-link" href="<?php echo site_url()?>contacts">Контакты</a></li>
              </ul>
            </div>
            <div class="col-6 col-lg-6 col-xl-6">
              <div class="copyright">
                <Good>Good Time Travel 2019 ©</Good>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </div>
    <script src="<?php echo site_url(); ?>resources/js/jquery-3.4.1.min.js"></script>
    <script src="<?php echo site_url(); ?>resources/js/slick.min.js"></script>
    <script src="<?php echo site_url(); ?>resources/js/jquery.fancybox.min.js"></script>
    <script src="<?php echo site_url(); ?>resources/js/parallax.min.js"></script>
    <script src="<?php echo site_url(); ?>resources/js/scripts.js"></script>
    <script src="<?php echo site_url(); ?>resources/js/main-scripts.js"></script>
  </body>
</html>
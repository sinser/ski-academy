<?php $this->load->view("header"); 

$this->db->from("regions");
$this->db->where("id", $region_id);
$data['regions'] = $this->db->get()->result_array();
foreach ($data['regions'] as $newregions):
		$regionName = $newregions['name'];
		$country_id = $newregions['country_id'];
		$region_image = $newregions['region_image'];
		$page_title = $newregions['page_title'];
		$main_block_left = $newregions['main_block_left'];
		$main_block_right = $newregions['main_block_right'];
				$this->db->from("countries");
				$this->db->where("id", $country_id);
				$data['country'] = $this->db->get()->result_array();
				foreach ($data['country'] as $newcountry):
						$countryName = $newcountry['name'];
				endforeach;
endforeach;
?> 

<div class="content">
        <section class="country">
		<div class="bg-overlay" style="background: url(<?php echo site_url();?>uploads/region_images/<?php echo $region_image; ?>) center top no-repeat;background-size: cover;"></div>
          <div class="container">
            <div class="country-header">
              <h1><?php echo $countryName;?></h1>
              <div class="country-header-subtitle"><?php echo $regionName;?></div>
            </div>
            <div class="country-text">
              <h2><?php echo $page_title;?></h2>
              <div class="text-slider">
                <div>
                  <div class="text-slider-item">
                    <div class="row">
                      <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
						<?php echo $main_block_left;?>
					  </div>
                      <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <?php echo $main_block_right;?>
                      </div>
                    </div>
                  </div>
                </div>	
              </div>
              <div class="fancy-slider">
			  <?php
				$this->db->from("region_carousel_images");
				$this->db->where("region_id",$region_id);
				$this->db->order_by("priority", "asc"); 
				$data['imagesproducts'] = $this->db->get()->result_array();
				foreach ($data['imagesproducts'] as $newimagesproducts):
					?>
					 <div>
						<a class="fancybox" href="<?php echo site_url();?>uploads/region_images_carousel/<?php echo $newimagesproducts['image_name'];?>" data-fancybox="gallery">
					
						<img class="img-fluid" style="max-height: 100px;" src="<?php echo site_url();?>uploads/region_images_carousel/<?php echo $newimagesproducts['image_name'];?>" alt="">
						
						</a>
					</div>
					<?php
				endforeach;
			  ?>
              </div>
              <div class="btn-more">
				<a class="btn-primary w-font" onClick="history.back();">назад</a>
				<a class="btn-primary w-font" href="<?php echo site_url();?>region/next/<?php echo $region_id;?>">далее</a>
				</div>
            </div>
          </div>
        </section>
      </div>
	  
	  
	   
	  
	  
<?php $this->load->view("footer"); ?>  
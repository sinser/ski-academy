     <footer class="footer">
          <div class="container">
            <div class="row">
              <div class="col-12 col-sm-12 col-md-7 col-lg-7 col-xl-7">
                <ul class="f-navi">
                  <li class="f-navi-item"><a class="f-navi-link" href="<?php echo site_url()?>about">О нас   </a></li>
                  <li class="f-navi-item"><a class="f-navi-link" href="<?php echo site_url()?>tours">Туры</a></li>
                  <li class="f-navi-item"><a class="f-navi-link" href="<?php echo site_url()?>country/all">Страны</a></li>
                  <li class="f-navi-item active"><a class="f-navi-link" href="<?php echo site_url()?>contacts">Контакты</a></li>
                </ul>
              </div>
              <div class="col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5">
                <div class="copyright">
                  <Good> Good Time Travel 2019 ©</Good>
                </div>
              </div>
            </div>
          </div>
        </footer>
		
		
      </div>
    </div>
    <script src="<?php echo site_url(); ?>resources/js/jquery-3.4.1.min.js"></script>
    <script src="<?php echo site_url(); ?>resources/js/slick.min.js"></script>
    <script src="<?php echo site_url(); ?>resources/js/jquery.fancybox.min.js"></script>
    <script src="<?php echo site_url(); ?>resources/js/scripts.js"></script>
	<script src="<?php echo site_url(); ?>resources/js/main-scripts.js"></script>
  </body>
</html>
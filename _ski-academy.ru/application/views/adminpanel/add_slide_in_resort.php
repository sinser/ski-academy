<?php 
	$this->load->view('adminpanel/adminheader');
?>
    
        
<div class="container">
            <ul class="breadcrumb"><li><a href="#">Главная</a></li>
<li class="active">Добавление слайда в курорт "<?php echo $resort_name;?>"</li>
 <a class="add-button" href="<?php echo site_url();?>admin/show_slides/<?php echo $resort_id;?>">Все слайды курорта "<?php echo $resort_name;?>"</a>
</ul>            
            
   
                    <div class="banners-index">

    <h4>Добавление слайда в курорт "<?php echo $resort_name;?>"</h4>
    
   
    <div id="w0" class="grid-view">


<?php 
	$attributes = array( 'class' => 'admin-form' );
	echo form_open_multipart('admin/add_slide_item_in_resort/',$attributes); 
?>


 <div class="form-group field-news-title required">
	<label class="control-label" for="news-title">Заголовок слайда</label>
	<input type="text" id="news-title" class="form-control" name="title" value="" required>
</div>


<div class="form-group  required">
<label class="control-label" for="news-title">Описание</label> 
<textarea name="description" id="" class="form-control" cols="30" rows="10"></textarea>
</div>


<div class="form-group field-news-title required">
	<label class="control-label" for="news-title">Картинка слайда </label>
	<input type="file" class="form-control" name="slide_image" >
</div>


<input type="hidden" value="<?php echo $resort_id;?>" name="resort_id">


  
  
 <div class="form-group field-news-hidden">



<div class="form-group">

<button type="submit" class="btn btn-success">Добавить курорт</button>    

</div>
</div>
</form>



        </div>
        </div>
        </div>
        
        
    
 <script>
    CKEDITOR.replace('description');
</script>    

   <?php 
	$this->load->view('adminpanel/adminfooter');
?>
     




</body></html>
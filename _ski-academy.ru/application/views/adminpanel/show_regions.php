<?php 

$this->load->view('adminpanel/adminheader');

?>

   

    

<div class="container">

<ul class="breadcrumb">

<ul class="breadcrumb"><li><a href="<?php echo site_url();?>admin/controlpanel">Главная</a></li>
<li> <a class href="<?php echo site_url();?>admin/regions">Регионы</a></li>
<li> Регионы "<?php echo $country_name; ?>"</li>
     <a class="add-button" href="<?php echo site_url();?>admin/add_regions_in_country/<?php echo $country_id;?>">Добавить регион в "<?php echo $country_name; ?>"</a>
     

</ul>            

               

                

  <div class="news-index">

 

<h4>Регионы</h4>



<!-- <div class="summary">Всего категорий:<b><?php //echo $ALL;?></b>.</div> -->

<table class="table table-striped table-bordered"><thead>

<tr><th>ID</th>
<th>Картинка</th>
<th>Заголовок</th>
<th>Страна</th>
<th>Порядок</th>
<th class="action-column">Действия</th></tr>


</thead>



<tbody id="search" style="    background: #8fffc1;">

</tbody>
<tbody >

<?php

$i=0; 


$ALL_CATEGORIES=0;
foreach ($show_regions as $newcategories):
$ALL_CATEGORIES++;
endforeach;

foreach ($show_regions as $newcategories): $i++;?>



<tr id="news<?php echo $newcategories['id'];?>">
<td><?php echo $i;?></td>
<td>
<?php
	if($newcategories['region_image'] != ""){
		?>
		<img src="<?php echo site_url();?>/uploads/region_images/<?php echo $newcategories['region_image'];?>" style="max-width:100px;">
		<?php
	}else{
		?>
		<img src="<?php echo site_url();?>/resources/images/no-image.png" style="max-width:100px;">
		<?php
	}
?>
</td>
<td><?php echo $newcategories['name'];?></td>

<td>
    <?php 
        $this->db->from("countries");
		$this->db->where("id", $newcategories['country_id']); 
        $data['regions'] = $this->db->get()->result_array();
        foreach ($data['regions'] as $oneRegion):  
                echo $oneRegion['name'];
        endforeach;
    ?>
</td>


<td>
<?php
    if($i!=1){
        ?>
            <a href="<?php echo site_url();?>admin/regionsup/<?php echo $newcategories['id'];?>/<?php echo $newcategories['country_id'];?>">
                <span class="glyphicon glyphicon-arrow-up"></span>
            </a>
            
        <?php
    }
?>

<?php
    if($i!=$ALL_CATEGORIES){
        ?>
            <a href="<?php echo site_url();?>admin/regionsdown/<?php echo $newcategories['id'];?>/<?php echo $newcategories['country_id'];?>">
                <span class="glyphicon glyphicon-arrow-down"></span>
            </a>
        <?php
    }
?>
    
        
</td>



<td>

<a href="<?php echo site_url();?>admin/show_resorts/<?php echo $newcategories['id'];?>" title="Посмотреть курорты" aria-label="Посмотреть курорты" data-pjax="0">
			Посмотреть курорты
		</a> |
		
		
    <a href="<?php echo site_url();?>admin/edit_regions/<?php echo $newcategories['id'];?>" title="Редактировать" aria-label="Редактировать" data-pjax="0">
        <span class="glyphicon glyphicon-pencil"></span>
    </a> 

    <span onclick="delRegion('<?php echo $newcategories['id'];?>','<?php echo site_url();?>')" title="Удалить" style="cursor:pointer;">
        <span class="glyphicon glyphicon-trash"></span>
    </span>
</td>

</tr>





<?php endforeach;?>





</tbody></table>







</div>

</div>

</div>







<?php 

$this->load->view('adminpanel/adminfooter');

?>

 
<?php 

$this->load->view('adminpanel/adminheader');


    $this->db->from("resorts");
    $this->db->where("id", $id); 
    $data['countries'] = $this->db->get()->result_array();

        foreach ($data['countries'] as $newRegions):
                $ID = $newRegions["id"];
                $NAME = $newRegions["name"];
				$MAP = $newRegions["map"];
                $REGION_ID = $newRegions["region_id"];
				$IMAGE = $newRegions["image"];
				$DESCRIPTION = $newRegions["description"];
        endforeach;
?>
<div class="container">

<ul class="breadcrumb"><li><a href="<?php echo site_url();?>admin/controlpanel">Главная</a></li>

<li><a href="<?php echo site_url();?>admin/resorts">Курорты</a></li>

<li class="active">Редактировать курорт</li>
<a class="add-button" href="<?php echo site_url();?>admin/add_resorts">Добавить курорт</a>
</ul>   
<div class="news-create"> 
<h4>Редактировать курорт</h4> 
<div class="news-form"> 
<?php 



$attributes = array('class' => 'admin-form', 'id' => 'admin-form', 'name' => 'admin-form');

echo form_open_multipart('admin/edit_resorts_item/',$attributes); 



?>

<div class="form-group field-news-title required">

<label class="control-label" for="news-title">Название курорта</label>

<input type="text" id="news-title" class="form-control" name="name" value="<?php echo $NAME; ?>">
</div>

<div class="form-group field-news-title required">
	<label class="control-label" for="news-title">Код карты</label>
	<input type="text" id="news-title" class="form-control" name="map" value="<?php echo $MAP; ?>">
</div>
<input type="hidden" value="<?php echo $ID;?>" name="hiddenId">



<div class="form-group field-news-title required">
	<label class="control-label" for="news-title">Главная картинка курота</label>
	<input type="file" class="form-control" name="resort_image" >
	<img  src="<?php echo site_url();?>uploads/resorts/<?php echo $IMAGE;?>" style="max-width: 330px;">
<input type="hidden"  name="hiddenImage" value="<?php echo $IMAGE;?>" >
<input type="hidden"  name="hiddenId" value="<?php echo $ID;?>" >

</div>


<div class="form-group field-news-title required">
	<label class="control-label">Описание</label>
	<textarea class="form-control" name="description"><?php echo $DESCRIPTION;?></textarea>
</div>


<div class="form-group field-news-title required">

<label class="control-label" for="news-title">К какому региону будет относиться</label>


<select name="region_id" id="region_id" class="form-control">
<option value=""></option>
 <?php
        $this->db->from("countries");
        $countries['countries'] = $this->db->get()->result_array();
        foreach ($countries['countries'] as $newcountries):  
?>
<option value="" style="background:#e3e3e3;" disabled><?php echo $newcountries["name"];?></option>
  <?php
        $this->db->from("regions");
		$this->db->where("country_id",$newcountries['id']);
        $regions['regions'] = $this->db->get()->result_array();
        foreach ($regions['regions'] as $newregions):  
?>
    <option value="<?php echo $newregions["id"];?>" <?php if($newregions["id"] == $REGION_ID ){ echo "selected"; } ?>> <?php echo $newregions["name"];?></option>
    <?php  endforeach;?>
<?php  endforeach;?>
</select>

</div>


<div class="form-group field-news-hidden">

<div class="form-group field-news-title required">
	<label class="control-label" for="news-title"> Фотографии курорта</label>
	<input type="file"  min="1" max="36" class="form-control" name="carousel_images[]" multiple="true">
</div>

<br>

<br>
 <?php 
$img=0;

  $ALL_IMAGES=0;
  $this->db->from("resort_images");
  $this->db->where("resort_id",$ID);
  $imagesproducts['imagesproducts'] = $this->db->get()->result_array();
foreach ($imagesproducts['imagesproducts'] as $newimagesproducts):
$ALL_IMAGES++;
endforeach;


        $this->db->from("resort_images");
        $this->db->where("resort_id",$ID);
        $this->db->order_by("priority", "asc"); 
        $data['imagesproducts'] = $this->db->get()->result_array();

        foreach ($data['imagesproducts'] as $newimagesproducts):
            ?>
<div class="imgdiv">
<b style="    min-width: 30px;
    display: block;
    float: left;
    text-align: center;">(<?php echo ++$img;?>)</b>
	
	<div style="    width: 100px;
    height: 100px;
    background-repeat: no-repeat;
    background: url(<?php echo site_url();?>uploads/resorts_images/<?php echo $newimagesproducts['image_name'];?>);
    background-position: center center;
    background-size: cover;
    margin: 10px;
    float: left;">
	</div>

<a onclick="delImageResort('<?php echo $newimagesproducts['id'];?>','<?php echo site_url();?>')" style="cursor:pointer;">(Удалить)</a> 
(<?php
    if($img!=1){
      ?>
        <a href="<?php echo site_url();?>Admin/resort_imagesup/<?php echo $newimagesproducts['id'];?>/<?php echo $ID;?>">
          <span class="glyphicon glyphicon-arrow-up"></span>
        </a>
        
      <?php
    }
  ?>

  <?php
    if($img!=$ALL_IMAGES){
      ?>
        <a href="<?php echo site_url();?>Admin/resort_imagesdown/<?php echo $newimagesproducts['id'];?>/<?php echo $ID;?>">
          <span class="glyphicon glyphicon-arrow-down"></span>
        </a>
      <?php
    }
  ?>)
</div>

            <?php
        endforeach;

    ?>


<div class="clear"></div>
<br>
<div class="form-group">

<button type="submit" class="btn btn-success">Изменить курорт</button>    


</form>
</div>

<div class="help-block"></div>

</div>

</div>

</div>

</div>
     
 <script>
    CKEDITOR.replace('description');
</script> 
<?php 
$this->load->view('adminpanel/adminfooter');
?>
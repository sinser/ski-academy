<?php 

$this->load->view('adminpanel/adminheader');


    $this->db->from("subsubcategories");

    $this->db->where("id", $id); 

    $data['category'] = $this->db->get()->result_array();

        foreach ($data['category'] as $newcategory):
                $ID = $newcategory["id"];
                $NAME = $newcategory["name"];
                $EN_NAME = $newcategory["nameEN"];
                $RO_NAME = $newcategory["nameRO"];
                $CAT = $newcategory["subcategory"];
            
        endforeach;

    

    

?>





    

<div class="container">

<ul class="breadcrumb"><li><a href="<?php echo site_url();?>admin/controlpanel">Главная</a></li>

<li><a href="#">Категории</a></li>

<li class="active">Редактировать Категорию 3 уровня</li>
<a class="add-button" href="<?php echo site_url();?>admin/add_categories_level_3">Добавить категорию 2 уровня</a>
</ul>            

        


<div class="news-create">



<h1>Редактировать Категорию</h1>



<div class="news-form">



<?php  
$attributes = array('class' => 'admin-form', 'id' => 'admin-form', 'name' => 'admin-form'); 
echo form_open_multipart('subsubcategories/editSubsubCategory/',$attributes);  
?>

<div class="form-group field-news-title required">

<label class="control-label" for="news-title">Заголовок</label>

<input type="text" id="news-title" class="form-control" name="name" value="<?php echo $NAME; ?>">

<br>
<label class="control-label" for="news-title">Заголовок (EN)</label>
<input type="text" id="news-title" class="form-control" name="nameEN" value="<?php echo $EN_NAME; ?>">


<label class="control-label" for="news-title">Заголовок (RO)</label>
<input type="text" id="news-title" class="form-control" name="nameRO" value="<?php echo $RO_NAME; ?>">

<div class="help-block"></div>

</div>




<br>
<div class="form-group field-news-title required">

<label class="control-label" for="news-title">К какой категории относится</label>
<select name="subcategory" class="form-control">
	<?php
		$this->db->from("categories");
        $data['categories'] = $this->db->get()->result_array();

        foreach ($data['categories'] as $newcategories): ?>
		<option disabled style="background:#ababab; color:#ffffff;"> <?php echo $newcategories["name"];?></option>
    <?php 
                $this->db->from("subcategories");
                $this->db->where("category",$newcategories['id']);
                $getCatName['getCatName'] = $this->db->get()->result_array();

                foreach ($getCatName['getCatName'] as $newgetCatName): 
                    ?>
		<option value="<?php echo $newgetCatName['id']?>" <?php if($CAT == $newgetCatName['id']){ echo 'selected';}?>> <?php echo $newgetCatName["name"];?></option>
    <?php 
                endforeach;    
        endforeach; 
    
    ?>
</select>


</div>

<input type="hidden"  name="hiddenId" value="<?php echo $ID; ?>">
<div class="form-group field-news-hidden">



<br>

<div class="form-group">

<button type="submit" class="btn btn-success">Изменить категорию</button>    



</div>

<div class="help-block"></div>

</div>

</div>

</div>

</div>

<?php 
$this->load->view('adminpanel/adminfooter');
?>
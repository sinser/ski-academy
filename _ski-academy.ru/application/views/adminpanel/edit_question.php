<?php 

$this->load->view('adminpanel/adminheader');


    $this->db->from("quiz_questions");
    $this->db->where("id", $question_id); 
    $data['countries'] = $this->db->get()->result_array();

        foreach ($data['countries'] as $newRegions):
				$id = $newRegions["id"];
                $question = $newRegions["question"];
				$quiz_id = $newRegions["quiz_id"];
        endforeach;
?>
<div class="container">

<ul class="breadcrumb"><li><a href="<?php echo site_url();?>admin/controlpanel">Главная</a></li>

<li><a href="<?php echo site_url();?>admin/quiz">Все вопросы к этому тесту</a></li>

<li class="active">Редактировать вопрос</li>
<a class="add-button" href="<?php echo site_url();?>admin/add_question_to_quiz/<?php echo $quiz_id;?>">Добавить вопрос к этому тесту</a>
</ul>   
<div class="news-create"> 
<h4>Редактировать вопрос</h4> 
<div class="news-form"> 
<?php 



$attributes = array('class' => 'admin-form', 'id' => 'admin-form', 'name' => 'admin-form');

echo form_open_multipart('admin/edit_question_item_in_quiz/',$attributes); 



?>

<div class="form-group field-news-title required">

<label class="control-label" for="news-title">Название теста</label>

<input type="text" id="news-title" class="form-control" name="question" value="<?php echo $question; ?>">
</div>
<input type="hidden" value="<?php echo $id;?>" name="hiddenId">
<input type="hidden" value="<?php echo $quiz_id;?>" name="quiz_id">


<div class="form-group field-news-hidden">



<br>

<div class="form-group">

<button type="submit" class="btn btn-success">Изменить вопрос</button>    


</form>
</div>

<div class="help-block"></div>

</div>

</div>

</div>

</div>

<?php 
$this->load->view('adminpanel/adminfooter');
?>
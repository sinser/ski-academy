<?php 

$this->load->view('adminpanel/adminheader');

?>

   

    

<div class="container">

<ul class="breadcrumb">

<ul class="breadcrumb"><li><a href="<?php echo site_url();?>admin/controlpanel">Главная</a></li>
<li> <a class href="<?php echo site_url();?>admin/resorts">Курорты</a></li>
<li> Слайды курорта "<?php echo $resort_name; ?>"</li>
     <a class="add-button" href="<?php echo site_url();?>admin/add_slide_in_resort/<?php echo $resort_id;?>">Добавить слайд в курорт "<?php echo $resort_name; ?>"</a>
     

</ul>            

               

                

  <div class="news-index">

 

<h1>Слайды курорта "<?php echo $resort_name; ?>"</h1>



<!-- <div class="summary">Всего категорий:<b><?php //echo $ALL;?></b>.</div> -->

<table class="table table-striped table-bordered"><thead>

<tr><th>ID</th>
<th>Заголовок</th>
<th>Описание</th>
<th>Курорт</th>
<th>Картинка</th>
<th>Порядок</th>
<th class="action-column">Действия</th></tr>


</thead>



<tbody id="search" style="    background: #8fffc1;">

</tbody>
<tbody >

<?php

$i=0; 


$ALL_CATEGORIES=0;
foreach ($resort_slides as $newcategories):
$ALL_CATEGORIES++;
endforeach;

foreach ($resort_slides as $newcategories): $i++;?>



<tr id="news<?php echo $newcategories['id'];?>">
<td><?php echo $i;?></td>

<td><?php echo $newcategories['title'];?></td>
<td><?php echo $newcategories['description'];?></td>
<td>
    <?php 
        $this->db->from("resorts");
		$this->db->where("id", $newcategories['resort_id']); 
        $data['resorts'] = $this->db->get()->result_array();
        foreach ($data['resorts'] as $oneResort):  
                echo $oneResort['name'];
        endforeach;
    ?>
</td>
<td>

<?php
	if($newcategories['image'] != ""){
		?>
		<img src="<?php echo site_url();?>/uploads/resort_slides/<?php echo $newcategories['image'];?>" style="max-width:100px;">
		<?php
	}else{
		?>
		<img src="<?php echo site_url();?>/resources/images/no-image.png" style="max-width:100px;">
		<?php
	}
?>


</td>

<td>
<?php
    if($i!=1){
        ?>
            <a href="<?php echo site_url();?>admin/slideup/<?php echo $newcategories['id'];?>/<?php echo $newcategories['resort_id'];?>">
                <span class="glyphicon glyphicon-arrow-up"></span>
            </a>
            
        <?php
    }
?>

<?php
    if($i!=$ALL_CATEGORIES){
        ?>
            <a href="<?php echo site_url();?>admin/slidedown/<?php echo $newcategories['id'];?>/<?php echo $newcategories['resort_id'];?>">
                <span class="glyphicon glyphicon-arrow-down"></span>
            </a>
        <?php
    }
?>
    
        
</td>



<td>
    <a href="<?php echo site_url();?>admin/edit_slide/<?php echo $newcategories['id'];?>" title="Редактировать" aria-label="Редактировать" data-pjax="0">
        <span class="glyphicon glyphicon-pencil"></span>
    </a> 

    <span onclick="delSlide('<?php echo $newcategories['id'];?>','<?php echo site_url();?>')" title="Удалить" style="cursor:pointer;">
        <span class="glyphicon glyphicon-trash"></span>
    </span>
</td>

</tr>





<?php endforeach;?>





</tbody></table>







</div>

</div>

</div>







<?php 

$this->load->view('adminpanel/adminfooter');

?>

 
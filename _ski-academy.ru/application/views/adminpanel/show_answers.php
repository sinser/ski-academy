   <?php 

	$this->load->view('adminpanel/adminheader');

?>

       

        

<div class="container">

	<ul class="breadcrumb">

<ul class="breadcrumb"><li><a href="<?php echo site_url();?>admin/controlpanel">Главная</a></li>
<li> <a class href="<?php echo site_url();?>admin/quiz">Ответы к вопросу "<?php echo $question_title;?>"</a></li>

	</ul>            

                   

                    

	  <div class="news-index">


<?php 
	$attributes = array( 'class' => 'admin-form' );
	echo form_open_multipart('admin/add_answer_item_to_question/',$attributes); 
?>


  <div class="form-group field-news-title required">

<label class="control-label" for="news-title">Ответ</label>

<input type="text" id="news-title" class="form-control" name="answer" value="" required>


</div>
<input type="hidden" name="question_id" id="question_id" value="<?php echo $question_id;?>">
  
  
 <div class="form-group field-news-hidden">



<div class="form-group">

<button type="submit" class="btn btn-success">Добавить ответ</button>    

</div>
</div>
</form>

    <h1>Все ответы</h1>



<!-- <div class="summary">Всего категорий:<b><?php //echo $ALL;?></b>.</div> -->
<?php 
	$attributes = array( 'class' => 'admin-form' );
	echo form_open_multipart('admin/edit_all_answers/',$attributes); 
?>

<table class="table table-striped table-bordered"><thead>

<tr>
<th>ID</th>
<th style="width:50px;">Правильный ответ</th>
<th>Ответ</th>
<th>Порядок</th>
<th class="action-column">Действия</th></tr>



</thead>



<tbody id="search" style="    background: #8fffc1;">

</tbody>
<tbody >

<?php

 $i=0; 


 $ALL_CATEGORIES=0;
foreach ($answers as $newcategories):
$ALL_CATEGORIES++;
endforeach;

foreach ($answers as $newcategories): $i++;?>



<tr id="news<?php echo $newcategories['id'];?>">
	<td><?php echo $i;?></td>
	<td><input type="checkbox" name="correct[]"  <?php if($newcategories['correct'] > 0){ echo "checked";}?> value="<?php echo $newcategories['id'];?>" class="checkbox"></td>
	<td><?php echo $newcategories['answer'];?></td>
	

	

<td>
	<?php
		if($i!=1){
			?>
				<a href="<?php echo site_url();?>admin/answernup/<?php echo $newcategories['id'];?>/<?php echo $newcategories['question_id'];?>">
					<span class="glyphicon glyphicon-arrow-up"></span>
				</a>
				
			<?php
		}
	?>

	<?php
		if($i!=$ALL_CATEGORIES){
			?>
				<a href="<?php echo site_url();?>admin/answerdown/<?php echo $newcategories['id'];?>/<?php echo $newcategories['question_id'];?>">
					<span class="glyphicon glyphicon-arrow-down"></span>
				</a>
			<?php
		}
	?>
		
			
	</td>

 

	<td>
	
		<a href="<?php echo site_url();?>admin/edit_answer/<?php echo $newcategories['id'];?>" title="Редактировать" aria-label="Редактировать" data-pjax="0">
			<span class="glyphicon glyphicon-pencil"></span>
		</a> 
	| 
		<span onclick="delAnswer('<?php echo $newcategories['id'];?>','<?php echo site_url();?>')" title="Удалить" style="cursor:pointer;">
			<span class="glyphicon glyphicon-trash"></span>
		</span>
		<input type="hidden" name="answers[]" value="<?php echo $newcategories['id'];?>">
		
	</td>

</tr>





<?php endforeach;?>





</tbody></table>
<input type="hidden" name="question_id" id="question_id" value="<?php echo $question_id;?>">
<button type="submit" class="btn btn-success">Редактировать</button>    
</form>




</div>

</div>

 </div>

 

 



<?php 

	$this->load->view('adminpanel/adminfooter');

?>

     
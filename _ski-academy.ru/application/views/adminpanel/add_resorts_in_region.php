<?php 
	$this->load->view('adminpanel/adminheader');
?>
    
        
<div class="container">
            <ul class="breadcrumb"><li><a href="#">Главная</a></li>
<li class="active">Добавление Курорта</li>
 <a class="add-button" href="<?php echo site_url();?>admin/resorts">Все курорты</a>
</ul>            
            
   
                    <div class="banners-index">

    <h3>Добавление Курорта в регион "<?php echo $region_name;?>"</h3>
    
   
    <div id="w0" class="grid-view">


<?php 
	$attributes = array( 'class' => 'admin-form' );
	echo form_open_multipart('admin/add_resorts_item_in_region/',$attributes); 
?>


  <div class="form-group field-news-title required">

<label class="control-label" for="news-title">Название курорта</label>

<input type="text" id="news-title" class="form-control" name="name" value="" required>


</div>

<input type="hidden" value="<?php echo $region_id;?>" name="region_id">


  
  
 <div class="form-group field-news-hidden">



<div class="form-group">

<button type="submit" class="btn btn-success">Добавить курорт</button>    

</div>
</div>
</form>



        </div>
        </div>
        </div>
        
        
        

   <?php 
	$this->load->view('adminpanel/adminfooter');
?>
     




</body></html>
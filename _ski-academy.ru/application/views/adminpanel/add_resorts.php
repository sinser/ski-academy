<?php 
	$this->load->view('adminpanel/adminheader');
?>
    
        
<div class="container">
            <ul class="breadcrumb"><li><a href="#">Главная</a></li>
<li class="active">Добавление Курорта</li>
 <a class="add-button" href="<?php echo site_url();?>admin/resorts">Все курорты</a>
</ul>            
            
   
                    <div class="banners-index">

    <h1>Добавление Курорта</h1>
    
   
    <div id="w0" class="grid-view">


<?php 
	$attributes = array( 'class' => 'admin-form' );
	echo form_open_multipart('admin/add_resorts_item/',$attributes); 
?>


<div class="form-group field-news-title required">
	<label class="control-label" for="news-title">Название курорта</label>
	<input type="text" id="news-title" class="form-control" name="name" value="" required>
</div>

<div class="form-group field-news-title required">
	<label class="control-label" for="news-title">Код карты</label>
	<input type="text" id="news-title" class="form-control" name="map" value="" >
</div>

<div class="form-group field-news-title required">
	<label class="control-label" for="news-title">Главная картинка курота</label>
	<input type="file" class="form-control" name="resort_image" >
</div>


<div class="form-group field-news-title required">
	<label class="control-label">Описание</label>
	<textarea class="form-control" name="description"></textarea>
</div>


<div class="form-group field-news-title required">

<label class="control-label" for="news-title">К какому региону будет относиться</label>

<select name="region_id" id="region_id" class="form-control">
<option value=""></option>
 <?php
        $this->db->from("countries");
        $countries['countries'] = $this->db->get()->result_array();
        foreach ($countries['countries'] as $newcountries):  
?>
<option value="" style="background:#e3e3e3;" disabled><?php echo $newcountries["name"];?></option>
  <?php
        $this->db->from("regions");
		$this->db->where("country_id",$newcountries['id']);
        $regions['regions'] = $this->db->get()->result_array();
        foreach ($regions['regions'] as $newregions):  
?>
    <option value="<?php echo $newregions["id"];?>"> <?php echo $newregions["name"];?></option>
    <?php  endforeach;?>
<?php  endforeach;?>
</select>


</div>
  
  

<div class="form-group field-news-title required">
	<label class="control-label" for="news-title"> Фотографии курорта</label>
	<input type="file"  min="1" max="36" class="form-control" name="carousel_images[]" multiple="true">
</div>

 <div class="form-group field-news-hidden">

     
<script>
    CKEDITOR.replace('description');
</script> 

<div class="form-group">

<button type="submit" class="btn btn-success">Добавить курорт</button>    

</div>
</div>
</form>



        </div>
        </div>
        </div>
        
        
        

   <?php 
	$this->load->view('adminpanel/adminfooter');
?>
     




</body></html>
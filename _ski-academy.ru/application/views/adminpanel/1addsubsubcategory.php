<?php 
	$this->load->view('adminpanel/adminheader');
?>
    
        
<div class="container">
            <ul class="breadcrumb"><li><a href="#">Главная</a></li>
<li class="active">Добавление Подкатегории</li>
 <a class="add-button" href="<?php echo site_url();?>admin/add_categories_level_3">Все Подкатегории</a>
</ul>            
            
   
                    <div class="banners-index">

    <h1>Добавление категории 3 уровня</h1>
    
   
    <div id="w0" class="grid-view">


<?php 
	$attributes = array('class' => 'admin-form', 'id' => 'addLeague', 'name' => 'addLeague');
	echo form_open_multipart('subsubcategories/addSubSubCategoryItem/',$attributes); 
?>



 

  <div class="form-group field-news-title required">

<label class="control-label" for="news-title">Заголовок (RU)</label>

<input type="text" id="news-title" class="form-control" name="name" value="" required>

<br>
<label class="control-label" for="news-title">Заголовок (EN)</label>
<input type="text" id="news-title" class="form-control" name="nameEN">


<label class="control-label" for="news-title">Заголовок (RO)</label>
<input type="text" id="news-title" class="form-control" name="nameRO">

<div class="help-block"></div>

</div>



 
 <div class="form-group field-news-title required">
<label class="control-label" for="news-title">К какой категории относится</label>
<select name="subcategory" class="form-control">
	<?php
		$this->db->from("categories");
        $data['categories'] = $this->db->get()->result_array();

        foreach ($data['categories'] as $newcategories): ?>
		<option disabled style="background:#ababab; color:#ffffff;"> <?php echo $newcategories["name"];?></option>
    <?php 
                $this->db->from("subcategories");
                $this->db->where("category",$newcategories['id']);
                $getCatName['getCatName'] = $this->db->get()->result_array();

                foreach ($getCatName['getCatName'] as $newgetCatName): 
                    ?>
		<option value="<?php echo $newgetCatName['id']?>"> <?php echo $newgetCatName["name"];?></option>
    <?php 
                endforeach;    
        endforeach; 
    
    ?>
</select>
</div>

  
  
 <div class="form-group field-news-hidden">



<div class="form-group">

<button type="submit" class="btn btn-success">Добавить Категорию</button>    

</div>
</div>
</form>



        </div>
        </div>
        </div>
        
        
        

   <?php 
	$this->load->view('adminpanel/adminfooter');
?>
     




</body></html>
<?php 

$this->load->view('adminpanel/adminheader');


    $this->db->from("quiz");
    $this->db->where("id", $quiz_id); 
    $data['countries'] = $this->db->get()->result_array();

        foreach ($data['countries'] as $newRegions):
				$id = $newRegions["id"];
                $question = $newRegions["question"];
				$resort_id = $newRegions["resort_id"];
        endforeach;
?>
<div class="container">

<ul class="breadcrumb"><li><a href="<?php echo site_url();?>admin/controlpanel">Главная</a></li>

<li><a href="<?php echo site_url();?>admin/quiz">Все тесты</a></li>

<li class="active">Редактировать тест</li>
<a class="add-button" href="<?php echo site_url();?>admin/add_test_to_resort/<?php echo $resort_id;?>">Добавить тест</a>
</ul>   
<div class="news-create"> 
<h4>Редактировать тест</h4> 
<div class="news-form"> 
<?php 



$attributes = array('class' => 'admin-form', 'id' => 'admin-form', 'name' => 'admin-form');

echo form_open_multipart('admin/edit_quiz_item/',$attributes); 



?>

<div class="form-group field-news-title required">

<label class="control-label" for="news-title">Название теста</label>

<input type="text" id="news-title" class="form-control" name="question" value="<?php echo $question; ?>">
</div>
<input type="hidden" value="<?php echo $id;?>" name="hiddenId">
<input type="hidden" value="<?php echo $resort_id;?>" name="resort_id">


<div class="form-group field-news-hidden">



<br>

<div class="form-group">

<button type="submit" class="btn btn-success">Изменить тест</button>    


</form>
</div>

<div class="help-block"></div>

</div>

</div>

</div>

</div>

<?php 
$this->load->view('adminpanel/adminfooter');
?>
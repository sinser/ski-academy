<?php 
	$this->load->view('adminpanel/adminheader');
?>
    
        
<div class="container">
            <ul class="breadcrumb"><li><a href="#">Главная</a></li>
<li class="active">Добавление Региона</li>
 <a class="add-button" href="<?php echo site_url();?>admin/regions">Все регионы</a>
</ul>            
            
   
                    <div class="banners-index">

    <h3>Добавление Региона в страну "<?php echo $country_name;?>"</h3>
    
   
    <div id="w0" class="grid-view">


<?php 
	$attributes = array( 'class' => 'admin-form' );
	echo form_open_multipart('admin/add_regions_item_in_country/',$attributes); 
?>


  <div class="form-group field-news-title required">

<label class="control-label" for="news-title">Название региона</label>

<input type="text" id="news-title" class="form-control" name="name" value="" required>


</div>



<div class="form-group field-news-title required">
	<label class="control-label" for="news-title">Главная картинка региона </label>
	<input type="file" class="form-control" name="region_image" >
</div>


<div class="form-group field-news-title required">
	<label class="control-label" for="news-title">Заголовок страницы</label>
	<input type="text" id="news-title" class="form-control" name="page_title" value="" required>
</div>

<div class="form-group field-news-title required">
	<label class="control-label" for="news-title">Главный блок слева</label>
	<textarea class="form-control" name="main_block_left"></textarea>
</div>

<div class="form-group field-news-title required">
	<label class="control-label" for="news-title">Главный блок справа</label>
	<textarea class="form-control" name="main_block_right"></textarea>
</div>


<input type="hidden" name="country_id" id="country_id" value="<?php echo $country_id;?>">
  
  
 <div class="form-group field-news-hidden">



<div class="form-group">

<button type="submit" class="btn btn-success">Добавить регион</button>    

</div>
</div>
</form>



        </div>
        </div>
        </div>
        
        
      <script>
    CKEDITOR.replace('main_block_left');
	CKEDITOR.replace('main_block_right');
</script>      

   <?php 
	$this->load->view('adminpanel/adminfooter');
?>
     




</body></html>
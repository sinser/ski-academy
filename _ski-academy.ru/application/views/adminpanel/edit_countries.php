   <?php 

	$this->load->view('adminpanel/adminheader');


		$this->db->from("countries");
		$this->db->where("id", $id); 
		$data['countries'] = $this->db->get()->result_array();

			foreach ($data['countries'] as $newcountries):
					$ID = $newcountries["id"];
					$NAME = $newcountries["name"];
					$country_image = $newcountries["country_image"];
			endforeach;
?>
<div class="container">

<ul class="breadcrumb"><li><a href="<?php echo site_url();?>admin/controlpanel">Главная</a></li>

<li><a href="<?php echo site_url();?>admin/countries">Страны</a></li>

<li class="active"><?php echo $NAME; ?></li>
<li class="active">Редактирование</li>
 <a class="add-button" href="<?php echo site_url();?>admin/add_countries">Добавить страну</a>
</ul>   
<div class="news-create"> 
    <h4>Редактировать Страну</h4> 
    <div class="news-form"> 
<?php 



	$attributes = array('class' => 'admin-form', 'id' => 'admin-form', 'name' => 'admin-form');

	echo form_open_multipart('admin/edit_countries_item/',$attributes); 



?>

    <div class="form-group field-news-title required">

<label class="control-label" for="news-title">Название страны</label>

<input type="text" id="news-title" class="form-control" name="name" value="<?php echo $NAME; ?>">
</div>
<input type="hidden" value="<?php echo $ID;?>" name="hiddenId">

<div class="form-group field-news-title required">
	<label class="control-label" for="news-title">Главная картинка страны</label>
	<input type="file" class="form-control" name="country_image" >
	<img  src="<?php echo site_url();?>uploads/countries/<?php echo $country_image;?>" style="max-width: 330px;">
<input type="hidden"  name="hiddenImage" value="<?php echo $country_image;?>" >
<input type="hidden"  name="hiddenId" value="<?php echo $id;?>" >

</div>

<br> 
  
  
 <div class="form-group field-news-hidden">



<br>

<div class="form-group">

<button type="submit" class="btn btn-success">Изменить страну</button>    


</form>
</div>

<div class="help-block"></div>

</div>

</div>

</div>

</div>

    <?php 
	$this->load->view('adminpanel/adminfooter');
?>
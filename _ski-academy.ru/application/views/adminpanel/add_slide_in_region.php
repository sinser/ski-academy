<?php 
	$this->load->view('adminpanel/adminheader');
?>
    
        
<div class="container">
            <ul class="breadcrumb"><li><a href="#">Главная</a></li>
<li class="active">Добавление слайда в регион "<?php echo $region_name;?>"</li>
 <a class="add-button" href="<?php echo site_url();?>admin/show_region_slides/<?php echo $region_id;?>">Все слайды региона "<?php echo $region_name;?>"</a>
</ul>            
            
   
                    <div class="banners-index">

    <h4>Добавление слайда в регион "<?php echo $region_name;?>"</h4>
    
   
    <div id="w0" class="grid-view">


<?php 
	$attributes = array( 'class' => 'admin-form' );
	echo form_open_multipart('admin/add_slide_item_in_region/',$attributes); 
?>


 <div class="form-group field-news-title required">
	<label class="control-label" for="news-title">Заголовок слайда</label>
	<input type="text" id="news-title" class="form-control" name="title" value="" required>
</div>


<div class="form-group  required">
<label class="control-label" for="news-title">Описание</label> 
<textarea name="description" id="" class="form-control" cols="30" rows="10"></textarea>
</div>


<div class="form-group field-news-title required">
	<label class="control-label" for="news-title">Картинка слайда </label>
	<input type="file" class="form-control" name="slide_image" >
</div>
<div class="form-group field-news-title required">

<label class="control-label" for="news-title">С какой стороны располагаем картинку?</label>

<select name="image_position"  class="form-control">
<option value=""></option>
<option value="right">Справа</option>
<option value="left">Слева</option>
</select>


</div>

<input type="hidden" value="<?php echo $region_id;?>" name="region_id">


  
  
 <div class="form-group field-news-hidden">



<div class="form-group">

<button type="submit" class="btn btn-success">Добавить слайд для региона</button>    

</div>
</div>
</form>



        </div>
        </div>
        </div>
        
        
    
 <script>
    CKEDITOR.replace('description');
</script>    

   <?php 
	$this->load->view('adminpanel/adminfooter');
?>
     




</body></html>
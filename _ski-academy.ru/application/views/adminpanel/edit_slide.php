<?php 
	$this->load->view('adminpanel/adminheader');


$this->db->from("resort_slides");
$this->db->where("id", $id); 
$data['countries'] = $this->db->get()->result_array();

foreach ($data['countries'] as $newRegions):
		$id = $newRegions["id"];
		$title = $newRegions["title"];
		$resort_id = $newRegions["resort_id"];
		$description = $newRegions["description"];
		$image = $newRegions["image"];
endforeach;



?>
    
        
<div class="container">
            <ul class="breadcrumb"><li><a href="#">Главная</a></li>
<li class="active">Редактирование слайда</li>
      <a class="add-button" href="<?php echo site_url();?>admin/show_slides/<?php echo $resort_id; ?>">Все слайды</a>

</ul>            
            
   
                    <div class="banners-index">

    <h4>Редактирование слайда</h4>
    
   
    <div id="w0" class="grid-view">


<?php 
	$attributes = array( 'class' => 'admin-form' );
	echo form_open_multipart('admin/edit_slide_item_in_resort/',$attributes); 
?>


 <div class="form-group field-news-title required">
	<label class="control-label" for="news-title">Заголовок слайда</label>
	<input type="text" id="news-title" class="form-control" name="title" value="<?php echo $title;?>" required>
</div>


<div class="form-group  required">
<label class="control-label" for="news-title">Описание</label> 
<textarea name="description" id="" class="form-control" cols="30" rows="10"><?php echo $description;?></textarea>
</div>


<div class="form-group field-news-title required">
	<label class="control-label" for="news-title">Картинка товара </label>
	<input type="file" class="form-control" name="slide_image" >
	<img  src="<?php echo site_url();?>uploads/resort_slides/<?php echo $image;?>" style="max-width: 330px;">
<input type="hidden"  name="hiddenImage" value="<?php echo $image;?>" >
<input type="hidden"  name="hiddenId" value="<?php echo $id;?>" >
<input type="hidden"  name="resort_id" value="<?php echo $resort_id;?>" >
</div>




  
  
 <div class="form-group field-news-hidden">



<div class="form-group">

<button type="submit" class="btn btn-success">Редактированть слайд</button>    

</div>
</div>
</form>



        </div>
        </div>
        </div>
        
        
    
 <script>
    CKEDITOR.replace('description');
</script>    

   <?php 
	$this->load->view('adminpanel/adminfooter');
?>
     




</body></html>
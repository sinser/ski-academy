<?php 

$this->load->view('adminpanel/adminheader');

?>

   

    

<div class="container">

<ul class="breadcrumb">

<ul class="breadcrumb"><li><a href="<?php echo site_url();?>admin/controlpanel">Главная</a></li>
<li> <a class href="<?php echo site_url();?>admin/resorts">Курорты</a></li>

     <a class="add-button" href="<?php echo site_url();?>admin/add_resorts">Добавить курорт</a>
     

</ul>            

               

                

  <div class="news-index">



<h1>Курорты</h1>



<!-- <div class="summary">Всего категорий:<b><?php //echo $ALL;?></b>.</div> -->

<table class="table table-striped table-bordered"><thead>

<tr><th>ID</th>
<th>Заголовок</th>
<th>Регион</th>

<th class="action-column">Действия</th></tr>


</thead>



<tbody id="search" style="    background: #8fffc1;">

</tbody>
<tbody >

<?php

$i=0; 


$ALL_CATEGORIES=0;
foreach ($resorts as $newcategories):
$ALL_CATEGORIES++;
endforeach;

foreach ($resorts as $newcategories): $i++;?>



<tr id="news<?php echo $newcategories['id'];?>">
<td><?php echo $i;?></td>

<td><?php echo $newcategories['name'];?></td>

<td>
    <?php 
        $this->db->from("regions");
		$this->db->where("id", $newcategories['region_id']); 
        $data['regions'] = $this->db->get()->result_array();
        foreach ($data['regions'] as $oneRegion):  
                echo $oneRegion['name'];
        endforeach;
    ?>
</td>

<td>
<a href="<?php echo site_url();?>admin/show_slides/<?php echo $newcategories['id'];?>" title="Посмотреть этого курорта" aria-label="Посмотреть этого курорта" data-pjax="0">
			Посмотреть слайды этого курорта
		</a> |
    <a href="<?php echo site_url();?>admin/edit_resorts/<?php echo $newcategories['id'];?>" title="Редактировать" aria-label="Редактировать" data-pjax="0">
        <span class="glyphicon glyphicon-pencil"></span>
    </a> 

    <span onclick="delResorts('<?php echo $newcategories['id'];?>','<?php echo site_url();?>')" title="Удалить" style="cursor:pointer;">
        <span class="glyphicon glyphicon-trash"></span>
    </span>
</td>

</tr>





<?php endforeach;?>





</tbody></table>







</div>

</div>

</div>







<?php 

$this->load->view('adminpanel/adminfooter');

?>

 
<?php 

$this->load->view('adminpanel/adminheader');


    $this->db->from("regions");
    $this->db->where("id", $id); 
    $data['countries'] = $this->db->get()->result_array();

        foreach ($data['countries'] as $newRegions):
                $ID = $newRegions["id"];
                $NAME = $newRegions["name"];
                $COUNTRY_ID = $newRegions["country_id"];
				
				$IMAGE = $newRegions["region_image"];
				$TITLE = $newRegions["page_title"];
				$LEFT_TEXT = $newRegions["main_block_left"];
				$RIGHT_TEXT = $newRegions["main_block_right"];
        endforeach;
?>
<div class="container">

<ul class="breadcrumb"><li><a href="<?php echo site_url();?>admin/controlpanel">Главная</a></li>

<li><a href="<?php echo site_url();?>admin/regions">Регионы</a></li>

<li class="active">Редактировать Регион</li>
<a class="add-button" href="<?php echo site_url();?>admin/add_regions">Добавить регион</a>
</ul>   
<div class="news-create"> 
<h4>Редактировать Регион</h4> 
<div class="news-form"> 
<?php 



$attributes = array('class' => 'admin-form', 'id' => 'admin-form', 'name' => 'admin-form');

echo form_open_multipart('admin/edit_regions_item/',$attributes); 



?>

<div class="form-group field-news-title required">

<label class="control-label" for="news-title">Название региона</label>

<input type="text" id="news-title" class="form-control" name="name" value="<?php echo $NAME; ?>">
</div>
<input type="hidden" value="<?php echo $ID;?>" name="hiddenId">




<div class="form-group field-news-title required">
	<label class="control-label" for="news-title">Главная картинка региона </label>
	<input type="file" class="form-control" name="region_image" >
	
	<img  src="<?php echo site_url();?>uploads/region_images/<?php echo $IMAGE;?>" style="max-width: 330px;">
<input type="hidden"  name="hiddenImage" value="<?php echo $IMAGE;?>" >

</div>


<div class="form-group field-news-title required">
	<label class="control-label" for="news-title">Заголовок страницы</label>
	<input type="text" id="news-title" class="form-control" name="page_title" value="<?php echo $TITLE;?>" required>
</div>

<div class="form-group field-news-title required">
	<label class="control-label" for="news-title">Главный блок слева</label>
	<textarea class="form-control" name="main_block_left"><?php echo $LEFT_TEXT;?></textarea>
</div>

<div class="form-group field-news-title required">
	<label class="control-label" for="news-title">Главный блок справа</label>
	<textarea class="form-control" name="main_block_right"><?php echo $RIGHT_TEXT;?></textarea>
</div>



<div class="form-group field-news-title required">

<label class="control-label" for="news-title">К какой стране будет отновится</label>

<select name="country_id" id="country_id" class="form-control">
<option value=""></option>
  <?php
        $this->db->from("countries");
        $countries['countries'] = $this->db->get()->result_array();
        foreach ($countries['countries'] as $newRegions):  
?>
    <option value="<?php echo $newRegions["id"];?>" <?php if($newRegions["id"] == $COUNTRY_ID ){ echo "selected"; } ?>> <?php echo $newRegions["name"];?></option>
    <?php  endforeach;?>
</select>


</div>

<div class="form-group field-news-title required">
	<label class="control-label" for="news-title">Фотографии региона <b style="color:#ff0000;"></b></label>
	<input type="file"  min="1" max="36" class="form-control" name="carousel_images[]" multiple="true">
</div>

 Фотографии региона
<br>
 <?php 
$img=0;

  $ALL_IMAGES=0;
  $this->db->from("region_carousel_images");
  $this->db->where("region_id",$id);
  $imagesproducts['imagesproducts'] = $this->db->get()->result_array();
foreach ($imagesproducts['imagesproducts'] as $newimagesproducts):
$ALL_IMAGES++;
endforeach;


        $this->db->from("region_carousel_images");
        $this->db->where("region_id",$ID);
        $this->db->order_by("priority", "asc"); 
        $data['imagesproducts'] = $this->db->get()->result_array();

        foreach ($data['imagesproducts'] as $newimagesproducts):
            ?>
<div class="imgdiv">
<b style="    min-width: 30px;
    display: block;
    float: left;
    text-align: center;">(<?php echo ++$img;?>)</b>
	
	<div style="    width: 100px;
    height: 100px;
    background-repeat: no-repeat;
    background: url(<?php echo site_url();?>uploads/region_images_carousel/<?php echo $newimagesproducts['image_name'];?>);
    background-position: center center;
    background-size: cover;
    margin: 10px;
    float: left;">
	</div>

<a onclick="delImageProduct('<?php echo $newimagesproducts['id'];?>','<?php echo site_url();?>')" style="cursor:pointer;">(Удалить)</a> 
(<?php
    if($img!=1){
      ?>
        <a href="<?php echo site_url();?>Admin/imageup/<?php echo $newimagesproducts['id'];?>/<?php echo $ID;?>">
          <span class="glyphicon glyphicon-arrow-up"></span>
        </a>
        
      <?php
    }
  ?>

  <?php
    if($img!=$ALL_IMAGES){
      ?>
        <a href="<?php echo site_url();?>Admin/imagedown/<?php echo $newimagesproducts['id'];?>/<?php echo $ID;?>">
          <span class="glyphicon glyphicon-arrow-down"></span>
        </a>
      <?php
    }
  ?>)
</div>

            <?php
        endforeach;

    ?>

	
	

<div class="form-group field-news-hidden">



<div style="clear:both;margin-bottom:30px;"></div>

<div class="form-group">

<button type="submit" class="btn btn-success">Изменить регион</button>    


</form>
</div>

<div class="help-block"></div>

</div>

</div>

</div>

</div>
     
 <script>
    CKEDITOR.replace('main_block_left');
	CKEDITOR.replace('main_block_right');
</script> 
<?php 
$this->load->view('adminpanel/adminfooter');
?>
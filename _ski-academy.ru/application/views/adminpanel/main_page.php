<?php 
  $this->load->view('adminpanel/adminheader');

  $this->db->from("main_page");
    $this->db->where("id", 1); 
    $data['main_page'] = $this->db->get()->result_array();  

	foreach ($data['main_page'] as $newmain_page):

		$title= $newmain_page['title'];
		$description= $newmain_page['description'];
		$image= $newmain_page['image'];
		
	endforeach;

?>
    
        
<div class="container">
            <ul class="breadcrumb"><li><a href="#">Главная</a></li>
       
<li class="active">Главная страница </li>

</ul>            
            
   
                    <div class="banners-index">

    <h1>Главная страница</h1>
    
   
    <div id="w0" class="grid-view">


<?php 
  $attributes = array('class' => 'admin-form');
  echo form_open_multipart('admin/edit_main_page/',$attributes); 
?>

 <div class="form-group field-news-title required bottom-border" style="margin-bottom: 70px;">

		<label class="control-label" for="news-title">Заголовок</label> 
		<input type="text"  class="form-control" name="title" value="<?php echo $title; ?>">
		<label class="control-label" for="news-title">Описание</label> 
		<textarea class="form-control" name="description" ><?php echo $description; ?></textarea>
</div>


<div class="form-group field-news-title required">
	<label class="control-label" for="news-title">Главная картинка</label>
	<input type="file" class="form-control" name="main_image" >
	<img  src="<?php echo site_url();?>uploads/main_page/<?php echo $image;?>" style="max-width: 330px;">
<input type="hidden"  name="hiddenImage" value="<?php echo $image;?>" >
<input type="hidden"  name="hiddenId" value="1" >

</div>


 
 <div class="form-group field-news-hidden">



<div class="form-group">

<button type="submit" class="btn btn-success">Обновить страницу</button>    

</div>
</div>
</form>



        </div>
        </div>
        </div>
        
        
        

   <?php 
  $this->load->view('adminpanel/adminfooter');
?>
     
<script>
    CKEDITOR.replace('description');
</script>



</body></html>
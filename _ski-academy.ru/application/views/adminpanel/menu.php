<nav id="w1" class="navbar-default navbar-fixed-top navbar" role="navigation">
<div class="container"><div class="navbar-header"><button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#w1-collapse"><span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span></button>
<a class="navbar-brand" href="<?php echo site_url();?>admin/controlpanel" style="padding:5px;">
<img src="<?php echo site_url();?>resources/images/logotype.png" style="    width: 110px;">
</a></div>

<div id="w1-collapse" class="collapse navbar-collapse">
<ul id="w2" class="navbar-nav navbar-left nav">
<!-- Пункт меню Панель управления -->


<li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown">Страны / Регионы / Курорты <b class="caret"></b></a>
	<ul id="w2" class="dropdown-menu">
	<li><a href="<?php echo site_url();?>admin/countries" class="top">Страны</a></li>
	<li><a href="<?php echo site_url();?>admin/regions" class="top">Регионы</a></li>
	<li><a href="<?php echo site_url();?>admin/resorts" class="top">Курорты</a></li>
	<li><br></li>
	<li><a href="<?php echo site_url();?>admin/add_countries" class="top">Добавить страну</a></li>
	<li><a href="<?php echo site_url();?>admin/add_regions" class="top">Добавить регион</a></li>
	<li><a href="<?php echo site_url();?>admin/add_resorts" class="top">Добавить курорт</a></li>
	</ul>
</li>


<li><a href="<?php echo site_url();?>admin/quiz" class="top">Вопросы и ответы</a></li>


<li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown">Разное <b class="caret"></b></a>
    <ul id="w2" class="dropdown-menu">
        <li><a href="<?php echo site_url();?>admin/users" class="top">Все пользователи</a></li>
		<li><a href="<?php echo site_url();?>admin/main_page" class="top">Главная страница</a></li>
    </ul>
</li>

<li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown">Настройки <b class="caret"></b></a>
	<ul id="w2" class="dropdown-menu">
		<li><a href="<?php echo site_url();?>admin/options" class="top">Настройки профиля</a></li>
	</ul>
</li>
<li><a class="#" href="<?php echo site_url();?>admin/exitt/">Выход</a></li>
<li><a href="<?php echo site_url();?>" target="_blank">На сайт</a></li>

</ul></div></div></nav>
        
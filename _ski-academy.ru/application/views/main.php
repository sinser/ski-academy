<?php $this->load->view("header"); 

    $this->db->from("main_page");
    $this->db->where("id", 1); 
    $data['main_page'] = $this->db->get()->result_array();
	foreach ($data['main_page'] as $newMainPage):
			$title = $newMainPage["title"];
			$description = $newMainPage["description"];
			$image = $newMainPage["image"];
	endforeach;
?>
        <div class="content">		  
		   <section class="article">
			  <div class="container">
				<div class="row">
				  <div class="col-12 col-sm-7 col-md-7 col-lg-7 col-xl-7 order-2 order-sm-1 order-md-1 order-lg-1 order-xl-1">
					<h1><?php echo $title; ?></h1>
					<?php echo $description; ?>
					<a class="btn-primary w-font" href="<?php echo site_url()?>choose">выбрать модуль</a>
				  </div>
				  <div class="col-12 col-sm-5 col-md-5 col-lg-5 col-xl-5 order-1 order-sm-2 order-md-2 order-lg-2 order-xl-2">
					<div class="article-pic"><img class="img-fluid" src="<?php echo site_url()?>uploads/main_page/<?php echo $image;?>" alt=""></div>
				  </div>
				</div>
			  </div>
			</section>
        </div>
 <?php $this->load->view("footer"); ?>  
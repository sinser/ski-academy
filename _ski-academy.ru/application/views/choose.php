﻿<?php $this->load->view("header"); ?>
        <div class="content">
          
          <section class="choice-country"> 
            <div class="container">
              <h2>выберите страну и регион</h2>
            
			  
				<?php
					$this->db->from("countries");
					$this->db->order_by("priority", "asc");
					$data['countries'] = $this->db->get()->result_array();
					foreach ($data['countries'] as $newCountries):
					
					?>
					  <div class="choice-slider">
							<?php
								$this->db->from("regions");
								$this->db->order_by("priority", "asc");
								$this->db->where("country_id",  $newCountries['id']);
								$data['regions'] = $this->db->get()->result_array();
								foreach ($data['regions'] as $newRegions):
						?>
					
			  
                                <div>
                                  <div class="slider-item">
                                    <div class="row">
                                      <div class="col-12 col-sm-12 col-md-12 col-lg-5 col-xl-5">
                                        <div class="slider-item-photo" style="background: url(<?php echo site_url();?>uploads/region_images/<?php echo $newRegions['region_image']; ?>) left center no-repeat;    max-height: 415px;"></div>
                                      </div>
                                      <div class="col-12 col-sm-12 col-md-12 col-lg-7 col-xl-7">
                                        <div class="slider-item-box">
                                          <div class="slider-item-title"><span><?php echo $newCountries['name']?></span><?php echo $newRegions['name']?></div>
                                          <div class="slider-item-descr" style="overflow:hidden;display: -webkit-box !important;
    -webkit-line-clamp: 5 !important;
    -webkit-box-orient: vertical !important;"><?php echo $newRegions['main_block_left']; ?></div>
											 <a class="btn-primary" href="<?php echo site_url()?>region/item/<?php echo $newRegions['id']; ?>">Начать обучение</a>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
																
								
					<?php endforeach; ?>
					  </div>
				<?php endforeach; ?>			
            
			  
			  
            </div>
          </section>
        </div>
 <?php $this->load->view("footer"); ?>  
<?php $this->load->view("header"); 
$this->db->from("resorts");
$this->db->where("id", $resort_id);
$data['resorts'] = $this->db->get()->result_array();
foreach ($data['resorts'] as $newresorts):
		$resortName = $newresorts['name'];
		$description = $newresorts['description'];
endforeach;

?> 
      <div class="content">
        <div class="wrap-page"></div>
        <section class="country" >
		
          <div class="container">
            <div class="country-header">
               <h1><?php echo $resortName;?></h1>
			   
			   <?php echo $description;?>
			    <div class="fancy-slider">
				  <?php
					$this->db->from("resort_images");
					$this->db->where("resort_id",$resort_id);
					$this->db->order_by("priority", "asc"); 
					$data['imagesproducts'] = $this->db->get()->result_array();
					foreach ($data['imagesproducts'] as $newimagesproducts):
						?>
						 <div>
							<a class="fancybox" href="<?php echo site_url();?>uploads/resorts_images/<?php echo $newimagesproducts['image_name'];?>" data-fancybox="gallery">
						
							<img class="img-fluid" style="max-height: 100px;" src="<?php echo site_url();?>uploads/resorts_images/<?php echo $newimagesproducts['image_name'];?>" alt="">
							
							</a>
						</div>
						<?php
					endforeach;
				  ?>
				  </div>
				  
				  
			   <div class="country-header-subtitle">Список тестов</div>
            </div>
			<?php
				$this->db->from("quiz");
				$this->db->where("resort_id", $resort_id);
				$this->db->order_by("priority", "asc");
				$data['quiz'] = $this->db->get()->result_array();
				foreach ($data['quiz'] as $newquiz):
				
						$allQuestion = 0;
						$this->db->from("quiz_questions");
						$this->db->where("quiz_id", $newquiz['id']);
						$data['quiz_all'] = $this->db->get()->result_array();
						foreach ($data['quiz_all'] as $newquiz_all):
							$allQuestion++;
						endforeach;
						
						
				?>
				 <div class="country-map">
				  <div class="country-map-header">
					<h3 style="font-size: 22px;"><?php echo $newquiz['question']; ?></h3>
					<a class="btn-primary w-font" href="<?php echo site_url(); ?>main/quiz/<?php echo $newquiz['id']; ?>" style="padding: 10px 80px;">
						Пройти тест<br>
						<small style="font-size: 10px;font-style: italic;">Кол-во вопросов: <?php echo $allQuestion;?> </small>
					</a>
				  </div>
				</div>
				<?php
				endforeach;
			?>
           
          </div>
          
        </section>
      </div>
<?php $this->load->view("footer"); ?> 
﻿<?php $this->load->view("header");

$user_id = $this->session->userdata('user_id');
$currentDate = date("F j, Y H:i:s");

$this->db->from("quiz");
$this->db->where("id", $quiz_id);
$data['quiz'] = $this->db->get()->result_array();
foreach ($data['quiz'] as $newquiz):
	//$isQuiz++;
	$quizTitle = $newquiz['question'];
	
		$this->db->from("resorts");
		$this->db->where("id", $newquiz['resort_id']);
		$data['resort'] = $this->db->get()->result_array();
		foreach ($data['resort'] as $newresort):
			$resortTitle = $newresort['name'];
		
		
			$this->db->from("regions");
			$this->db->where("id", $newresort['region_id']);
			$data['region'] = $this->db->get()->result_array();
			foreach ($data['region'] as $newregion):
				$regionTitle = $newregion['name'];
			
			
						$this->db->from("countries");
						$this->db->where("id", $newregion['country_id']);
						$data['countries'] = $this->db->get()->result_array();
						foreach ($data['countries'] as $newcountries):
							$countryTitle = $newcountries['name'];
						endforeach;
			endforeach;
		endforeach;
endforeach;


if(!$quizTitle){
      header('Location: '.site_url());
    }
	
$getAttemptId = 0;
$this->db->from("quiz_attempts");
$this->db->where('user_id', $user_id);
$this->db->where('quiz_id', $quiz_id);
$this->db->where("was_finished", "0");
$data['getAttemptId'] = $this->db->get()->result_array();
foreach ($data['getAttemptId'] as $newgetAttemptId):
	
	$format = "F j, Y H:i:s";
	$date = date_create_from_format($format, $currentDate);
	
	$getAttemptId = $newgetAttemptId['id'];
	$dataStart = $newgetAttemptId['data'];
	$dataFinish = date_format($date, $format);
		
	
	$you_date = $dataStart; // Ваша дата
	$now_date = $dataFinish; // Текущая дата
	$you_date_unix = strtotime($you_date);
	$now_date_unix = strtotime($now_date);
	$result = ($now_date_unix-$you_date_unix); // Расчитываем года

	$time_spent = date('i:s',  $result);
	//echo $time_spent = $time_spent * 1;
	if($result > 900){
		$time_spent = "Тест был прерван";
	}else{
		$time_spent = "00:".$time_spent;
	}

	$finish['was_finished'] = 1;
	$finish['date_time_finish'] = date_format($date, $format);
	$finish['time_spent'] = $time_spent;	
			
	$this->db->where('user_id', $user_id);
	$this->db->where('quiz_id', $quiz_id);
	$this->db->where('id', $getAttemptId);
	$this->db->update('quiz_attempts',$finish);

endforeach;




$this->db->from("quiz_attempts");
$this->db->where('user_id', $user_id);
$this->db->where('quiz_id', $quiz_id);
$this->db->where("was_finished", "1");
$data['getAttemptId'] = $this->db->get()->result_array();
foreach ($data['getAttemptId'] as $newgetAttemptId):
	$time_spent = $newgetAttemptId['time_spent'];	
	$getAttemptId = $newgetAttemptId['id'];
endforeach;


$allQuestion = 0;
$this->db->from("quiz_questions");
$this->db->where("quiz_id", $quiz_id);
$data['quiz'] = $this->db->get()->result_array();
foreach ($data['quiz'] as $newquiz):
	$allQuestion++;
endforeach;

$correctAnswers = 0;
$this->db->from("quiz_attempts_answers");
$this->db->where("quiz_id", $quiz_id);
$this->db->where("quiz_attempt_id", $getAttemptId);
$this->db->where('user_id', $user_id);
$data['quiz_answers'] = $this->db->get()->result_array();
foreach ($data['quiz_answers'] as $newquiz_answers):

		$this->db->from("quiz_answers");
		$this->db->where("question_id", $newquiz_answers['question_id']);
		$this->db->where("id", $newquiz_answers['answer_id']);
		$data['correctAnswers'] = $this->db->get()->result_array();
		foreach ($data['correctAnswers'] as $newcorrect_answers):
			if($newcorrect_answers['correct'] == 1){
				$correctAnswers++;
			}
		endforeach;

endforeach;

$time_spent = substr($time_spent, 3);
 ?>
 
  <div class="content">
        <div class="wrap-page"></div>
        <section class="results">
          <div class="container">
           <h1><?php echo $countryTitle;?></h1>
            <div class="results-subtitle" style="    margin-bottom: 15px;"><?php echo $regionTitle;?>. <?php echo $resortTitle;?></div>
			<div class="results-subtitle"><?php echo  $quizTitle;?></div>
            <div class="row">
              <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 order-2 order-sm-2 order-md-2 order-lg-1 order-xl-1">
                <div class="row">
                  <div class="col-6"><a class="results-box clock" href="#"><span class="results-box--descr">затраченное <br /> время</span><span class="results-box--data"> <strong><?php echo $time_spent;?></strong></span></a></div>
                  <div class="col-6"><a class="results-box answ" href="#"><span class="results-box--descr">правильные <br /> ответы</span><span class="results-box--data"><strong><?php echo $correctAnswers;?> </strong>из <strong><?php echo $allQuestion;?></strong></span></a></div>
                </div>
              </div>
              <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 order-1 order-sm-1 order-md-1 order-lg-2 order-xl-2">
                <div class="results-text">
                  <h3>поздравляем,<br /> вы неплохо справились!</h3>
                  <p>Вы успешно ответили на большинство вопросов теста, вы можете попробовать пройти тест заново и улучшить свои показатели, лбио вернуться на главную и попробовать себя в других регионах.</p>
                </div>
                <div class="results-f-col d-none d-sm-none d-md-none d-lg-block d-xl-block">
                  <div class="results-buttons">
                    <div class="row">
                      <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6"><a class="btn-primary" href="#">пройти заново</a></div>
                      <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6"><a class="btn-primary" href="#">Вернуться на главную</a></div>
                    </div>
                  </div>
                  <div class="social-row">
                    <div class="row align-items-center">
                      <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <div class="social-row-text">поделитесь результатом <br />в социальных сетях</div>
                      </div>
                      <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <ul class="social-box">
                          <li><a class="social-box-link vk" href="#"></a></li>
                          <li><a class="social-box-link ok" href="#"></a></li>
                          <li><a class="social-box-link tw" href="#"></a></li>
                          <li><a class="social-box-link fb" href="#"></a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="results-f-col d-block d-sm-block d-md-block d-lg-none d-xl-none">
              <div class="results-buttons">
                <div class="row">
                  <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6"><a class="btn-primary" href="<?php echo site_url();?>main/quiz/<?php echo $quiz_id; ?>">пройти заново</a></div>
                  <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6"><a class="btn-primary" href="<?php echo site_url();?>">Вернуться на главную</a></div>
                </div>
              </div>
              <div class="social-row">
                <div class="row align-items-center">
                  <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                    <div class="social-row-text">поделитесь результатом <br />в социальных сетях</div>
                  </div>
                  <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                    <ul class="social-box">
                      <li><a class="social-box-link vk" href="#"></a></li>
                      <li><a class="social-box-link ok" href="#"></a></li>
                      <li><a class="social-box-link tw" href="#"></a></li>
                      <li><a class="social-box-link fb" href="#">       </a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
	  
	  
     <?php $this->load->view("footer"); ?>
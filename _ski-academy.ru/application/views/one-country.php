﻿<?php $this->load->view("header"); ?> 
<div class="content">
        <div class="wrap-page"></div>
        <section class="country">
          <div class="country-header" style="background: url(<?php echo site_url();?>resources/content/bg-country-1.jpg) center top no-repeat;">
            <div class="container">
              <div class="country-header-wrap">
                <div class="country-header-l"><a class="country-arrow lf" href="#"><span>австрия. земля тироль. <br /></span>долина пацаун</a></div>
                <div class="country-header-c">
                  <h1>франция</h1>
                  <div class="country-header-subtitle">Три Долины. Курорт Валь Торанс</div>
                </div>
                <div class="country-header-r"><a class="country-arrow rg" href="#"><span>далее к франция. <br /></span>Курорт Межев</a></div>
              </div>
            </div>
          </div>
          <div class="container">
            <div class="country-text">
              <h2>изюминка трёх <br />знаменитых долин</h2>
              <div class="text-slider">
                <div>
                  <div class="text-slider-item">
                    <div class="row">
                      <div class="col-6 col-lg-6 col-xl-6">
                        <p>Зальцбург — старинный европейский городок. В состав Австрии он вошел по историческим меркам недавно, и это наложило отпечаток на архитектуру, культуру и даже кулинарные традиции. Туристов сюда привлекают узкие улочки города и затейливые барочные фасады древних зданий, особенно живописные с вершины Фестунгберг, к которой ведет канатная дорога. Наряду с привычными музеями и галереями, тут есть несколько интерактивных экспозиций.</p>
                      </div>
                      <div class="col-6 col-lg-6 col-xl-6">
                        <p>Зимой Зальцбург становится отправной точкой для лыжников, стремящихся на курорты австрийских Альп.</p>
                        <p>Своего рода символ города — знаменитый на весь мир композитор Вольфганг Амадей Моцарт. Место, где он родился, — одна из главных туристических достопримечательностей. Именем музыканта-виртуоза названы не только улицы, площади и выставки, но и многочисленные кофейни, магазины, конфеты и даже ликер.</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div>
                  <div class="text-slider-item">
                    <div class="row">
                      <div class="col-6 col-lg-6 col-xl-6">
                        <p>Зальцбург — старинный европейский городок. В состав Австрии он вошел по историческим меркам недавно, и это наложило отпечаток на архитектуру, культуру и даже кулинарные традиции. Туристов сюда привлекают узкие улочки города и затейливые барочные фасады древних зданий, особенно живописные с вершины Фестунгберг, к которой ведет канатная дорога. Наряду с привычными музеями и галереями, тут есть несколько интерактивных экспозиций.</p>
                      </div>
                      <div class="col-6 col-lg-6 col-xl-6">
                        <p>Зимой Зальцбург становится отправной точкой для лыжников, стремящихся на курорты австрийских Альп.</p>
                        <p>Своего рода символ города — знаменитый на весь мир композитор Вольфганг Амадей Моцарт. Место, где он родился, — одна из главных туристических достопримечательностей. Именем музыканта-виртуоза названы не только улицы, площади и выставки, но и многочисленные кофейни, магазины, конфеты и даже ликер.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="fancy-slider">
                <div><a class="fancybox" href="<?php echo site_url();?>resources/content/photo-m.jpg" data-fancybox="gallery"><img class="img-fluid" src="content/photo-m.jpg" alt=""></a></div>
                <div><a class="fancybox" href="<?php echo site_url();?>resources/content/photo-m.jpg" data-fancybox="gallery"><img class="img-fluid" src="content/photo-m.jpg" alt=""></a></div>
                <div><a class="fancybox" href="<?php echo site_url();?>resources/content/photo-m.jpg" data-fancybox="gallery"><img class="img-fluid" src="content/photo-m.jpg" alt=""></a></div>
                <div><a class="fancybox" href="<?php echo site_url();?>resources/content/photo-m.jpg" data-fancybox="gallery"><img class="img-fluid" src="content/photo-m.jpg" alt=""></a></div>
                <div><a class="fancybox" href="<?php echo site_url();?>resources/content/photo-m.jpg" data-fancybox="gallery"><img class="img-fluid" src="content/photo-m.jpg" alt=""></a></div>
                <div><a class="fancybox" href="<?php echo site_url();?>resources/content/photo-m.jpg" data-fancybox="gallery"><img class="img-fluid" src="content/photo-m.jpg" alt=""></a></div>
                <div><a class="fancybox" href="<?php echo site_url();?>resources/content/photo-m.jpg" data-fancybox="gallery"><img class="img-fluid" src="content/photo-m.jpg" alt=""></a></div>
                <div><a class="fancybox" href="<?php echo site_url();?>resources/content/photo-m.jpg" data-fancybox="gallery"><img class="img-fluid" src="content/photo-m.jpg" alt=""></a></div>
              </div>
            </div>
            <div class="country-things">
              <h2>чем заняться?</h2>
              <div class="row country-things-row">
                <div class="col-7 col-lg-7 col-xl-7">
                  <h4>рождество в зальцбурге</h4>
                  <p>В Рождество в Зальцбурге открываются Рождественские рынки, где вы можете попробовать традиционные местные блюда — горячий картофель, пончики с травами, и обязательно конфеты, а также приобрести елочные украшения, изделия местных ремесленников и зимнюю одежду.</p>
                  <p>Старинный Зальцбург (Salzburg) в зимнее время романтичен и тих. Сказочные улицы, покрытые снегом, выглядят словно театральные декорации к традиционным яслям, адвентским рынкам и вездесущей рождественской музыке.</p>
                  <p>Совместите зимний праздник в Зальцбурге с поездкой в горы. Бесплатный лыжный трансфер доставит вас с комфортом в международную лыжную зону «Snow Space Salzburg». Шаттл-бусы будут курсировать Зальцбург — Флахау ежедневно – с 21 декабря 2019 г. по 23 марта 2020 г., в сопровождении опытных гидов. Гиды организуют на месте приобретение ски-пассов и прокат инвентаря. Лыжников отвезут в область Флахау – это в 70 км от Зальцбурга. Там расположена зона Ski Amadé. Протяжённость её лыжных трасс составляет 860 км.</p>
                </div>
                <div class="col-5 col-lg-5 col-xl-5"><img class="img-fluid" src="<?php echo site_url();?>resources/content/photo-g.jpg" alt=""></div>
              </div>
              <div class="row country-things-row">
                <div class="col-5 col-lg-5 col-xl-5"><img class="img-fluid" src="<?php echo site_url();?>resources/content/photo-g.jpg" alt=""></div>
                <div class="col-6 col-lg-6 col-xl-6">
                  <h4>Ski Shuttle</h4>
                  <p>В зимний сезон с 8 декабря по 2 апреля рейсовые автобусы и шаттлы компаний Fly Ski Shuttle, Orio Shuttle и Flexibus будут курсировать по субботам и воскресеньям из аэропортов Вероны, Бергамо, Милана, Венеции и Тревизо, а также некоторых крупных городов севера Италии на самые известные горнолыжные курорты провинции Трентино.</p>
                  <p>Благодаря сотрудничеству с транспортной компанией Orio Shuttle маршрутная сеть еще больше расширилась – среди аэропортов, откуда быстро и удобно можно попасть на популярные горнолыжные курорты в Доломитах, появился самый крупный аэропорт Милана – Malpensa. Бронировать поездки нужно заранее в Интернете: www.flyskishuttle.com.</p>
                </div>
              </div>
              <div class="row">
                <div class="col-7 col-lg-7 col-xl-7">
                  <h4>Достопримечательности круглый год</h4>
                  <p>В Рождество в Зальцбурге открываются Рождественские рынки, где вы можете попробовать традиционные местные блюда — горячий картофель, пончики с травами, и обязательно конфеты, а также приобрести елочные украшения, изделия местных ремесленников и зимнюю одежду.</p>
                  <p>Старинный Зальцбург (Salzburg) в зимнее время романтичен и тих. Сказочные улицы, покрытые снегом, выглядят словно театральные декорации к традиционным яслям, адвентским рынкам и вездесущей рождественской музыке.</p>
                  <p>Совместите зимний праздник в Зальцбурге с поездкой в горы. Бесплатный лыжный трансфер доставит вас с комфортом в международную лыжную зону «Snow Space Salzburg». Шаттл-бусы будут курсировать Зальцбург — Флахау ежедневно – с 21 декабря 2019 г. по 23 марта 2020 г., в сопровождении опытных гидов. Гиды организуют на месте приобретение ски-пассов и прокат инвентаря. Лыжников отвезут в область Флахау – это в 70 км от Зальцбурга. Там расположена зона Ski Amadé. Протяжённость её лыжных трасс составляет 860 км.</p>
                </div>
                <div class="col-5 col-lg-5 col-xl-5"><img class="img-fluid" src="<?php echo site_url();?>resources/content/photo-g.jpg" alt=""></div>
              </div>
            </div>
          </div>
          <div class="testing-block">
            <div class="testing-block-pic"><img class="img-fluid" src="<?php echo site_url();?>resources/content/photo-article2.jpg" alt=""></div>
            <div class="container">
              <div class="row">
                <div class="col-5 col-lg-5 col-xl-5">
                  <h3>тестирование</h3>
                  <p>Проверьте свои знания относительно Зальцбурга в нашем тестировании. Тестирование покажет, насколько            хорошо вы подготовлены для продажи туров по этому направлению, а также даст вам преимущество перед другими, так как результатами тестирования можно поделиться в социальных сетях, а мы подтвердим что вы действительно успешно справились со всеми вопросам. </p><a class="btn-primary" href="#">начать тест     </a>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
<?php $this->load->view("footer"); ?>  
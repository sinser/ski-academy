﻿<?php $this->load->view("header"); 
$this->db->from("regions");
$this->db->where("id", $region_id);
$data['regions'] = $this->db->get()->result_array();
foreach ($data['regions'] as $newregions):
		$regionName = $newregions['name'];
		$country_id = $newregions['country_id'];
		$region_image = $newregions['region_image'];
		$page_title = $newregions['page_title'];
		$main_block_left = $newregions['main_block_left'];
		$main_block_right = $newregions['main_block_right'];
		
				$this->db->from("countries");
				$this->db->where("id", $country_id);
				$data['country'] = $this->db->get()->result_array();
				foreach ($data['country'] as $newcountry):
						$countryName = $newcountry['name'];
				endforeach;
endforeach;

?> 
      <div class="content">
        <div class="wrap-page"></div>
        <section class="country" >
		
          <div class="container">
            <div class="country-header">
               <h1><?php echo $countryName;?></h1>
              <div class="country-header-subtitle"><?php echo $regionName;?></div>
            </div>
			<?php
				$this->db->from("resorts");
				$this->db->where("region_id", $region_id);
				$this->db->order_by("priority", "asc");
				$data['resorts'] = $this->db->get()->result_array();
				foreach ($data['resorts'] as $newresorts):
				
						
				?>
				 <div class="country-map">
				  <div class="country-map-header">
					<h3><?php echo $newresorts['name']; ?></h3><a class="btn-primary w-font" href="<?php echo site_url(); ?>resort/testing/<?php echo $newresorts['id']; ?>">Далее</a>
				  </div>
				   <div class="country-map-row">
						<img src="<?php echo site_url();?>uploads/resorts/<?php echo $newresorts['image']; ?>" style="width:100%">
				  </div>
				  <?php if($newresorts['map']){ ?>
					  <div class="country-map-row">
					  <iframe src="<?php echo $newresorts['map']; ?>" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
					  </div>
				  <?php } ?>
				</div>
				<?php
				endforeach;
			?>
           
          </div>
          
        </section>
      </div>
<?php $this->load->view("footer"); ?> 
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {
	
function index(){
	$this->load->view('adminpanel/adminpanel');
}





function account_password(){
	$this->load->view('adminpanel/account_password');
}
private function Redirect($URL){
	echo "
		<script language='JavaScript' type='text/javascript'>
			
					<!-- 
					function GoNah(){ 
					 location='".site_url($URL)."'; 
					} 
					setTimeout( 'GoNah()', 100 ); 
					//--> 
					</script>	
	";
}
private function changePriorityUp($ID, $PARENT_ID = "", $PARENT_CONDITION = "", $TABLE, $REDIRECT, $ORDER){
	
	$this->db->from($TABLE);
	$this->db->where("id", $ID); 
	if($PARENT_CONDITION != "" &&  $PARENT_ID != ""){
		$this->db->where($PARENT_CONDITION, $PARENT_ID); 
	}
	
	$data['seasons'] = $this->db->get()->result_array();
	foreach ($data['seasons'] as $newseasons):
		 $THIS_LEAGUE_PRIORITY = $newseasons['priority'];
	endforeach;
	
	$findLeague=0;
	$nextLeaguePriority=0;
	
	if($ORDER == "up"){
		for($i=$THIS_LEAGUE_PRIORITY;$i>=0;$i--){
				$nextPR = $i-1;
				$this->db->from($TABLE);
				$this->db->where("priority", $nextPR); 
				if($PARENT_CONDITION != "" &&  $PARENT_ID != ""){
					$this->db->where($PARENT_CONDITION, $PARENT_ID); 
				}
				$data['seasons'] = $this->db->get()->result_array();
					foreach ($data['seasons'] as $newseasons):
						$nextLeaguePriority = $newseasons['priority'];
						$nextLeagueId = $newseasons['id'];
						$findLeague++;

					endforeach;
					if($findLeague!=0){break;}
		}
	}else{
		for($i=$THIS_LEAGUE_PRIORITY;$i<=9999;$i++){
				$nextPR = $i+1;
				$this->db->from($TABLE);
				$this->db->where("priority", $nextPR); 
				if($PARENT_CONDITION != "" &&  $PARENT_ID != ""){
					$this->db->where($PARENT_CONDITION, $PARENT_ID); 
				}
				$data['seasons'] = $this->db->get()->result_array();
					foreach ($data['seasons'] as $newseasons):
						$nextLeaguePriority = $newseasons['priority'];
						$nextLeagueId = $newseasons['id'];
						$findLeague++;

					endforeach;
					if($findLeague!=0){break;}
		}
	}
	
	$THIS_LEAGUE['priority'] = $nextLeaguePriority;
	$this->db->where('id', $ID);
	if($PARENT_CONDITION != "" &&  $PARENT_ID != ""){
		$this->db->where($PARENT_CONDITION, $PARENT_ID); 
	}
	$this->db->update($TABLE,$THIS_LEAGUE);
	
	
	$UP_LEAGUE['priority'] = $THIS_LEAGUE_PRIORITY;
	$this->db->where('id', $nextLeagueId);
	if($PARENT_CONDITION != "" &&  $PARENT_ID != ""){
		$this->db->where($PARENT_CONDITION, $PARENT_ID); 
	}
	$this->db->update($TABLE,$UP_LEAGUE);
	
	$this->Redirect($REDIRECT);
	
}

private function changePriorityDown(){
	
}

//----------------------------------------------------E_MAIL
function chngeEmail()
  {
    $isset = 0;

	$this->db->from("users");
    $this->db->where("user_email",$_POST['NEWEMAIL']);
      $userData['user'] = $this->db->get()->result_array();
      
      foreach ($userData['user'] as $user):
          $isset++;
      endforeach;

            	if ( $isset > 0 )
            		{echo "Данный email занят";}
	            else {
	            		if ( $_POST['PASS'] != $_POST['HIDDENPASS'] )
	            		{ echo "Введен неверный пароль"; }
		            	else 
		            	{echo "Верно";} 
	            		
	            	}                        
}

function delCountry($id){
		$this->db->where('id',$id);
		$this->db->delete('countries');	
		
		$this->db->where('country_id',$id);
		$this->db->delete('regions');	
		
		$this->Redirect("admin/countries/");
		
	}
function delRegionSlide($id){
		$this->db->where('id',$id);
		$this->db->delete('region_slides');	
		
		
		$this->Redirect("admin/regions/");
		
	}
	function delSlide($id){
		$this->db->where('id',$id);
		$this->db->delete('resort_slides');	
		
		
		$this->Redirect("admin/resorts/");
		
	}
	
function delQuiz($id){
		$this->db->where('id',$id);
		$this->db->delete('quiz');	
		
		$this->Redirect("admin/quiz/");
	}
	

function delRegion($id) {
		$this->db->where('id',$id);
		$this->db->delete('regions');	
		
		$this->db->where('region_id',$id);
		$this->db->delete('resorts');	
		$this->Redirect("admin/regions/");
		
	}
	
function delQuestion($id){
		$this->db->where('id',$id);
		$this->db->delete('quiz_questions');	
		$this->Redirect("admin/quiz/");
	}
	
	
function delResorts($id) {
		$this->db->where('id',$id);
		$this->db->delete('resorts');	
		$this->Redirect("admin/resorts/");
	}
	

	
function enter()
	{
		
		$this->load->database();
		$data = array();
		
		$login=$_POST['login'];
		$pass=$_POST['pass'];

		$i=0;

		$this->db->from("users");
		$this->db->where("user_email",$login);
		$this->db->where("user_status","admin");
		$data['admin'] = $this->db->get()->result_array();

		foreach ($data['admin'] as $newusers):
			if($newusers['user_email']){
				$i=1;
			}
		endforeach;

		


		$this->db->from("users");
		$this->db->where("user_password",$pass);
		$this->db->where("user_status","admin");
		$data['pass'] = $this->db->get()->result_array();

		foreach ($data['pass'] as $newusers):
			if($newusers['user_password']){
				$i=1;
			}
		endforeach;
		

		if($i==1){
				if($newusers['user_password']==$pass){
					$newdata = array(
                   'username'  => $newusers['user_id'],
                   'logged_in' => TRUE
               );

					$this->session->set_userdata($newdata);
					
			$session_id = $this->session->userdata('username');
					$this->Redirect("admin/controlpanel/");
					
				}else{redirect( base_url());}
					 		}else{redirect( base_url());}


	}


function main_page(){
	$this->load->view('adminpanel/main_page');
}

	
function edit_main_page()
	{
		$data['title']=$_POST['title'];
		$data['description']=$_POST['description'];
		
		$config['upload_path'] = './uploads/main_page';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '0';
		$config['max_width']  = '0';
		$config['max_height']  = '0';
		$config['encrypt_name']= TRUE;
		
		$this->load->library('upload', $config);

		$this->upload->do_upload('main_image');
		$upload_data = $this->upload->data();
		 $data['image'] = $upload_data['file_name'];

		if($data['image'] == ""){
			$data['image'] = $_POST['hiddenImage'];
		}
		
		
		$this->db->where('id', $_POST['hiddenId']);
		$this->db->update('main_page',$data);


		$this->Redirect("admin/main_page/");
		
	}

function controlpanel(){
	$this->load->view('adminpanel/controlpanel');
}

function options(){
	$this->load->view('adminpanel/options');
}
function users(){
	$this->load->view('adminpanel/users');
}	
	
function quiz(){
	$this->db->from("resorts");
	$this->db->order_by("priority", "asc"); 
	$data['quiz'] = $this->db->get()->result_array();
	$this->load->view('adminpanel/quiz' ,$data);
}


function edit_quiz($id)
	{
		$data['quiz_id'] = $id;
		$this->load->view('adminpanel/edit_quiz' ,$data);

	}
function edit_quiz_item(){
	$data['question']=$_POST['question'];
	$this->db->where('id', $_POST['hiddenId']);
	$this->db->update('quiz',$data);


	$this->Redirect("admin/show_test/".$_POST['resort_id']);
}
	
function add_test_to_resort($resort_id){
	$data['resort_id'] = $resort_id;
	$this->load->view('adminpanel/add_test_to_resort' , $data );
}
function add_test_item_to_resort(){
	$this->db->from("quiz");
	$this->db->select_max('priority');
    $result = $this->db->get()->result_array();
	$res = $result[0]['priority'];
	
	$data['question']=$_POST['question'];
	$data['resort_id']=$_POST['resort_id'];
	if($data['question']=="" ){
		echo "Заполните все поля
				<input type=\"button\"  value=\"Вернуться\" onclick=\"history.back()\">
				";die;
	}

	$data['priority' ] =  $res + 10;
	$this->db->insert('quiz',$data);
	
	$this->Redirect("admin/show_test/".$_POST['resort_id']);
}

function show_test($id){
	
	$data['resort_id'] = $id;
	
	$this->db->from("resorts");
	$this->db->where("id", $id); 
	$data['asd'] = $this->db->get()->result_array();
	foreach ($data['asd'] as $newasd):
		$data['resort_name'] = $newasd['name'];
	endforeach;
	
	$this->db->from("quiz");
	$this->db->where("resort_id", $id); 
	$this->db->order_by("priority", "asc"); 
	$data['show_test'] = $this->db->get()->result_array();
	$this->load->view('adminpanel/show_test' ,$data);
}
function show_regions($id){
	
	$this->db->from("countries");
	$this->db->where("id", $id); 
	$data['asd'] = $this->db->get()->result_array();
	foreach ($data['asd'] as $newasd):
		$data['country_name'] = $newasd['name'];
		$data['country_id'] = $newasd['id'];
	endforeach;
	
	$this->db->from("regions");
	$this->db->where("country_id", $id); 
	$this->db->order_by("priority", "asc"); 
	$data['show_regions'] = $this->db->get()->result_array();
	$this->load->view('adminpanel/show_regions' ,$data);
}


function show_resorts($id){
	
	$this->db->from("regions");
	$this->db->where("id", $id); 
	$data['asd'] = $this->db->get()->result_array();
	foreach ($data['asd'] as $newasd):
		$data['region_name'] = $newasd['name'];
		$data['region_id'] = $newasd['id'];
	endforeach;
	
	$this->db->from("resorts");
	$this->db->where("region_id", $id); 
	$this->db->order_by("priority", "asc"); 
	$data['show_resorts'] = $this->db->get()->result_array();
	$this->load->view('adminpanel/show_resorts' ,$data);
}

/****************************************************************Countries ****/
	function countries(){
		$this->db->from("countries");
		$this->db->order_by("priority", "asc"); 
		$data['countries'] = $this->db->get()->result_array();
		$this->load->view('adminpanel/countries' ,$data);
	} 
	function add_countries(){
		$this->load->view('adminpanel/add_countries');
	}
	function edit_countries($id){
		$data['id'] = $id;
		$this->load->view('adminpanel/edit_countries',$data);
	}
	
function edit_countries_item()
	{
		$data['name']=$_POST['name'];
		
		$config['upload_path'] = './uploads/countries';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '0';
		$config['max_width']  = '0';
		$config['max_height']  = '0';
		$config['encrypt_name']= TRUE;
		
		$this->load->library('upload', $config);

		$this->upload->do_upload('country_image');
		$upload_data = $this->upload->data();
		 $data['country_image'] = $upload_data['file_name'];

		if($data['country_image'] == ""){
			$data['country_image'] = $_POST['hiddenImage'];
		}
		
		
		$this->db->where('id', $_POST['hiddenId']);
		$this->db->update('countries',$data);


		$this->Redirect("admin/countries/");
		
	}

function add_countries_item(){

	$this->db->from("countries");
	$this->db->select_max('priority');
    $result = $this->db->get()->result_array();
	$res = $result[0]['priority'];
	
	$data['name']=$_POST['name'];
	
	$config['upload_path'] = './uploads/countries';
	$config['allowed_types'] = 'gif|jpg|png';
	$config['max_size']	= '0';
	$config['max_width']  = '0';
	$config['max_height']  = '0';
	$config['encrypt_name']= TRUE;
	
	$this->load->library('upload', $config);

	$this->upload->do_upload('country_image');
	$upload_data = $this->upload->data();
//	echo$upload_data['file_name'];die;
	 $data['country_image'] = $upload_data['file_name'];
	
	$data['priority' ] =  $res + 10;
	$this->db->insert('countries',$data);
	
	$this->Redirect("admin/countries/");
}


/******************************************************************* Regions*/
	function regions(){
		$this->db->from("regions");
		$this->db->order_by("priority", "asc"); 
		$data['regions'] = $this->db->get()->result_array();
		$this->load->view('adminpanel/regions' ,$data);
	} 
	
	
	function add_regions_in_country($country_id){
		
		$this->db->from("countries");
		$this->db->where("id", $country_id); 
		$data['asd'] = $this->db->get()->result_array();
		foreach ($data['asd'] as $newasd):
			$data['country_name'] = $newasd['name'];
		endforeach;
		
		$data['country_id'] = $country_id;
		$this->load->view('adminpanel/add_regions_in_country',$data);
	}
	
	function add_regions_item_in_country(){
		$this->db->from("regions");
		$this->db->select_max('priority');
		$result = $this->db->get()->result_array();
		$res = $result[0]['priority'];
		
		$data['name']=$_POST['name'];
		$data['country_id']=$_POST['country_id'];	
		//$data['region_image']=$_POST['region_image'];
		$data['page_title']=$_POST['page_title'];
		$data['main_block_left']=$_POST['main_block_left'];
		$data['main_block_right']=$_POST['main_block_right'];
		
		$config['upload_path'] = './uploads/region_images';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '0';
		$config['max_width']  = '0';
		$config['max_height']  = '0';
		$config['encrypt_name']= TRUE;
		
		$this->load->library('upload', $config);

		$this->upload->do_upload('region_image');
		$upload_data = $this->upload->data();
	//	echo$upload_data['file_name'];die;
		 $data['region_image'] = $upload_data['file_name'];
	 
		$data['priority' ] =  $res + 10;
		$this->db->insert('regions',$data);
		
		$latest_id = $this->db->insert_id();
		
		
		/* start uploading images*/
			$configMultiply['upload_path'] = './uploads/region_images_carousel';
			$configMultiply['allowed_types'] = 'jpg|png';
			$configMultiply['max_size']	= '0';
			$configMultiply['max_width'] = '0';
			$configMultiply['max_height'] = '0';
			$configMultiply['encrypt_name'] = TRUE;
			$this->load->library('upload', $configMultiply);
			$this->upload->initialize($configMultiply);
			$temp_files = $_FILES;
			$count = count ($_FILES['carousel_images']['name']);
			$ORDER = 0;
			if ($count >= 1) {
				
				for ($i=0; $i<=$count-1; $i++)
					{
						$ORDER++;
						$_FILES['carousel_images'] = array (
						'name'=>$temp_files['carousel_images']['name'][$i],
						'type'=>$temp_files['carousel_images']['type'][$i],
						'tmp_name'=>$temp_files['carousel_images']['tmp_name'][$i],
						'error'=>$temp_files['carousel_images']['error'][$i],
						'size'=>$temp_files['carousel_images']['size'][$i]);
						
						 $temp_files['carousel_images']['name'][$i]."<br>";
						
						$this->upload->do_upload('carousel_images');
						$tmp_data = $this->upload->data();

						
						
						$IMG['image_name'] = $tmp_data['file_name'];
						$IMG['region_id'] = $latest_id;
						$IMG['priority'] = $ORDER;
						$this->db->insert('region_carousel_images',$IMG);
						
						//$files_data[$i]['data'] = $tmp_data['full_path'];
				}

			}
			$empty="";
		  	$this->db->where('image_name',$empty);
			$this->db->delete('region_carousel_images');
		/* end uploading images*/
		
		$this->Redirect("admin/show_regions/".$_POST['country_id']);
	}
	
	function add_regions(){
		$this->load->view('adminpanel/add_regions');
	}
	function edit_regions($id){
		$data['id'] = $id;
		$this->load->view('adminpanel/edit_regions',$data);
	}
	
function edit_regions_item()
	{
		
		$data['name']=$_POST['name'];
		$data['country_id']=$_POST['country_id'];	
		$data['page_title']=$_POST['page_title'];
		$data['main_block_left']=$_POST['main_block_left'];
		$data['main_block_right']=$_POST['main_block_right'];
		
		$config['upload_path'] = './uploads/region_images';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '0';
		$config['max_width']  = '0';
		$config['max_height']  = '0';
		$config['encrypt_name']= TRUE;
		
		$this->load->library('upload', $config);

		$this->upload->do_upload('region_image');
		$upload_data = $this->upload->data();
		 $data['region_image'] = $upload_data['file_name'];

		if($data['region_image'] == ""){
			$data['region_image'] = $_POST['hiddenImage'];
		}
		
		
		$this->db->where('id', $_POST['hiddenId']);
		$this->db->update('regions',$data);
		
		
		$arr[]="";
		$this->db->from("region_carousel_images");
		$this->db->where("region_id",$_POST['hiddenId']);
		$result['resultMax'] = $this->db->get()->result_array();
			foreach ($result['resultMax'] as $newresultMax):
				$arr[]=$newresultMax['priority'];
			endforeach;

		//var_dump($result);
		$MAX =  max($arr);	
		
		
		$config['upload_path'] = './uploads/region_images_carousel';
		$config['allowed_types'] = 'jpg|png';
		$config['max_size']	= '0';
		$config['max_width'] = '0';
		$config['max_height'] = '0';
		$config['encrypt_name'] = TRUE;
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		$temp_files = $_FILES;
		$count = count ($_FILES['carousel_images']['name']);
		//$ORDER = 0;
		for ($i=0; $i<=$count-1; $i++)
			{
				//$ORDER++;
				$_FILES['carousel_images'] = array (
				'name'=>$temp_files['carousel_images']['name'][$i],
				'type'=>$temp_files['carousel_images']['type'][$i],
				'tmp_name'=>$temp_files['carousel_images']['tmp_name'][$i],
				'error'=>$temp_files['carousel_images']['error'][$i],
				'size'=>$temp_files['carousel_images']['size'][$i]);
				
				 $temp_files['carousel_images']['name'][$i]."<br>";
				
				$this->upload->do_upload('carousel_images');
				$tmp_data = $this->upload->data();
				
				$IMG['image_name'] = $tmp_data['file_name'];
				$IMG['region_id'] = $_POST['hiddenId'];
				$IMG['priority'] = $MAX+$i;
				$this->db->insert('region_carousel_images',$IMG); 
				//$files_data[$i]['data'] = $tmp_data['full_path'];
		}

		$empty="";
		$this->db->where('image_name',$empty);
		$this->db->delete('region_carousel_images');


		$this->Redirect("admin/regions/");
	}
function delImageProduct($id){
	$this->db->where('id',$id);
	$this->db->delete('region_carousel_images');

		echo "
			<script language='JavaScript' type='text/javascript'>
			
						<!-- 
						function GoNah(){ 
						  location='".$_SERVER['HTTP_REFERER']."'; 
						} 
						setTimeout( 'GoNah()', 100 ); 
						//--> 
						</script>	
		";
}
function add_regions_item(){

	$this->db->from("regions");
	$this->db->select_max('priority');
    $result = $this->db->get()->result_array();
	$res = $result[0]['priority'];
	
	$data['name']=$_POST['name'];
	$data['country_id']=$_POST['country_id'];	
	
	
	$data['page_title']=$_POST['page_title'];
	$data['main_block_left']=$_POST['main_block_left'];
	$data['main_block_right']=$_POST['main_block_right'];
	
	$config['upload_path'] = './uploads/region_images';
	$config['allowed_types'] = 'gif|jpg|png';
	$config['max_size']	= '0';
	$config['max_width']  = '0';
	$config['max_height']  = '0';
	$config['encrypt_name']= TRUE;
	
	$this->load->library('upload', $config);

	$this->upload->do_upload('region_image');
	$upload_data = $this->upload->data();
//	echo$upload_data['file_name'];die;
	 $data['region_image'] = $upload_data['file_name'];
	 
	 
	if($data['name']=="" ){
		echo "Заполните все поля
				<input type=\"button\"  value=\"Вернуться\" onclick=\"history.back()\">
				";die;
	}

	$data['priority' ] =  $res + 10;
	$this->db->insert('regions',$data);
	$latest_id = $this->db->insert_id();
	
	/* start uploading images*/
			$configMultiply['upload_path'] = './uploads/region_images_carousel';
			$configMultiply['allowed_types'] = 'jpg|png';
			$configMultiply['max_size']	= '0';
			$configMultiply['max_width'] = '0';
			$configMultiply['max_height'] = '0';
			$configMultiply['encrypt_name'] = TRUE;
			$this->load->library('upload', $configMultiply);
			$this->upload->initialize($configMultiply);
			$temp_files = $_FILES;
			$count = count ($_FILES['carousel_images']['name']);
			$ORDER = 0;
			if ($count >= 1) {
				
				for ($i=0; $i<=$count-1; $i++)
					{
						$ORDER++;
						$_FILES['carousel_images'] = array (
						'name'=>$temp_files['carousel_images']['name'][$i],
						'type'=>$temp_files['carousel_images']['type'][$i],
						'tmp_name'=>$temp_files['carousel_images']['tmp_name'][$i],
						'error'=>$temp_files['carousel_images']['error'][$i],
						'size'=>$temp_files['carousel_images']['size'][$i]);
						
						 $temp_files['carousel_images']['name'][$i]."<br>";
						
						$this->upload->do_upload('carousel_images');
						$tmp_data = $this->upload->data();

						
						
						$IMG['image_name'] = $tmp_data['file_name'];
						$IMG['region_id'] = $latest_id;
						$IMG['priority'] = $ORDER;
						$this->db->insert('region_carousel_images',$IMG);
						
						//$files_data[$i]['data'] = $tmp_data['full_path'];
				}

			}
			$empty="";
		  	$this->db->where('image_name',$empty);
			$this->db->delete('region_carousel_images');
		/* end uploading images*/
	
	$this->Redirect("admin/regions/");
}


/******************************************************************* Resorts*/
	function resorts(){
		$this->db->from("resorts");
		$this->db->order_by("priority", "asc"); 
		$data['resorts'] = $this->db->get()->result_array();
		$this->load->view('adminpanel/resorts' ,$data);
	} 
	function add_resorts(){
		$this->load->view('adminpanel/add_resorts');
	}
	function edit_resorts($id){
		$data['id'] = $id;
		$this->load->view('adminpanel/edit_resorts',$data);
	}
	
function edit_resorts_item()
	{
		$data['name']=$_POST['name'];
		$data['map']=$_POST['map'];
		$data['region_id']=$_POST['region_id'];	
		$data['description']=$_POST['description'];	
		
		
		$config['upload_path'] = './uploads/resorts';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '0';
		$config['max_width']  = '0';
		$config['max_height']  = '0';
		$config['encrypt_name']= TRUE;
		
		$this->load->library('upload', $config);

		$this->upload->do_upload('resort_image');
		$upload_data = $this->upload->data();
		 $data['image'] = $upload_data['file_name'];

		if($data['image'] == ""){
			$data['image'] = $_POST['hiddenImage'];
		}
	
	
	
		$this->db->where('id', $_POST['hiddenId']);
		$this->db->update('resorts',$data);
		
		
		
		$arr[]="";
		$count_images = 0;
		$this->db->from("resort_images");
		$this->db->where("resort_id",$_POST['hiddenId']);
		$result['resultMax'] = $this->db->get()->result_array();
			foreach ($result['resultMax'] as $newresultMax):
				$arr[]=$newresultMax['priority'];
				$count_images++;
			endforeach;

		//var_dump($result);
		if($count_images == 0){
			$MAX = 0;
		}else{
			$MAX =  max($arr);
		}
		
		/* start uploading images*/
			$configMultiply['upload_path'] = './uploads/resorts_images';
			$configMultiply['allowed_types'] = 'jpg|png';
			$configMultiply['max_size']	= '0';
			$configMultiply['max_width'] = '0';
			$configMultiply['max_height'] = '0';
			$configMultiply['encrypt_name'] = TRUE;
			$this->load->library('upload', $configMultiply);
			$this->upload->initialize($configMultiply);
			$temp_files = $_FILES;
			$count = count ($_FILES['carousel_images']['name']);
			//$ORDER = 0;
			if ($count >= 1) {
				
				for ($i=0; $i<=$count-1; $i++)
					{
						//$ORDER++;
						$_FILES['carousel_images'] = array (
						'name'=>$temp_files['carousel_images']['name'][$i],
						'type'=>$temp_files['carousel_images']['type'][$i],
						'tmp_name'=>$temp_files['carousel_images']['tmp_name'][$i],
						'error'=>$temp_files['carousel_images']['error'][$i],
						'size'=>$temp_files['carousel_images']['size'][$i]);
						
						 $temp_files['carousel_images']['name'][$i]."<br>";
						
						$this->upload->do_upload('carousel_images');
						$tmp_data = $this->upload->data();

						
						
						$IMG['image_name'] = $tmp_data['file_name'];
						$IMG['resort_id'] = $_POST['hiddenId'];
						$IMG['priority'] = $MAX+$i;
						$this->db->insert('resort_images',$IMG);
						
						//$files_data[$i]['data'] = $tmp_data['full_path'];
				}

			}
			$empty="";
		  	$this->db->where('image_name',$empty);
			$this->db->delete('resort_images');
		/* end uploading images*/


		$this->Redirect("admin/resorts/");
		
	}
	
	function add_resorts_in_region($region_id){
		
		$this->db->from("regions");
		$this->db->where("id", $region_id); 
		$data['asd'] = $this->db->get()->result_array();
		foreach ($data['asd'] as $newasd):
			$data['region_name'] = $newasd['name'];
		endforeach;
		
		$data['region_id'] = $region_id;
		$this->load->view('adminpanel/add_resorts_in_region',$data);
	}
	function add_slide_in_resort($resort_id){
		
		$this->db->from("resorts");
		$this->db->where("id", $resort_id); 
		$data['asd'] = $this->db->get()->result_array();
		foreach ($data['asd'] as $newasd):
			$data['resort_name'] = $newasd['name'];
		endforeach;
		
		$data['resort_id'] = $resort_id;
		$this->load->view('adminpanel/add_slide_in_resort',$data);
	}
	
	function add_slide_in_region($region_id){
		
		$this->db->from("regions");
		$this->db->where("id", $region_id); 
		$data['asd'] = $this->db->get()->result_array();
		foreach ($data['asd'] as $newasd):
			$data['region_name'] = $newasd['name'];
		endforeach;
		
		$data['region_id'] = $region_id;
		$this->load->view('adminpanel/add_slide_in_region',$data);
	}
	
	

function add_resorts_item(){

	$this->db->from("resorts");
	$this->db->select_max('priority');
    $result = $this->db->get()->result_array();
	$res = $result[0]['priority'];
	
	$data['name']=$_POST['name'];
	$data['map']=$_POST['map'];
	$data['region_id']=$_POST['region_id'];	
	if($data['name']=="" ){
		echo "Заполните все поля
				<input type=\"button\"  value=\"Вернуться\" onclick=\"history.back()\">
				";die;
	}
	
	$config['upload_path'] = './uploads/resorts';
	$config['allowed_types'] = 'gif|jpg|png';
	$config['max_size']	= '0';
	$config['max_width']  = '0';
	$config['max_height']  = '0';
	$config['encrypt_name']= TRUE;
	
	$this->load->library('upload', $config);

	$this->upload->do_upload('resort_image');
	$upload_data = $this->upload->data();
	 $data['image'] = $upload_data['file_name'];

	$data['priority' ] =  $res + 10;
	$this->db->insert('resorts',$data);
	$latest_id = $this->db->insert_id();
	
	/* start uploading images*/
		$configMultiply['upload_path'] = './uploads/resorts_images';
		$configMultiply['allowed_types'] = 'jpg|png';
		$configMultiply['max_size']	= '0';
		$configMultiply['max_width'] = '0';
		$configMultiply['max_height'] = '0';
		$configMultiply['encrypt_name'] = TRUE;
		$this->load->library('upload', $configMultiply);
		$this->upload->initialize($configMultiply);
		$temp_files = $_FILES;
		$count = count ($_FILES['carousel_images']['name']);
		$ORDER = 0;
		if ($count >= 1) {
			
			for ($i=0; $i<=$count-1; $i++)
				{
					$ORDER++;
					$_FILES['carousel_images'] = array (
					'name'=>$temp_files['carousel_images']['name'][$i],
					'type'=>$temp_files['carousel_images']['type'][$i],
					'tmp_name'=>$temp_files['carousel_images']['tmp_name'][$i],
					'error'=>$temp_files['carousel_images']['error'][$i],
					'size'=>$temp_files['carousel_images']['size'][$i]);
					
					 $temp_files['carousel_images']['name'][$i]."<br>";
					
					$this->upload->do_upload('carousel_images');
					$tmp_data = $this->upload->data();

					
					
					$IMG['image_name'] = $tmp_data['file_name'];
					$IMG['resort_id'] = $latest_id;
					$IMG['priority'] = $ORDER;
					$this->db->insert('resort_images',$IMG);
					
					//$files_data[$i]['data'] = $tmp_data['full_path'];
			}

		}
		$empty="";
		$this->db->where('image_name',$empty);
		$this->db->delete('resort_images');
	/* end uploading images*/
	
	$this->Redirect("admin/resorts/");
}


function add_resorts_item_in_region(){

	$this->db->from("resorts");
	$this->db->select_max('priority');
    $result = $this->db->get()->result_array();
	$res = $result[0]['priority'];
	
	$data['name']=$_POST['name'];
	$data['map']=$_POST['map'];
	$data['region_id']=$_POST['region_id'];	
	if($data['name']=="" ){
		echo "Заполните все поля
				<input type=\"button\"  value=\"Вернуться\" onclick=\"history.back()\">
				";die;
	}

	$data['priority' ] =  $res + 10;
	$this->db->insert('resorts',$data);
	$latest_id = $this->db->insert_id();
	
	/* start uploading images*/
		$configMultiply['upload_path'] = './uploads/resorts_images';
		$configMultiply['allowed_types'] = 'jpg|png';
		$configMultiply['max_size']	= '0';
		$configMultiply['max_width'] = '0';
		$configMultiply['max_height'] = '0';
		$configMultiply['encrypt_name'] = TRUE;
		$this->load->library('upload', $configMultiply);
		$this->upload->initialize($configMultiply);
		$temp_files = $_FILES;
		$count = count ($_FILES['carousel_images']['name']);
		$ORDER = 0;
		if ($count >= 1) {
			
			for ($i=0; $i<=$count-1; $i++)
				{
					$ORDER++;
					$_FILES['carousel_images'] = array (
					'name'=>$temp_files['carousel_images']['name'][$i],
					'type'=>$temp_files['carousel_images']['type'][$i],
					'tmp_name'=>$temp_files['carousel_images']['tmp_name'][$i],
					'error'=>$temp_files['carousel_images']['error'][$i],
					'size'=>$temp_files['carousel_images']['size'][$i]);
					
					 $temp_files['carousel_images']['name'][$i]."<br>";
					
					$this->upload->do_upload('carousel_images');
					$tmp_data = $this->upload->data();

					
					
					$IMG['image_name'] = $tmp_data['file_name'];
					$IMG['resort_id'] = $latest_id;
					$IMG['priority'] = $ORDER;
					$this->db->insert('resort_images',$IMG);
					
					//$files_data[$i]['data'] = $tmp_data['full_path'];
			}

		}
		$empty="";
		$this->db->where('image_name',$empty);
		$this->db->delete('resort_images');
	/* end uploading images*/
	
	$this->Redirect("admin/resorts/");
}


function edit_slide($id){
	$data['id'] = $id;
	$this->load->view('adminpanel/edit_slide',$data);
}

function edit_region_slide($id){
	$data['id'] = $id;
	$this->load->view('adminpanel/edit_region_slide',$data);
}
function add_slide_item_in_resort(){

	$this->db->from("resort_slides");
	$this->db->select_max('priority');
	$this->db->where('resort_id',$_POST['resort_id'] );
    $result = $this->db->get()->result_array();
	$res = $result[0]['priority'];
	
	$data['title']=$_POST['title'];
	$data['description']=$_POST['description'];
	$data['resort_id']=$_POST['resort_id'];	
	
	
	$config['upload_path'] = './uploads/resort_slides';
	$config['allowed_types'] = 'gif|jpg|png';
	$config['max_size']	= '0';
	$config['max_width']  = '0';
	$config['max_height']  = '0';
	$config['encrypt_name']= TRUE;
	
	$this->load->library('upload', $config);

	$this->upload->do_upload('slide_image');
	$upload_data = $this->upload->data();
//	echo$upload_data['file_name'];die;
	 $data['image'] = $upload_data['file_name'];

	$data['priority' ] =  $res + 10;
	$this->db->insert('resort_slides',$data);
	
	$this->Redirect("admin/show_slides/".$_POST['resort_id']);
}


function add_slide_item_in_region(){

	$this->db->from("region_slides");
	$this->db->select_max('priority');
	$this->db->where('region_id',$_POST['region_id'] );
    $result = $this->db->get()->result_array();
	$res = $result[0]['priority'];
	
	$data['title']=$_POST['title'];
	$data['description']=$_POST['description'];
	$data['region_id']=$_POST['region_id'];	
	$data['image_position']=$_POST['image_position'];	
	
	$config['upload_path'] = './uploads/region_slides';
	$config['allowed_types'] = 'gif|jpg|png';
	$config['max_size']	= '0';
	$config['max_width']  = '0';
	$config['max_height']  = '0';
	$config['encrypt_name']= TRUE;
	
	$this->load->library('upload', $config);

	$this->upload->do_upload('slide_image');
	$upload_data = $this->upload->data();
//	echo$upload_data['file_name'];die;
	 $data['image'] = $upload_data['file_name'];

	$data['priority' ] =  $res + 10;
	$this->db->insert('region_slides',$data);
	
	$this->Redirect("admin/show_region_slides/".$_POST['region_id']);
}

function edit_slide_item_in_resort(){

	
	$data['title']=$_POST['title'];
	$data['description']=$_POST['description'];
	$data['resort_id']=$_POST['resort_id'];	
	
	
	$config['upload_path'] = './uploads/resort_slides';
	$config['allowed_types'] = 'gif|jpg|png';
	$config['max_size']	= '0';
	$config['max_width']  = '0';
	$config['max_height']  = '0';
	$config['encrypt_name']= TRUE;
	
	$this->load->library('upload', $config);

	$this->upload->do_upload('slide_image');
	$upload_data = $this->upload->data();
	 $data['image'] = $upload_data['file_name'];

	if($data['image'] == ""){
		$data['image'] = $_POST['hiddenImage'];
	}
	$this->db->where('id', $_POST['hiddenId']);
	$this->db->update('resort_slides',$data);
	
	$this->Redirect("admin/show_slides/".$_POST['resort_id']);
}

function edit_slide_item_in_region(){

	
	$data['title']=$_POST['title'];
	$data['description']=$_POST['description'];
	$data['region_id']=$_POST['region_id'];	
	$data['image_position']=$_POST['image_position'];	
	
	$config['upload_path'] = './uploads/region_slides';
	$config['allowed_types'] = 'gif|jpg|png';
	$config['max_size']	= '0';
	$config['max_width']  = '0';
	$config['max_height']  = '0';
	$config['encrypt_name']= TRUE;
	
	$this->load->library('upload', $config);

	$this->upload->do_upload('slide_image');
	$upload_data = $this->upload->data();
	 $data['image'] = $upload_data['file_name'];

	if($data['image'] == ""){
		$data['image'] = $_POST['hiddenImage'];
	}
	$this->db->where('id', $_POST['hiddenId']);
	$this->db->update('region_slides',$data);
	
	$this->Redirect("admin/show_region_slides/".$_POST['region_id']);
}

function show_slides($resort_id){
	
		$this->db->from("resort_slides");
		$this->db->where("resort_id", $resort_id); 
		$this->db->order_by("priority", "asc"); 
		$data['resort_slides'] = $this->db->get()->result_array();
		
		$data['resort_id'] = $resort_id;
		
		$this->db->from("resorts");
		$this->db->where("id", $resort_id); 
		$data['asd'] = $this->db->get()->result_array();
		foreach ($data['asd'] as $newasd):
			$data['resort_name'] = $newasd['name'];
		endforeach;
		
		$this->load->view('adminpanel/show_slides',$data);
}


function show_region_slides($region_id){
	
		$this->db->from("region_slides");
		$this->db->where("region_id", $region_id); 
		$this->db->order_by("priority", "asc"); 
		$data['region_slides'] = $this->db->get()->result_array();
		
		$data['region_id'] = $region_id;
		
		$this->db->from("regions");
		$this->db->where("id", $region_id); 
		$data['asd'] = $this->db->get()->result_array();
		foreach ($data['asd'] as $newasd):
			$data['region_name'] = $newasd['name'];
		endforeach;
		
		$this->load->view('adminpanel/show_region_slides',$data);
}
/////////////////////////////////START ANSWERS
function show_questions($quiz_id){
		$this->db->from("quiz_questions");
		$this->db->where("quiz_id", $quiz_id); 
		$this->db->order_by("priority", "asc"); 
		$data['quiz_quiestions'] = $this->db->get()->result_array();
		
		$data['quiz_id'] = $quiz_id;
		
		$this->db->from("quiz");
		$this->db->where("id", $quiz_id); 
		$data['asd'] = $this->db->get()->result_array();
		foreach ($data['asd'] as $newasd):
			$data['test_title'] = $newasd['question'];
		endforeach;
		
		$this->load->view('adminpanel/show_questions',$data);
}

function add_question_to_quiz($quiz_id){
	
		$this->db->from("quiz");
		$this->db->where("id", $quiz_id); 
		$data['asd'] = $this->db->get()->result_array();
		foreach ($data['asd'] as $newasd):
			$data['quiz_title'] = $newasd['question'];
		endforeach;
		
		$data['quiz_id'] = $quiz_id;
		$this->load->view('adminpanel/add_question_to_quiz',$data);
}

function add_question_item_in_quiz(){
	$this->db->from("quiz_questions");
	$this->db->select_max('priority');
    $result = $this->db->get()->result_array();
	$res = $result[0]['priority'];
	
	$data['question']=$_POST['question'];
	$data['quiz_id']=$_POST['quiz_id'];	
	

	$data['priority' ] =  $res + 10;
	$this->db->insert('quiz_questions',$data);
	
	$this->Redirect("admin/show_questions/".$_POST['quiz_id']);
	
}
function edit_question($id){
	$data['question_id'] = $id;
	$this->load->view('adminpanel/edit_question',$data);
}
function edit_question_item_in_quiz(){
		$data['question']=$_POST['question'];
		$this->db->where('id', $_POST['hiddenId']);
		$this->db->update('quiz_questions',$data);


	$this->Redirect("admin/show_questions/".$_POST['quiz_id']);
}
/////////////////////////////////END ANSWERS

function show_answers($question_id){
	$this->db->from("quiz_answers");
	$this->db->where("question_id", $question_id); 
	$this->db->order_by("priority", "asc"); 
	$data['answers'] = $this->db->get()->result_array();
	
	
	$this->db->from("quiz_questions");
	$this->db->where("id", $question_id); 
	$data['asd'] = $this->db->get()->result_array();
	foreach ($data['asd'] as $newasd):
		$data['question_title'] = $newasd['question'];
	endforeach;
	$data['question_id'] = $question_id;
	$this->load->view('adminpanel/show_answers',$data);
}


function add_answer_item_to_question(){
	$answer = $_POST['answer'];
	$question_id = $_POST['question_id'];
	
	$this->db->from("quiz_answers");
	$this->db->where("question_id", $question_id);
	$this->db->select_max('priority');
    $result = $this->db->get()->result_array();
	$res = $result[0]['priority'];
	
	$data['answer']=$answer;
	$data['question_id']=$question_id;	
	

	$data['priority' ] =  $res + 10;
	$this->db->insert('quiz_answers',$data);
	
	$this->Redirect("admin/show_answers/".$question_id);
}

function edit_all_answers(){
	$correct_arr = count($_POST['correct']);
	$answers = $_POST['answers'];
	if($correct_arr > 0){
		foreach($answers as $answer_id){ // сначала убираем у всех ответов галку, что они правильные
			$dataA['correct'] = 0;
			$this->db->where('id', $answer_id);
			$this->db->update('quiz_answers',$dataA);
		}
		foreach($_POST['correct'] as $correct_answer_id){//а здесь ставим галку нужным выделенным
			$data['correct'] = 1;
			$this->db->where('id', $correct_answer_id);
			$this->db->update('quiz_answers',$data);
		}
		$this->Redirect("admin/show_answers/".$_POST['question_id']);
	}
}

function edit_answer($id){
	
	$this->db->from("quiz_answers");
	$this->db->where("id", $id); 
	$data['asd'] = $this->db->get()->result_array();
	foreach ($data['asd'] as $newasd):
		$data['question_id'] = $newasd['question_id'];
	endforeach;
	
	
	$data['answer_id'] = $id;
	$this->load->view('adminpanel/edit_answer',$data);
}

function edit_answer_item(){
	$data['answer']=$_POST['answer'];
	$this->db->where('id', $_POST['hiddenId']);
	$this->db->update('quiz_answers',$data);

	$this->Redirect("admin/show_answers/".$_POST['question_id']);
}

//start countries order
function countriesup($ID){
	$this->changePriorityUp($ID, null, "id", "countries", "admin/countries/", "up");	
}
function countriesdown($ID){
	$this->changePriorityUp($ID, null, "id", "countries", "admin/countries/", "down");
}
//end countries order

//start regions order
function regionsup($ID, $COUNTRY_ID){
	$this->changePriorityUp($ID, $COUNTRY_ID, "country_id", "regions", "admin/show_regions/".$COUNTRY_ID, "up");	
}
function regionsdown($ID, $COUNTRY_ID){
	$this->changePriorityUp($ID, $COUNTRY_ID, "country_id", "regions", "admin/show_regions/".$COUNTRY_ID, "down");		
}
//end regions order



//start resorts order
function resortsup($ID, $COUNTRY_ID){
	$this->changePriorityUp($ID, $COUNTRY_ID, "region_id", "resorts", "admin/show_resorts/".$COUNTRY_ID, "up");
}
function resortsdown($ID, $COUNTRY_ID){
	$this->changePriorityUp($ID, $COUNTRY_ID, "region_id", "resorts", "admin/show_resorts/".$COUNTRY_ID, "down");	
}
//end resorts order




//start slides order
function slideup($ID, $RESORT_ID){
	$this->changePriorityUp($ID, $RESORT_ID, "resort_id", "resort_slides", "admin/show_slides/".$RESORT_ID, "up");	
}
function slidedown($ID, $RESORT_ID){
	$this->changePriorityUp($ID, $RESORT_ID, "resort_id", "resort_slides", "admin/show_slides/".$RESORT_ID, "down");		
}
//end slides order

//start quiz order
function quizup($ID, $RESORT_ID){
	$this->changePriorityUp($ID, $RESORT_ID, "resort_id", "quiz", "admin/show_test/".$RESORT_ID, "up");
}
function quizdown($ID, $RESORT_ID){
	$this->changePriorityUp($ID, $RESORT_ID, "resort_id", "quiz", "admin/show_test/".$RESORT_ID, "down");
}
//end quiz order


//start questions order
function questionup($ID, $QUIZ_ID){
	$this->changePriorityUp($ID, $QUIZ_ID, "quiz_id", "quiz_questions", "admin/show_questions/".$QUIZ_ID, "up");
}
function questiondown($ID, $QUIZ_ID){
 $this->changePriorityUp($ID, $QUIZ_ID, "quiz_id", "quiz_questions", "admin/show_questions/".$QUIZ_ID, "down");
}
//end quiz questions order
function answernup($ID, $QUESTION_ID){
	 $this->changePriorityUp($ID, $QUESTION_ID, "question_id", "quiz_answers", "admin/show_answers/".$QUESTION_ID, "up");
}
function answerdown($ID, $QUESTION_ID){
	 $this->changePriorityUp($ID, $QUESTION_ID, "question_id", "quiz_answers", "admin/show_answers/".$QUESTION_ID, "down");
}

function regionslideup($ID, $REGION_ID){
	 $this->changePriorityUp($ID, $REGION_ID, "region_id", "region_slides", "admin/show_region_slides/".$REGION_ID, "up");
}
function regionslidedown($ID, $REGION_ID){
	 $this->changePriorityUp($ID, $REGION_ID, "region_id", "region_slides", "admin/show_region_slides/".$REGION_ID, "down");
}

function imageup($ID, $REGION_ID){
	 $this->changePriorityUp($ID, $REGION_ID, "region_id", "region_carousel_images", "admin/edit_regions/".$REGION_ID, "up");
}
function imagedown($ID, $REGION_ID){
	 $this->changePriorityUp($ID, $REGION_ID, "region_id", "region_carousel_images", "admin/edit_regions/".$REGION_ID,  "down");
}


function resort_imagesup($ID, $REGION_ID){
	 $this->changePriorityUp($ID, $REGION_ID, "region_id", "resort_images", "admin/edit_resorts/".$REGION_ID, "up");
}
function resort_imagesdown($ID, $REGION_ID){
	 $this->changePriorityUp($ID, $REGION_ID, "region_id", "resort_images", "admin/edit_resorts/".$REGION_ID,  "down");
}

	function exitt(){
	$this->session->sess_destroy();
			echo "
					<script language='JavaScript' type='text/javascript'>
							<!-- 
							function GoNah(){ 
							location='".site_url()."'; 
							} 
							setTimeout( 'GoNah()', 100 ); 
							//--> 
							</script>	
					";
		}




}

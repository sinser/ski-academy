<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Mycab extends CI_Controller {

	public function __construct()
	  {
		parent::__construct();
		$this->load->library('session');
	  }
	public function index()
	{
		$newdata = array(
				'current_page'  => 'mycab',
		);
		$this->session->set_userdata($newdata);
		$this->load->view('mycab');
	}

}

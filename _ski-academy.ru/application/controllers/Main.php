<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public function __construct()
	  {
		parent::__construct();
		$this->load->library('session');
	  }
  
	public function index()
	{
		$user_id = $this->session->userdata('user_id');
		$newdata = array(
				'current_page'  => 'home',
				'user_id'  => $user_id,
		);
		$this->session->set_userdata($newdata);
		$this->load->view('main');
		
	}

	private function Redirect($URL=""){
		echo "
			<script language='JavaScript' type='text/javascript'>
				
						<!-- 
						function GoNah(){ 
						 location='".site_url($URL)."'; 
						} 
						setTimeout( 'GoNah()', 100 ); 
						//--> 
						</script>	
		";
	}

	function exitt(){
		$this->session->sess_destroy();
		$this->Redirect();
	}


	function registration(){
		$email = $_POST['email'];

		$this->db->from("users");
        $this->db->where("user_email",$email);
        $user = $this->db->get()->row();
        if($user){
            echo "1";
        }else{
			$data['user_name'] = $_POST['name'];
			$data['user_sname'] = $_POST['sname'];
            $data['user_email'] = $_POST['email'];
            $data['user_password'] = $_POST['password'];
			$this->db->insert('users',$data);
			
            $latest_id = $this->db->insert_id();
            $newdata = array(
				'user_id'  => $latest_id,
				'error_login'  => "",
				'logged_in' => TRUE
			);
            $this->session->set_userdata($newdata);
            echo "ok";
        }
	}
	function quiz($quiz_id){
		$quiz['quiz_id'] = $quiz_id;
		$this->load->view('one_quiz', $quiz);
	}
	function quiz_end($quiz_id){
		$quiz['quiz_id'] = $quiz_id;
		$this->load->view('one_quiz_end', $quiz);
	}
	function currentDate(){
		echo $currentDate = date("F j, Y H:i:s");
	}
	function getWidth(){
		
		$user_id = $this->session->userdata('user_id');
		$question_id = $_POST['question_id'];
		$quiz_attempt = $_POST['quiz_attempt'];
		$answer_id = $_POST['answer_id'];
		$quiz_id = $_POST['quiz_id'];
		$currentQuestion = $_POST['currentQuestion']+1;
		
		$allQuestion = 0;
		$this->db->from("quiz_questions");
		$this->db->where("quiz_id", $quiz_id);
		$data['quiz'] = $this->db->get()->result_array();
		foreach ($data['quiz'] as $newquiz):
			$allQuestion++;
		endforeach;


		echo $width = $currentQuestion*100/$allQuestion;
	}
	
	function getCurrentQuestion(){
		
		echo $currentQuestion = $_POST['currentQuestion']+1;
		
	}
	
	function getNextQuestion(){
		$user_id = $this->session->userdata('user_id');
		$question_id = $_POST['question_id'];
		$quiz_attempt = $_POST['quiz_attempt'];
		$answer_id = $_POST['answer_id'];
		$quiz_id = $_POST['quiz_id'];
		$currentQuestion = $_POST['currentQuestion'];

		$allQuestion = 0;
		$this->db->from("quiz_questions");
		$this->db->where("quiz_id", $quiz_id);
		$data['quiz'] = $this->db->get()->result_array();
		foreach ($data['quiz'] as $newquiz):
			$allQuestion++;
		endforeach;		
		
		$dataQ['user_id'] = $user_id;
		$dataQ['quiz_attempt_id'] = $quiz_attempt;
		$dataQ['quiz_id'] = $quiz_id;
		$dataQ['question_id'] = $question_id;
		$dataQ['answer_id'] = $answer_id;
		$this->db->insert('quiz_attempts_answers',$dataQ);
		
		$i = 0;
		if($allQuestion >= $currentQuestion+1){
		
			$this->db->from("quiz_questions");
			$this->db->where("quiz_id", $quiz_id);
			$this->db->limit($currentQuestion+1);
			$data['quiz'] = $this->db->get()->result_array();
			foreach ($data['quiz'] as $newquiz):
				$i++;
				if($i == $currentQuestion+1){
				$question = $newquiz['question'];
				$question_id = $newquiz['id'];
					?>
						 <div class="quiz-question"><?php echo $question;?></div>
						  <div class="row">
							<?php
									$this->db->from("quiz_answers");
									$this->db->order_by("priority","asc");
									$this->db->where("question_id", $question_id);
									$data['answer'] = $this->db->get()->result_array();
									foreach ($data['answer'] as $newanswer):
										?>
											<div class="col-6 col-lg-6 col-xl-6">
												<div class="quiz-button" 
													question_id="<?php echo $newanswer['question_id'];?>" 
													quiz_attempt="<?php echo $quiz_attempt;?>" 
													answer_id="<?php echo $newanswer['id'];?>"
													quiz_id="<?php echo $quiz_id;?>"><?php echo $newanswer['answer'];?>
												</div>
											</div>
							<?php endforeach; ?>
						</div>
					<?php
				}
			endforeach;
		}else{
			echo "end";
		}
	}
}

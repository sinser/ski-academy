<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Region extends CI_Controller {

	public function __construct()
	  {
		parent::__construct();
		$this->load->library('session');
	  }
public function item($region_id)
	{
		
		$this->db->from("regions");
		$this->db->where("id",  $region_id);
		$data['regions'] = $this->db->get()->result_array();
		foreach ($data['regions'] as $newRegions):
					
				$this->db->from("countries");
				$this->db->where("id", $newRegions['country_id']);
				$data['countries'] = $this->db->get()->result_array();
				foreach ($data['countries'] as $newCountries):
						$ID = $newCountries['id'];
				endforeach;
		endforeach;						
								
		$newdata = array(
				'current_page'  => 'country'.$ID ,
		);
		$this->session->set_userdata($newdata);
		$data['region_id'] = $region_id;
		$this->load->view('one-region', $data);
	}
	
	public function next($region_id)
	{
		$this->db->from("regions");
		$this->db->where("id",  $region_id);
		$data['regions'] = $this->db->get()->result_array();
		foreach ($data['regions'] as $newRegions):
					
				$this->db->from("countries");
				$this->db->where("id", $newRegions['country_id']);
				$data['countries'] = $this->db->get()->result_array();
				foreach ($data['countries'] as $newCountries):
						$ID = $newCountries['id'];
				endforeach;
		endforeach;						
								
		$newdata = array(
				'current_page'  => 'country'.$ID ,
		);
		$this->session->set_userdata($newdata);
		
		$data['region_id'] = $region_id;
		$this->load->view('one-region-next', $data);
	}
	
	public function testing($region_id)
	{
		$this->db->from("regions");
		$this->db->where("id",  $region_id);
		$data['regions'] = $this->db->get()->result_array();
		foreach ($data['regions'] as $newRegions):
					
				$this->db->from("countries");
				$this->db->where("id", $newRegions['country_id']);
				$data['countries'] = $this->db->get()->result_array();
				foreach ($data['countries'] as $newCountries):
						$ID = $newCountries['id'];
				endforeach;
		endforeach;						
								
		$newdata = array(
				'current_page'  => 'country'.$ID ,
		);
		$this->session->set_userdata($newdata);
		
		$data['region_id'] = $region_id;
		$this->load->view('one-region-testing', $data);
	}

}

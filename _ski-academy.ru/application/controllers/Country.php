<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Country extends CI_Controller {

	public function __construct()
	  {
		parent::__construct();
		$this->load->library('session');
	  }
public function item($country_id)
	{
		$data['country_id'] = $country_id;
		$this->load->view('one-country', $data);
	}
public function all()
	{
		$this->load->view('all-country');
	}

}

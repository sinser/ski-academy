<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MainModel extends CI_Model {


	
	function pagination($num, $offset)
	{
		//$this->db->limit(5);
	    $this->db->order_by('id','desc');
	    $query = $this->db->get('subcategories',$num, $offset);
	    return $query->result_array();
	}
	
	function paginationProducts($num, $offset)
	{
		//$this->db->limit(5);
	    $this->db->order_by('id','desc');
	    $query = $this->db->get('products',$num, $offset);
	    return $query->result_array();
	}
	function paginationSubSub($num, $offset)
	{
		//$this->db->limit(5);
	    $this->db->order_by('id','desc');
	    $query = $this->db->get('subsubcategories',$num, $offset);
	    return $query->result_array();
	}

	function paginationPortfolio($num, $offset)
	{
		//$this->db->limit(5);
	    $this->db->order_by('id','desc');
	    $query = $this->db->get('portfolio',$num, $offset);
	    return $query->result_array();
	}

	function paginationPortfolioItems($num, $offset)
	{
		//$this->db->limit(5);
	    $this->db->order_by('id','desc');
	    $query = $this->db->get('portfolio_items',$num, $offset);
	    return $query->result_array();
	}

	function paginationModels($num, $offset)
	{
		//$this->db->limit(5);
	    $this->db->order_by('id','desc');
	    $query = $this->db->get('models',$num, $offset);
	    return $query->result_array();
	}

	function paginationModelsItems($num, $offset)
	{
		//$this->db->limit(5);
	    $this->db->order_by('id','desc');
	    $query = $this->db->get('models_items',$num, $offset);
	    return $query->result_array();
	}
	
	
	
	

}
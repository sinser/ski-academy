-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 20, 2020 at 10:47 PM
-- Server version: 5.6.39-83.1
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aeroporter_acade`
--

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `name` varchar(123) NOT NULL,
  `description` varchar(2345) NOT NULL,
  `priority` int(12) NOT NULL,
  `country_image` varchar(123) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `description`, `priority`, `country_image`) VALUES
(1, 'Франция', '', 130, ''),
(4, 'Австрия', '', 140, '');

-- --------------------------------------------------------

--
-- Table structure for table `main_page`
--

CREATE TABLE IF NOT EXISTS `main_page` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `title` varchar(123) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(123) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `main_page`
--

INSERT INTO `main_page` (`id`, `title`, `description`, `image`) VALUES
(1, 'Горнолыжная академия', '<p>На сегодняшний день компания&nbsp;<strong>GOOD TIME TRAVEL&nbsp;</strong>&mdash; крупнейший многопрофильный туроператор Северо-Западного Федерального округа, способный безупречно организовывать увлекательные вояжи по всему миру.</p>\r\n\r\n<p>Два современных офиса в Санкт-Петербурге, офис в Ростове-на-Дону, представительства в Новосибирске и Екатеринбурге.</p>\r\n\r\n<p>Основная цель нашей деятельности заключается в удовлетворении растущих запросов российского туриста с ориентацией на предпочтения знающих и бывалых путешественников. Если у вас осталось мало чистых страниц в загранпаспорте &ndash; мы готовы приятно удивить вас принципиально новыми турами, во время которых даже хорошо знакомые места предстанут перед вами в ином более привлекательном виде.</p>\r\n\r\n<p>Туроператор GOOD TIME TRAVEL &ndash;<br />\r\nс нами можно хоть на Северный Полюс!</p>\r\n', 'cc3b149f19d428d2d3e3ba97cb2c20c1.png');

-- --------------------------------------------------------

--
-- Table structure for table `quiz`
--

CREATE TABLE IF NOT EXISTS `quiz` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `question` text NOT NULL,
  `resort_id` int(12) NOT NULL,
  `priority` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quiz`
--

INSERT INTO `quiz` (`id`, `question`, `resort_id`, `priority`) VALUES
(7, 'Практические знания курорта', 8, 70),
(8, 'Тестирование второе', 8, 80),
(9, 'Тестирование третье', 8, 100),
(10, 'тест 777', 8, 90);

-- --------------------------------------------------------

--
-- Table structure for table `quiz_answers`
--

CREATE TABLE IF NOT EXISTS `quiz_answers` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `answer` varchar(1234) NOT NULL,
  `question_id` int(12) NOT NULL,
  `correct` int(12) NOT NULL DEFAULT '0',
  `priority` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quiz_answers`
--

INSERT INTO `quiz_answers` (`id`, `answer`, `question_id`, `correct`, `priority`) VALUES
(1, 'В магазине', 5, 0, 20),
(2, 'У прохожих перед рецепцией', 5, 0, 40),
(3, 'Брать с собой', 5, 0, 30),
(4, 'Взять в аренду в агенстве', 5, 1, 50),
(5, 'Ответ 1', 6, 0, 10),
(6, 'Ответ 2', 6, 1, 20),
(7, 'Ответ 3', 6, 1, 30),
(8, 'Ответ 4', 6, 0, 40),
(9, 'Ответ 1', 8, 0, 10),
(10, 'Ответ 2', 8, 1, 20),
(11, 'Ответ 3', 8, 0, 30),
(12, 'Ответ 4', 8, 0, 40),
(13, '1Зоны катания Дорфгастайн и Спортгастайн', 9, 0, 10),
(14, '2Все зоны катания и этой долине соединены между собой подъемниками', 9, 1, 20),
(15, '4Зоны катания курортов Бад Хофгастайн и Бад Гастайн', 9, 0, 40),
(16, '3Все зоны катания соединяет между собой только бесплатный лыжный автобус', 9, 0, 30),
(17, 'Хорошо', 10, 0, 10),
(18, 'Так себе', 10, 1, 20),
(19, 'Отлично', 10, 0, 30),
(20, 'Прекрасно', 10, 0, 40),
(21, 'PHP', 11, 1, 10),
(22, 'React JS', 11, 0, 20),
(23, 'HTML', 11, 0, 30),
(24, 'CSS', 11, 0, 40),
(25, 'У бабушек', 12, 0, 10),
(26, 'У дедушек', 12, 0, 20),
(27, 'Лучше их воровать', 12, 0, 30),
(28, 'Самому сушить подсолнухи', 12, 1, 40),
(29, 'На санках', 13, 0, 10),
(30, 'На скейте', 13, 1, 20),
(31, 'На попе', 13, 0, 30),
(32, 'На лыжах', 13, 0, 40),
(33, '1', 14, 1, 10),
(34, '2', 14, 0, 20),
(35, '3', 14, 0, 30),
(36, '4', 14, 0, 40),
(37, 'Из кармана', 15, 0, 20),
(38, 'Из тумбочки', 15, 1, 10),
(39, 'За шкафом', 15, 0, 30),
(40, 'В кастрюле', 15, 0, 40),
(41, 'Ибрагим', 16, 0, 10),
(42, 'Вертолёт', 16, 1, 20),
(43, 'Классика', 16, 0, 30),
(44, 'Асфльт', 16, 0, 40),
(45, 'Ответ 1', 17, 0, 20),
(46, 'Нормалёк', 17, 0, 10),
(47, 'Хорошо', 17, 0, 40),
(48, 'Прекрасно', 17, 1, 30);

-- --------------------------------------------------------

--
-- Table structure for table `quiz_attempts`
--

CREATE TABLE IF NOT EXISTS `quiz_attempts` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `user_id` int(12) NOT NULL,
  `quiz_id` int(12) NOT NULL,
  `data` varchar(123) NOT NULL,
  `data_finish` varchar(123) NOT NULL,
  `date_time_finish` varchar(123) NOT NULL,
  `time_spent` varchar(123) NOT NULL,
  `was_finished` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=155 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quiz_attempts`
--

INSERT INTO `quiz_attempts` (`id`, `user_id`, `quiz_id`, `data`, `data_finish`, `date_time_finish`, `time_spent`, `was_finished`) VALUES
(98, 2, 7, 'December 19, 2019 15:01:00', 'December 19, 2019 15:16:00', 'December 19, 2019 15:01:09', '00:00:09', 1),
(99, 2, 7, 'December 19, 2019 15:13:56', 'December 19, 2019 15:28:56', 'December 19, 2019 15:14:07', '00:00:11', 1),
(100, 2, 7, 'December 19, 2019 15:14:49', 'December 19, 2019 15:29:49', 'December 19, 2019 15:15:00', '00:00:11', 1),
(101, 2, 7, 'December 19, 2019 15:15:56', 'December 19, 2019 15:30:56', 'December 19, 2019 15:16:31', '00:35', 1),
(102, 2, 7, 'December 19, 2019 15:17:46', 'December 19, 2019 15:32:46', 'December 19, 2019 15:18:05', '00:19', 1),
(103, 2, 7, 'December 19, 2019 15:20:51', 'December 19, 2019 15:35:51', 'December 19, 2019 15:26:56', '06:05', 1),
(104, 2, 7, 'December 19, 2019 15:27:44', 'December 19, 2019 15:42:44', 'December 19, 2019 15:28:46', '01:02', 1),
(105, 2, 7, 'December 19, 2019 15:28:48', 'December 19, 2019 15:43:48', 'December 19, 2019 15:28:54', '00:06', 1),
(106, 2, 7, 'December 19, 2019 15:28:59', 'December 19, 2019 15:43:59', 'December 19, 2019 15:29:11', '00:12', 1),
(107, 2, 7, 'December 19, 2019 15:43:05', 'December 19, 2019 15:58:05', 'December 19, 2019 15:43:16', '00:00:31', 1),
(108, 2, 8, 'December 19, 2019 17:27:51', 'December 19, 2019 17:42:51', 'December 19, 2019 17:28:06', '00:15', 1),
(109, 2, 8, 'December 19, 2019 17:28:11', 'December 19, 2019 17:43:11', 'December 19, 2019 17:28:18', '00:07', 1),
(110, 2, 8, 'December 19, 2019 17:32:20', 'December 19, 2019 17:47:20', 'December 19, 2019 17:32:26', '00:00:06', 1),
(111, 2, 9, 'December 19, 2019 17:41:27', 'December 19, 2019 17:56:27', 'December 19, 2019 17:41:43', '00:16', 1),
(112, 2, 9, 'December 19, 2019 17:41:45', 'December 19, 2019 17:56:45', 'December 19, 2019 17:42:03', '00:00:28', 1),
(115, 2, 9, 'December 19, 2019 18:09:25', 'December 19, 2019 18:24:25', 'December 19, 2019 18:09:47', '00:00:22', 1),
(116, 2, 7, 'December 19, 2019 18:15:23', 'December 19, 2019 18:30:23', 'December 19, 2019 18:17:38', '00:02:15', 1),
(117, 2, 8, 'December 19, 2019 18:20:30', 'December 19, 2019 18:35:30', 'December 19, 2019 18:21:19', '00:00:49', 1),
(118, 2, 8, 'December 19, 2019 18:21:42', 'December 19, 2019 18:36:42', 'December 19, 2019 18:21:48', '00:00:06', 1),
(119, 2, 8, 'December 19, 2019 18:22:01', 'December 19, 2019 18:37:01', 'December 19, 2019 18:22:13', '00:00:12', 1),
(120, 2, 8, 'December 19, 2019 18:22:19', 'December 19, 2019 18:37:19', 'December 19, 2019 18:22:24', '00:00:05', 1),
(121, 2, 8, 'December 19, 2019 18:22:33', 'December 19, 2019 18:37:33', 'December 19, 2019 18:22:41', '00:00:08', 1),
(122, 2, 7, 'December 19, 2019 19:29:45', 'December 19, 2019 19:44:45', 'December 19, 2019 19:29:59', '00:00:14', 1),
(123, 2, 7, 'December 19, 2019 19:30:11', 'December 19, 2019 19:45:11', 'December 19, 2019 19:30:25', '00:00:14', 1),
(124, 2, 7, 'December 19, 2019 19:31:14', 'December 19, 2019 19:46:14', 'December 19, 2019 19:31:32', '00:00:18', 1),
(125, 2, 9, 'December 19, 2019 19:31:45', 'December 19, 2019 19:46:45', 'December 19, 2019 19:31:59', '00:00:14', 1),
(126, 2, 9, 'December 19, 2019 22:23:37', 'December 19, 2019 22:38:37', 'December 19, 2019 22:23:52', '00:00:15', 1),
(127, 2, 9, 'December 19, 2019 22:24:28', 'December 19, 2019 22:39:28', 'December 19, 2019 22:27:08', '00:02:40', 1),
(128, 2, 9, 'December 19, 2019 23:39:00', 'December 19, 2019 23:54:00', 'December 19, 2019 23:39:12', '00:00:12', 1),
(129, 7, 7, 'December 19, 2019 23:41:39', 'December 19, 2019 23:56:39', 'December 19, 2019 23:42:09', '00:00:30', 1),
(130, 7, 9, 'December 19, 2019 23:41:40', 'December 19, 2019 23:56:40', 'December 19, 2019 23:41:48', '00:00:08', 1),
(131, 2, 8, 'December 20, 2019 14:43:37', 'December 20, 2019 14:45:37', 'December 20, 2019 14:45:41', '00:02:04', 1),
(132, 2, 7, 'December 21, 2019 19:58:41', 'December 21, 2019 20:13:41', 'December 21, 2019 19:58:55', '00:00:14', 1),
(133, 2, 9, 'December 21, 2019 20:31:42', 'December 21, 2019 20:46:42', 'December 21, 2019 20:31:50', '00:00:08', 1),
(134, 2, 7, 'December 21, 2019 20:36:32', 'December 21, 2019 20:51:32', 'December 21, 2019 20:36:43', '00:00:11', 1),
(135, 2, 8, 'December 21, 2019 20:36:53', 'December 21, 2019 20:51:53', 'December 21, 2019 20:36:59', '00:00:06', 1),
(136, 2, 7, 'December 22, 2019 04:10:01', 'December 22, 2019 04:25:01', 'December 24, 2019 09:55:57', 'Тест был прерван', 1),
(137, 2, 7, 'December 24, 2019 09:54:37', 'December 24, 2019 10:09:37', 'December 24, 2019 09:55:57', '00:01:20', 1),
(138, 2, 7, 'December 24, 2019 09:56:51', 'December 24, 2019 10:11:51', 'December 24, 2019 09:58:45', '00:01:54', 1),
(139, 2, 8, 'December 24, 2019 09:59:08', 'December 24, 2019 10:14:08', 'December 24, 2019 09:59:13', '00:00:05', 1),
(140, 12, 8, 'December 24, 2019 12:01:57', 'December 24, 2019 12:16:57', 'December 24, 2019 12:02:06', '00:00:09', 1),
(141, 12, 8, 'December 24, 2019 12:25:11', 'December 24, 2019 12:40:11', 'December 24, 2019 12:25:34', '00:00:23', 1),
(142, 6, 7, 'December 26, 2019 18:52:36', 'December 26, 2019 19:07:36', 'December 26, 2019 18:52:45', '00:00:09', 1),
(143, 4, 9, 'December 26, 2019 19:13:56', 'December 26, 2019 19:28:56', 'December 26, 2019 19:14:04', '00:00:08', 1),
(144, 2, 7, 'December 29, 2019 16:26:56', 'December 29, 2019 16:41:56', 'December 29, 2019 16:30:02', '00:03:06', 1),
(145, 2, 7, 'December 30, 2019 13:02:45', 'December 30, 2019 13:17:45', 'December 30, 2019 13:03:03', '00:00:18', 1),
(146, 2, 8, 'December 31, 2019 19:13:43', 'December 31, 2019 19:28:43', 'December 31, 2019 19:13:50', '00:00:07', 1),
(147, 2, 7, 'January 1, 2020 19:13:46', 'January 1, 2020 19:28:46', 'January 1, 2020 19:14:15', '00:00:29', 1),
(148, 2, 10, 'January 2, 2020 13:34:18', 'January 2, 2020 13:49:18', 'January 2, 2020 13:34:36', '00:00:18', 1),
(149, 2, 7, 'January 12, 2020 23:04:37', 'January 12, 2020 23:19:37', 'January 12, 2020 23:05:30', '00:00:53', 1),
(150, 6, 7, 'January 20, 2020 11:46:40', 'January 20, 2020 12:01:40', 'January 20, 2020 11:46:54', '00:00:14', 1),
(151, 6, 7, 'January 20, 2020 16:53:43', 'January 20, 2020 17:08:43', 'January 20, 2020 16:54:42', '00:00:59', 1),
(152, 6, 8, 'January 20, 2020 16:59:25', 'January 20, 2020 17:14:25', 'January 20, 2020 16:59:46', '00:00:21', 1),
(153, 14, 7, 'January 20, 2020 21:38:40', 'January 20, 2020 21:53:40', 'January 20, 2020 21:38:51', '00:00:11', 1),
(154, 14, 7, 'January 20, 2020 22:25:36', 'January 20, 2020 22:40:36', 'January 20, 2020 22:26:53', '00:01:17', 1);

-- --------------------------------------------------------

--
-- Table structure for table `quiz_attempts_answers`
--

CREATE TABLE IF NOT EXISTS `quiz_attempts_answers` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `user_id` int(12) NOT NULL,
  `quiz_attempt_id` int(12) NOT NULL,
  `quiz_id` int(12) NOT NULL,
  `question_id` int(12) NOT NULL,
  `answer_id` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=227 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quiz_attempts_answers`
--

INSERT INTO `quiz_attempts_answers` (`id`, `user_id`, `quiz_attempt_id`, `quiz_id`, `question_id`, `answer_id`) VALUES
(25, 2, 78, 7, 8, 11),
(26, 2, 80, 7, 8, 10),
(27, 2, 82, 7, 8, 9),
(28, 2, 82, 7, 9, 13),
(29, 2, 83, 7, 8, 9),
(30, 2, 83, 7, 9, 16),
(31, 2, 88, 7, 8, 10),
(32, 2, 88, 7, 9, 14),
(33, 2, 88, 7, 10, 17),
(34, 2, 89, 7, 8, 9),
(35, 2, 89, 7, 9, 14),
(36, 2, 89, 7, 10, 19),
(37, 2, 90, 7, 8, 9),
(38, 2, 90, 7, 9, 15),
(39, 2, 90, 7, 10, 18),
(40, 2, 91, 7, 8, 10),
(41, 2, 91, 7, 9, 15),
(42, 2, 91, 7, 10, 20),
(43, 2, 92, 7, 8, 9),
(44, 2, 92, 7, 9, 14),
(45, 2, 92, 7, 10, 19),
(46, 2, 93, 7, 8, 9),
(47, 2, 93, 7, 9, 15),
(48, 2, 93, 7, 10, 17),
(49, 2, 94, 7, 8, 9),
(50, 2, 94, 7, 9, 15),
(51, 2, 94, 7, 10, 18),
(52, 2, 95, 7, 8, 9),
(53, 2, 95, 7, 9, 14),
(54, 2, 95, 7, 10, 17),
(55, 2, 95, 7, 10, 17),
(56, 2, 96, 7, 8, 9),
(57, 2, 96, 7, 9, 14),
(58, 2, 96, 7, 10, 20),
(59, 2, 97, 7, 8, 9),
(60, 2, 97, 7, 9, 14),
(61, 2, 97, 7, 10, 20),
(62, 2, 98, 7, 8, 9),
(63, 2, 98, 7, 9, 15),
(64, 2, 98, 7, 10, 19),
(65, 2, 99, 7, 8, 9),
(66, 2, 99, 7, 9, 13),
(67, 2, 99, 7, 10, 17),
(68, 2, 100, 7, 8, 10),
(69, 2, 100, 7, 9, 14),
(70, 2, 100, 7, 10, 17),
(71, 2, 101, 7, 8, 10),
(72, 2, 101, 7, 9, 15),
(73, 2, 101, 7, 10, 20),
(74, 2, 102, 7, 8, 10),
(75, 2, 102, 7, 9, 13),
(76, 2, 102, 7, 10, 18),
(77, 2, 102, 7, 11, 21),
(78, 2, 103, 7, 8, 11),
(79, 2, 103, 7, 9, 16),
(80, 2, 103, 7, 10, 20),
(81, 2, 103, 7, 11, 23),
(82, 2, 104, 7, 8, 10),
(83, 2, 104, 7, 9, 15),
(84, 2, 104, 7, 10, 18),
(85, 2, 104, 7, 11, 21),
(86, 2, 106, 7, 8, 10),
(87, 2, 106, 7, 9, 14),
(88, 2, 106, 7, 10, 18),
(89, 2, 106, 7, 11, 21),
(90, 2, 107, 7, 8, 9),
(91, 2, 107, 7, 9, 14),
(92, 2, 107, 7, 10, 18),
(93, 2, 107, 7, 11, 21),
(94, 2, 108, 8, 12, 28),
(95, 2, 108, 8, 13, 29),
(96, 2, 109, 8, 12, 25),
(97, 2, 109, 8, 13, 31),
(98, 2, 110, 8, 12, 26),
(99, 2, 110, 8, 13, 30),
(100, 2, 111, 9, 14, 34),
(101, 2, 111, 9, 15, 39),
(102, 2, 111, 9, 16, 43),
(103, 2, 112, 9, 14, 34),
(104, 2, 112, 9, 15, 38),
(105, 2, 112, 9, 16, 42),
(106, 2, 115, 9, 14, 34),
(107, 2, 115, 9, 15, 39),
(108, 2, 115, 9, 16, 42),
(109, 2, 116, 7, 8, 10),
(110, 2, 116, 7, 9, 15),
(111, 2, 116, 7, 10, 18),
(112, 2, 116, 7, 11, 21),
(113, 2, 117, 8, 12, 28),
(114, 2, 117, 8, 13, 30),
(115, 2, 118, 8, 12, 26),
(116, 2, 118, 8, 13, 30),
(117, 2, 119, 8, 12, 28),
(118, 2, 119, 8, 13, 30),
(119, 2, 120, 8, 12, 26),
(120, 2, 120, 8, 13, 32),
(121, 2, 121, 8, 12, 28),
(122, 2, 121, 8, 13, 30),
(123, 2, 122, 7, 8, 10),
(124, 2, 122, 7, 9, 14),
(125, 2, 122, 7, 10, 18),
(126, 2, 122, 7, 11, 21),
(127, 2, 123, 7, 8, 10),
(128, 2, 123, 7, 9, 13),
(129, 2, 123, 7, 10, 19),
(130, 2, 123, 7, 11, 23),
(131, 2, 124, 7, 8, 10),
(132, 2, 124, 7, 9, 14),
(133, 2, 124, 7, 10, 18),
(134, 2, 124, 7, 11, 21),
(135, 2, 125, 9, 14, 34),
(136, 2, 125, 9, 15, 37),
(137, 2, 125, 9, 16, 42),
(138, 2, 126, 9, 14, 33),
(139, 2, 126, 9, 15, 38),
(140, 2, 126, 9, 16, 42),
(141, 2, 127, 9, 14, 34),
(142, 2, 127, 9, 15, 39),
(143, 2, 127, 9, 16, 42),
(144, 2, 128, 9, 14, 33),
(145, 2, 128, 9, 15, 37),
(146, 2, 128, 9, 16, 43),
(147, 7, 130, 9, 14, 34),
(148, 7, 130, 9, 15, 39),
(149, 7, 130, 9, 16, 42),
(150, 7, 129, 7, 8, 10),
(151, 7, 129, 7, 9, 16),
(152, 7, 129, 7, 10, 17),
(153, 7, 129, 7, 11, 21),
(154, 2, 131, 8, 12, 28),
(155, 2, 132, 7, 8, 10),
(156, 2, 132, 7, 9, 14),
(157, 2, 132, 7, 10, 20),
(158, 2, 132, 7, 11, 21),
(159, 2, 133, 9, 14, 33),
(160, 2, 133, 9, 15, 38),
(161, 2, 133, 9, 16, 41),
(162, 2, 134, 7, 8, 10),
(163, 2, 134, 7, 9, 14),
(164, 2, 134, 7, 10, 18),
(165, 2, 134, 7, 11, 21),
(166, 2, 135, 8, 12, 25),
(167, 2, 135, 8, 13, 30),
(168, 2, 136, 7, 8, 9),
(169, 2, 137, 7, 8, 10),
(170, 2, 137, 7, 9, 14),
(171, 2, 137, 7, 10, 17),
(172, 2, 137, 7, 11, 21),
(173, 2, 138, 7, 8, 10),
(174, 2, 138, 7, 9, 14),
(175, 2, 138, 7, 10, 18),
(176, 2, 138, 7, 11, 22),
(177, 2, 139, 8, 12, 25),
(178, 2, 139, 8, 13, 29),
(179, 12, 140, 8, 12, 26),
(180, 12, 140, 8, 13, 30),
(181, 12, 141, 8, 12, 28),
(182, 12, 141, 8, 13, 30),
(183, 6, 142, 7, 8, 10),
(184, 6, 142, 7, 9, 14),
(185, 6, 142, 7, 10, 18),
(186, 6, 142, 7, 11, 21),
(187, 4, 143, 9, 14, 34),
(188, 4, 143, 9, 15, 37),
(189, 4, 143, 9, 16, 42),
(190, 2, 144, 7, 8, 10),
(191, 2, 144, 7, 9, 14),
(192, 2, 144, 7, 10, 18),
(193, 2, 144, 7, 11, 21),
(194, 2, 145, 7, 8, 10),
(195, 2, 145, 7, 9, 14),
(196, 2, 145, 7, 10, 18),
(197, 2, 145, 7, 11, 21),
(198, 2, 146, 8, 12, 27),
(199, 2, 146, 8, 13, 30),
(200, 2, 147, 7, 8, 10),
(201, 2, 147, 7, 9, 14),
(202, 2, 147, 7, 10, 18),
(203, 2, 147, 7, 11, 21),
(204, 2, 148, 10, 17, 48),
(205, 2, 149, 7, 8, 10),
(206, 2, 149, 7, 9, 13),
(207, 2, 149, 7, 10, 18),
(208, 2, 149, 7, 11, 22),
(209, 6, 150, 7, 8, 10),
(210, 6, 150, 7, 9, 16),
(211, 6, 150, 7, 10, 20),
(212, 6, 150, 7, 11, 22),
(213, 6, 151, 7, 8, 9),
(214, 6, 151, 7, 9, 15),
(215, 6, 151, 7, 10, 20),
(216, 6, 151, 7, 11, 24),
(217, 6, 152, 8, 12, 25),
(218, 6, 152, 8, 13, 32),
(219, 14, 153, 7, 8, 10),
(220, 14, 153, 7, 9, 14),
(221, 14, 153, 7, 10, 20),
(222, 14, 153, 7, 11, 22),
(223, 14, 154, 7, 8, 9),
(224, 14, 154, 7, 9, 14),
(225, 14, 154, 7, 10, 18),
(226, 14, 154, 7, 11, 21);

-- --------------------------------------------------------

--
-- Table structure for table `quiz_questions`
--

CREATE TABLE IF NOT EXISTS `quiz_questions` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `question` varchar(1234) NOT NULL,
  `quiz_id` int(12) NOT NULL,
  `priority` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quiz_questions`
--

INSERT INTO `quiz_questions` (`id`, `question`, `quiz_id`, `priority`) VALUES
(1, 'Сколько стоит самый дорогой номер в самом дорогом отеле?', 3, 20),
(2, 'Как жить хорошо?', 3, 30),
(3, 'На чем можно передвигаться  здесь дешево?', 3, 10),
(5, 'Где лучше покупать лыжи?', 5, 40),
(6, 'Сколько стоит отдых на неделю?', 5, 50),
(8, 'Вопрос 1', 7, 60),
(9, 'Какие зоны катания между собой соединены системой канатов? 123', 7, 70),
(10, 'Как дела?', 7, 80),
(11, 'Какой самый лучший язык программирования?', 7, 90),
(12, 'Где лучше покупать семечки?', 8, 100),
(13, 'Как лучше кататься с горки?', 8, 110),
(14, 'Сколько можно, если нельзя столько же?', 9, 120),
(15, 'Откуда брать деньги, если у меня есть миллион?', 9, 130),
(16, 'Колбаса или контрабас? ', 9, 140),
(17, 'Как дела?', 10, 150);

-- --------------------------------------------------------

--
-- Table structure for table `regions`
--

CREATE TABLE IF NOT EXISTS `regions` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `name` varchar(234) NOT NULL,
  `description` varchar(2345) NOT NULL,
  `country_id` int(12) NOT NULL,
  `priority` int(12) NOT NULL,
  `region_image` varchar(123) NOT NULL,
  `page_title` varchar(123) NOT NULL,
  `main_block_left` text NOT NULL,
  `main_block_right` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `regions`
--

INSERT INTO `regions` (`id`, `name`, `description`, `country_id`, `priority`, `region_image`, `page_title`, `main_block_left`, `main_block_right`) VALUES
(4, 'Три долины', '', 1, 190, '774ccb6881a889f39b04bd332ea0de0b.jpg', 'Три долины', '<p>Описание этого региона</p>\r\n', ''),
(10, 'Земля Зальцбург', '', 4, 160, 'c599b3a9bcdefd06ae49994ad3009d6f.jpg', 'перевалочный пункт для горнолыжников', '<p>Зальцбург &mdash; старинный европейский городок. В состав Австрии он вошел по историческим меркам недавно,<br />\r\nи это наложило отпечаток на архитектуру, культуру и даже кулинарные традиции. Туристов сюда привлекают узкие улочки города и затейливые барочные фасады древних зданий, особенно живописные с вершины Фестунгберг, к которой ведет канатная дорога. Наряду с привычными музеями и галереями, тут есть несколько интерактивных экспозиций.</p>\r\n', '<p>Зимой Зальцбург становится отправной точкой для лыжников, стремящихся на курорты австрийских Альп.</p>\r\n\r\n<p>Своего рода символ города &mdash; знаменитый на весь мир композитор Вольфганг Амадей Моцарт. Место, где он родился, &mdash; одна из главных туристических достопримечательностей. Именем музыканта-виртуоза названы не только улицы, площади и выставки, но и многочисленные кофейни, магазины, конфеты и даже ликер.</p>\r\n'),
(11, 'Земля Тироль.', '', 4, 150, 'b716ff4ec3f83e4e21c6ef1b3998b140.jpg', 'Долина Пацнаун', '<p>Австрийский Тироль вот уже полвека как превратился в&nbsp;почти сплошную зону катания, объединенную километрами трасс и&nbsp;десятками подъемников. Однако находчивые австрийцы каждый год изобретают, чем в&nbsp;новом сезоне удивить гостей. Буйной ночной жизнью горнолыжные курорты давно никого не&nbsp;удивляют. Однако Ишгль, благодаря скоплению магазинов самых именитых брендов и&nbsp;ресторанов высокой кухни на&nbsp;центральной улице, концертам, в&nbsp;которых считают за&nbsp;честь принять участие &laquo;самые-самые&raquo; звезды современной музыки, с&nbsp;каждым годом нагоняет своего именитого конкурента из&nbsp;французской части Альп.<br />\r\n&nbsp;</p>\r\n', '<p>Горнолыжные курорты в&nbsp;Пацнауне формировались вокруг деревушек с&nbsp;населением 600&minus;700 человек. Что сравнимо с&nbsp;украинскими селами наподобие Поляницы близ Буковеля. Даже &laquo;столица&raquo; долины&nbsp;&mdash; Ишгль&nbsp;&mdash; не&nbsp;слишком многочисленна: всего полторы тысячи жителей. Высоты, на&nbsp;которых расположены села в&nbsp;тирольских долинах (а&nbsp;значит, и&nbsp;нижние станции подъемников),&nbsp;&mdash; 1300&minus;1600&nbsp;м. Верхние&nbsp;же станции забираются на&nbsp;высоты далеко за&nbsp;отметку 2000&minus;2500 м.<br />\r\n&nbsp;</p>\r\n'),
(15, 'Эвазьон МонБлан', '', 1, 140, '8dd73fcafffd403cef700dca88b6c91a.jpg', '555555555', '<p>Эвазьон МонБлан делит Зальцбург на два района &ndash; левобережный пешеходный Старый город, который славится зданиями, возведенными в Средние века и в эпоху барокко, и правобережный Новый город, построенный в XIX веке</p>\r\n', '<p>777777777</p>\r\n'),
(19, 'Ещё один курорт', '', 4, 200, 'd371053b104848cd280ba5d1a4d97ed5.jpg', 'Это заголовок страницы', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>\r\n', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `region_carousel_images`
--

CREATE TABLE IF NOT EXISTS `region_carousel_images` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `image_name` varchar(123) NOT NULL,
  `region_id` int(12) NOT NULL,
  `priority` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `region_carousel_images`
--

INSERT INTO `region_carousel_images` (`id`, `image_name`, `region_id`, `priority`) VALUES
(4, 'd3de3e4230c774c714df663e16e4fe6e.jpg', 15, 2),
(5, '78398dea6f75cd3baa16f6b1f94917c8.jpg', 15, 1),
(6, '636aab2c748c1ff7b78b3e70e01bb327.jpg', 15, 0),
(10, '455236d6dcbeefaefd254f854ad63927.jpg', 15, 0),
(11, 'e269c8ec66fd34a87644c3dfb4cf87fd.jpg', 15, 1),
(12, 'e228d46a72fa6696e8f599fb0b4005b8.jpg', 15, 2),
(16, '6ba15843f339c3bffb52dcd6fb7b8549.jpg', 18, 1),
(17, '052eaaeb03b7d7607c7d3ac83fb9267e.jpg', 18, 2),
(18, 'e19f24dd2e0fb941c0597a31ad52bcfa.jpg', 18, 3),
(19, 'd8fd747672442a604c131caf6a92d940.jpg', 18, 4),
(20, '4ec5ee7d9605011777ebcba6afba07ee.jpg', 18, 5),
(21, 'aaafb3dac72a07165ead6d73d90e7f39.jpg', 11, 0),
(22, 'c587a8878cda9b119643d7e8bde570b2.jpg', 11, 1),
(23, '0f95cd1cd1ac323d3421d1c22336a362.jpg', 11, 2),
(24, '5098068e9c6ab66225f4ea847754ac86.jpg', 11, 3),
(25, '1ffefeb57092c1a66c7ed350915fd022.jpg', 11, 3);

-- --------------------------------------------------------

--
-- Table structure for table `region_slides`
--

CREATE TABLE IF NOT EXISTS `region_slides` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `region_id` int(12) NOT NULL,
  `title` varchar(123) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(123) NOT NULL,
  `priority` int(12) NOT NULL,
  `image_position` varchar(123) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `region_slides`
--

INSERT INTO `region_slides` (`id`, `region_id`, `title`, `description`, `image`, `priority`, `image_position`) VALUES
(1, 15, 'рождество в зальцбурге', '<p>В Рождество в Зальцбурге открываются Рождественские рынки, где вы можете попробовать традиционные местные блюда &mdash; горячий картофель, пончики с травами, и обязательно конфеты, а также приобрести елочные украшения, изделия местных ремесленников и зимнюю одежду.</p>\r\n\r\n<p>Старинный Зальцбург (Salzburg) в зимнее время романтичен и тих. Сказочные улицы, покрытые снегом, выглядят словно театральные декорации к традиционным яслям, адвентским рынкам и вездесущей рождественской музыке.</p>\r\n\r\n<p>Совместите зимний праздник в Зальцбурге с поездкой в горы. Бесплатный лыжный трансфер доставит вас с комфортом в международную лыжную зону &laquo;Snow Space Salzburg&raquo;. Шаттл-бусы будут курсировать Зальцбург &mdash; Флахау ежедневно &ndash; с 21 декабря 2019 г. по 23 марта 2020 г., в сопровождении опытных гидов. Гиды организуют на месте приобретение ски-пассов и прокат инвентаря. Лыжников отвезут в область Флахау &ndash; это в 70 км от Зальцбурга. Там расположена зона Ski Amad&eacute;. Протяжённость её лыжных трасс составляет 860 км.</p>\r\n', '9d4cffcae688597abf212aa5e1824ce7.jpg', 10, 'right'),
(3, 15, 'Ski Shuttle', '<p>В зимний сезон с 8 декабря по 2 апреля рейсовые автобусы и шаттлы компаний Fly Ski Shuttle, Orio Shuttle и Flexibus будут курсировать по субботам и воскресеньям из аэропортов Вероны, Бергамо, Милана, Венеции и Тревизо, а также некоторых крупных городов севера Италии на самые известные горнолыжные курорты провинции Трентино.</p>\r\n\r\n<p>Благодаря сотрудничеству с транспортной компанией Orio Shuttle маршрутная сеть еще больше расширилась &ndash; среди аэропортов, откуда быстро и удобно можно попасть на популярные горнолыжные курорты в Доломитах, появился самый крупный аэропорт Милана &ndash; Malpensa.<br />\r\nБронировать поездки нужно заранее в Интернете: www.flyskishuttle.com.</p>\r\n', '1442138c0207500e6ee174dbfefbe6aa.jpg', 20, 'left'),
(4, 15, 'Достопримечательности круглый год', '<p>В Рождество в Зальцбурге открываются Рождественские рынки, где вы можете попробовать традиционные местные блюда &mdash; горячий картофель, пончики с травами, и обязательно конфеты, а также приобрести елочные украшения, изделия местных ремесленников и зимнюю одежду.</p>\r\n\r\n<p>Старинный Зальцбург (Salzburg) в зимнее время романтичен и тих. Сказочные улицы, покрытые снегом, выглядят словно театральные декорации к традиционным яслям, адвентским рынкам и вездесущей рождественской музыке.</p>\r\n\r\n<p>Совместите зимний праздник в Зальцбурге с поездкой в горы. Бесплатный лыжный трансфер доставит вас с комфортом в международную лыжную зону &laquo;Snow Space Salzburg&raquo;. Шаттл-бусы будут курсировать Зальцбург &mdash; Флахау ежедневно &ndash; с 21 декабря 2019 г. по 23 марта 2020 г., в сопровождении опытных гидов. Гиды организуют на месте приобретение ски-пассов и прокат инвентаря. Лыжников отвезут в область Флахау &ndash; это в 70 км от Зальцбурга. Там расположена зона Ski Amad&eacute;. Протяжённость её лыжных трасс составляет 860 км.</p>\r\n', '08ee0164b7d3b8aa949bd40ea27d60fd.jpg', 30, 'right'),
(5, 11, 'рождество в Долине', '<p>Самый обширный и&nbsp;известный горнолыжный курорт в&nbsp;долине Пацнаун расположен над городком Ишгль. Туда лыжников вывозят несколько гигантских гондольных подъемников. Их&nbsp;нижние станции находятся прямо на&nbsp;центральных улицах городка (к&nbsp;тому&nbsp;же, разные районы Ишгля для удобства еще и&nbsp;соединены тоннелем с&nbsp;горизонтальным эскалатором).<br />\r\n<br />\r\nЧитайте больше на: https://liferead.media/travel/gornolyzhnye-kurorty-doliny-pacnaun-avstrijskij-otvet-kurshavelyu.html</p>\r\n', '1969b218b3953d8fd402a4acefddaf72.jpg', 10, ''),
(6, 11, 'Прыгать и кататься', '<p>Почти в&nbsp;верховьях Пацнауна есть еще деревня Гальтюр. Окружающие пейзажи становятся более суровыми даже на&nbsp;дне долины: из-за высокогорья тут меньше лесов. Однако суровость и&nbsp;однообразие обманчивы. Если перевалить через ближайший хребет на&nbsp;гондольном подъемнике, то&nbsp;открывается впечатляющий вид на&nbsp;водохранилище и&nbsp;плотину. А&nbsp;внизу, почти в&nbsp;деревне есть хорошая детская лыжная. Для &laquo;наших&raquo; горнолыжные курорты наподобие Гальтюра интересны, прежде всего, более доступными ценами на&nbsp;жилье. Как и&nbsp;в&nbsp;случае Капля, они здесь на&nbsp;10&minus;15% ниже.</p>\r\n', '4e8fb2fd696ae8c83d2c5e54c3ebac55.jpg', 20, ''),
(7, 11, 'Пешком по Альпам ', '<p>Одно из&nbsp;занятных приключений, которое предлагают горнолыжные курорты, но&nbsp;в&nbsp;большинстве случаев проходящее мимо внимания наших любителей зимы и&nbsp;снега&nbsp;&mdash; поход в&nbsp;горы на&nbsp;снегоступах (50&nbsp;евро). Приспособление представляет собой пластины с&nbsp;зубцами, которые при помощи креплений удерживаются на&nbsp;ногах. Эти штуковины позволяют не&nbsp;только двигаться, не&nbsp;проваливаясь, по&nbsp;глубокому снегу, но&nbsp;и&nbsp;подниматься по&nbsp;обледенелым склонам. Прогулка длительностью 3&minus;4 ч&nbsp;позволяет совсем по&nbsp;другому взглянуть на&nbsp;природу Альп, чем мы&nbsp;вопринимаем ее, сидя в&nbsp;кресле подъемника или на&nbsp;пару минут остановившись на&nbsp;трассе. Впрочем, возможен и&nbsp;более развлекательный вариант. Компания выходит в&nbsp;поход под вечер, в&nbsp;сумерках добирается до&nbsp;горной хижины, где ужинает фондю и&nbsp;прочей горской снедью. Назад&nbsp;же в&nbsp;цивилизацию уставших и&nbsp;разомлевших участников вывозит сверкающий фарами и&nbsp;сигнальными огнями ратрак.<br />\r\n&nbsp;</p>\r\n', 'a1c09ff0250ba6d4159108a363ee8576.jpg', 30, '');

-- --------------------------------------------------------

--
-- Table structure for table `resorts`
--

CREATE TABLE IF NOT EXISTS `resorts` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `name` varchar(234) NOT NULL,
  `description` varchar(2345) NOT NULL,
  `image` varchar(123) NOT NULL,
  `map` varchar(1234) NOT NULL,
  `region_id` int(12) NOT NULL,
  `priority` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `resorts`
--

INSERT INTO `resorts` (`id`, `name`, `description`, `image`, `map`, `region_id`, `priority`) VALUES
(8, 'Курорт Ишгль/Зее/Каппль/Гальтюр', '<p>Горнолыжный курорт Ишгль находится на границе Австрии и Швейцарии. Со швейцарской стороны располагается курорт Самнаун (Samnaun), в который можно попасть &laquo;не снимая лыж&raquo;.</p>\r\n\r\n<p>Входит в состав округа&nbsp;<a href=\"https://ru.wikipedia.org/wiki/%D0%9B%D0%B0%D0%BD%D0%B4%D0%B5%D0%BA_(%D0%BE%D0%BA%D1%80%D1%83%D0%B3)\">Ландек</a>. Население составляет 1489 человек (на 5 августа 2001 года). Занимает площадь 103,3 км&sup2;.&nbsp;<a href=\"https://ru.wikipedia.org/wiki/%D0%98%D0%B4%D0%B5%D0%BD%D1%82%D0%B8%D1%84%D0%B8%D0%BA%D0%B0%D1%86%D0%B8%D0%BE%D0%BD%D0%BD%D1%8B%D0%B9_%D0%BA%D0%BE%D0%B4_%D1%81%D1%83%D0%B1%D1%8A%D0%B5%D0%BA%D1%82%D0%B0_%D1%81%D0%B0%D0%BC%D0%BE%D1%83%D0%BF%D1%80%D0%B0%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D1%8F\">Официальный код</a>&nbsp;&mdash;&nbsp;<em>70608</em>.</p>\r\n', 'cff8eed7b754ed70018fa3a2c0f7c5e2.png', '', 11, 20),
(9, 'Курорт Валь Торанс', '', '', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2806.5056572037734!2d6.579301815831436!3d45.29821025252445!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4789866002267071%3A0x8ec7bc6dc1eef333!2z0JLQsNC70Ywg0KLQvtGA0LDQvdGB!5e0!3m2!1sru!2s!4v1576946822767!5m2!1sru!2s', 4, 50),
(11, 'Курорт Межев', '', '', 'https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d3929.702592367964!2d6.615606711052476!3d45.856915861035986!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1z0LrRg9GA0L7RgNGCINGA0Y_QtNC-0Lwg0YEg0JzQtdC20LXQsiwg0KTRgNCw0L3RhtC40Y8!5e0!3m2!1sru!2s!4v1576945884702!5m2!1sru!2s', 15, 70),
(15, 'город Зальцбург', '', '', '', 10, 90);

-- --------------------------------------------------------

--
-- Table structure for table `resort_images`
--

CREATE TABLE IF NOT EXISTS `resort_images` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `image_name` varchar(123) NOT NULL,
  `resort_id` int(12) NOT NULL,
  `priority` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `resort_images`
--

INSERT INTO `resort_images` (`id`, `image_name`, `resort_id`, `priority`) VALUES
(23, 'b4abfa640e487d6fd7253d34e7b5a67f.jpg', 14, 2),
(22, '3fe144c609a07a1df1eac3fc15eecd05.jpg', 14, 1),
(21, '8be38792cce0e46f73c42669bf6ce9cb.jpg', 8, 2),
(20, '4051ef34aa3138d26899017c7ce046a8.jpg', 8, 0),
(19, 'e83eecb893f9b206a8fa14798b0dfc23.jpg', 8, 1);

-- --------------------------------------------------------

--
-- Table structure for table `resort_slides`
--

CREATE TABLE IF NOT EXISTS `resort_slides` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `resort_id` int(12) NOT NULL,
  `title` varchar(123) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(123) NOT NULL,
  `priority` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `resort_slides`
--

INSERT INTO `resort_slides` (`id`, `resort_id`, `title`, `description`, `image`, `priority`) VALUES
(1, 1, '55', '<p>yuyu</p>\r\n', '597f90723b6afa12d9e4a323b2ff3442.jpg', 10),
(2, 3, 'еееееееееее1', '<p>нннннннннннн2</p>\r\n', '28f342ea0913c961921fce3e89a1a791.png', 20),
(3, 3, '111', '<p>222</p>\r\n', '2215e6b16477c4ce67b3b75a3623f58c.jpg', 10),
(4, 5, '123', '<p>123</p>\r\n', '', 30),
(5, 5, 'dsfdfg', '<p>dfgfdg</p>\r\n', '', 10),
(6, 5, '4444', '<p>4444</p>\r\n', '25006efa23ce75bd73f7b615b8cebba8.png', 20);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(123) NOT NULL AUTO_INCREMENT,
  `user_name` text NOT NULL,
  `user_sname` varchar(123) NOT NULL,
  `user_email` varchar(1234) NOT NULL,
  `user_password` varchar(123) NOT NULL,
  `user_gender` varchar(123) NOT NULL,
  `user_status` varchar(123) NOT NULL DEFAULT 'user',
  `activate` int(12) NOT NULL DEFAULT '0',
  `activateCode` varchar(123) NOT NULL,
  `fb_id` varchar(123) NOT NULL DEFAULT 'empty',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `user_sname`, `user_email`, `user_password`, `user_gender`, `user_status`, `activate`, `activateCode`, `fb_id`) VALUES
(1, 'admin', '', 'admin@admin.com', 'skiadmin', '', 'admin', 1, '', ''),
(2, 'Сергей', 'Викторович', 'viktorovich87@yandex.ru', '123123123', '', 'user', 0, '', 'empty'),
(3, 'Geka', 'Geka', 'jobmailhtml@gmail.com', 'jobmailhtml', '', 'user', 0, '', 'empty'),
(4, 'Maxon', 'Stipaska', 'example@example.com', 'zaq12345`!', '', 'user', 0, '', 'empty'),
(6, 'Крис', 'Олейникова', 'aeroporter@gmail.com', '20ылшфвьшт20', '', 'user', 0, '', 'empty'),
(13, 'Татьяна', 'Чемодурова', 'ski@goodtimetravel.ru', 'abcd1234', '', 'user', 0, '', 'empty'),
(14, 'gttt', 'yyy', 'sinser300@gmai.com', '5656565656', '', 'user', 0, '', 'empty'),
(15, 'Maxon4ik', 'PussyMagnet', 'mhoromanschii@netinfo.md', '123456789', '', 'user', 0, '', 'empty'),
(16, 'qqq', 'www', 'www@www.www', 'wwwwwwww', '', 'user', 0, '', 'empty'),
(17, 'Ttt', 'Ttt', 'ttt@ttt.ttt', 'tttttttt', '', 'user', 0, '', 'empty');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

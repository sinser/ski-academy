$(document).ready(function () {
    URL = $('#URL').val();
    function checkEmail (email) {
        return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)
      }

    $("#reg-btn").click( function() { // click on registration button

        $('#error-message').text("").removeClass('show');
        name = $('#name').val();
        sname = $('#sname').val();
        email = $('#email').val();
        password = $('#password').val();
        spassword = $('#spassword').val();

        $('#register-popup .spinner-content').fadeIn('slow');
        $('#register-popup .spinner-content').addClass('show-spinner');
        setTimeout(checkRegData, 2000);

            function checkRegData() {
                if(name == "" || sname == "" || email == "" || password == "" || spassword == "" ){
                    $("#register-popup .error-message").text("Некоторые поля остались пустыми").addClass('show');
                    $("#register-popup .spinner-content").fadeOut('slow');
                }else{ 
                    if(password.length < 8){
                            $("#register-popup .error-message").text("Пароль должен быть не менее 8 символов").addClass('show');
                            $("#register-popup .spinner-content").fadeOut('slow');
                        }else{
                            if(password != spassword){
                                $('#register-popup .error-message').text("Пароли не совпадают").addClass('show');
                                $('#register-popup .spinner-content').fadeOut('slow');
                            }else{
                                
                                    if(checkEmail(email)){
                                        jQuery(function () {
                                            $.ajax({
                                            type: "POST", 
                                            url: URL+"Login/registration", 
                                            data: "email="+email+"&name="+name+"&sname="+sname+"&password="+password,  
                                            error: function () { //
                                            
                                            },
                                            success: function (data) {
                                                console.log(data);
                                                        if(data!="ok"){
                                                            $('#register-popup .error-message').text("Такой email уже существует").addClass('show');
                                                            $('#register-popup .spinner-content').fadeOut('slow');
                                                        }else{
                                                            location.reload();
                                                        }
                                                } 
                                            });
                                        });
                                    }else{
                                        $('#register-popup .error-message').text("Формат email не верный").addClass('show');
                                        $('#register-popup .spinner-content').fadeOut('slow');
                                    }
                                }
                            }
                    }
            }   
    });

    $("#login-btn").click( function() {
        $('#enter-popup .error-message').text("").removeClass('show');
        
        email = $('#login-email').val();
        password = $('#login-password').val();

        $('#enter-popup .spinner-content').fadeIn('slow');
        $('#enter-popup .spinner-content').addClass('show-spinner');
        setTimeout(checkRegData, 2000);

            function checkRegData() {
                if(email == "" || password == ""){
                    $("#enter-popup .error-message").text("Некоторые поля остались пустыми").addClass('show');
                    $("#enter-popup .spinner-content").fadeOut('slow');
                }else{     
                    if(checkEmail(email)){
                        jQuery(function () {
                            $.ajax({
                            type: "POST", 
                            url: URL+"Login/enter", 
                            data: "email="+email+"&password="+password,  
                            error: function () { //
                            
                            },
                            success: function (data) {
								//console.log(data);
                                        if(data == "0"){
                                            $('#enter-popup .error-message').text("Логин и/или пароль неверный").addClass('show');
                                            $('#enter-popup .spinner-content').fadeOut('slow');
                                        }else{
                                            //location.reload();
											location='http://ski-academy.ru/main'; 
                                        }
                                } 
                            });
                        });
                    }else{
                        $('#enter-popup .error-message').text("Формат email не верный").addClass('show');
                        $('#enter-popup .spinner-content').fadeOut('slow');
                    }
                }
                            
                    
            }   
    });
	
	
	$("#resend-btn").click( function() {
        $('#popup-forget-form .error-message-forget').text("").removeClass('show');
        
        email = $('#login-email-forget').val();

        $('#enter-popup .spinner-content').fadeIn('slow');
        $('#enter-popup .spinner-content').addClass('show-spinner');
        setTimeout(checkRegData, 2000);

            function checkRegData() {
                if(email == "" ){
                    $("#enter-popup .error-message-forget").text("Некоторые поля остались пустыми").addClass('show');
                    $("#enter-popup .spinner-content").fadeOut('slow');
                }else{     
                    if(checkEmail(email)){
                        jQuery(function () {
                            $.ajax({
                            type: "POST", 
                            url: URL+"Login/forget", 
                            data: "email="+email,  
                            error: function () { //
                            
                            },
                            success: function (data) {
                                        if(data == "0"){
                                            $('#enter-popup .error-message-forget').text("Такого емайла нет в базе").addClass('show');
                                            $('#enter-popup .spinner-content').fadeOut('slow');
                                        }else{
											$('#enter-popup .error-message-forget').text("На ваш почтовый адрес было выслано письмо с настройками доступа").addClass('show');
											$('#enter-popup .spinner-content').fadeOut('slow');
                                        }
                                } 
                            });
                        });
                    }else{
                        $('#enter-popup .error-message-forget').text("Формат email не верный").addClass('show');
                        $('#enter-popup .spinner-content').fadeOut('slow');
                    }
                }
                            
                    
            }   
    });
	
	
	$(".quiz-button").click( function() {
		$('.quiz-button').removeClass('active');
		$(this).addClass('active');
		$('.btn-primary').removeClass('disabled').addClass('can-next');
	});
	
	$("#forget").click( function() {
		$('#popup-login-form').addClass('hide');
		$('#popup-forget-form').addClass('show');
	});
	
	$("#forget-back").click( function() {
		$('#popup-login-form').removeClass('hide');
		$('#popup-forget-form').removeClass('show');
	});
	
	
	
	$(".btn-primary.next-btn").click( function() {
		currentQuestion = $(this).attr('currentQuestion');
		hasClass = $(this).hasClass("can-next");
		if(hasClass){
					$('.spinner-content').fadeIn('slow');
					$('.spinner-content').addClass('show-spinner');
		}
		
		setTimeout(getNextQuestion, 500);
		
			function getNextQuestion() {
				
				if(hasClass){
					$('.btn-primary').addClass('disabled').removeClass('can-next');
					question_id = $('.quiz-button.active').attr('question_id');
					quiz_attempt = $('.quiz-button.active').attr('quiz_attempt');
					answer_id = $('.quiz-button.active').attr('answer_id');
					quiz_id = $('.quiz-button.active').attr('quiz_id');
					
					jQuery(function () {
						$.ajax({
						type: "POST", 
						url: URL+"Main/getNextQuestion", 
						data: "question_id="+question_id+"&quiz_attempt="+quiz_attempt+"&answer_id="+answer_id+"&quiz_id="+quiz_id+"&currentQuestion="+currentQuestion,  
						error: function () { //
						
						},
						success: function (data) {
							if(data != "end"){
								$('.spinner-content').fadeOut('slow');
								//$('#question-abswer-box').html('');
								document.getElementById('question-abswer-box').innerHTML = '';
								$('#question-abswer-box').html(data);
								//$('.btn-primary').removeClass('disabled');
							}
								location.reload();
						} 
						});
				});
			}
		}
	});
	
    
    

});
$( document ).ready(function() {
  $('.choice-slider').slick({
    arrows: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight: false,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          arrows: false
        }
      },
      {
        breakpoint: 767,
        settings: {
          arrows: false,
          centerMode: true
        }
      }
    ]
  });

  $('.text-slider').slick({
    arrows: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight: false,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false
        }
      }
    ]
  });

  $('.fancy-slider').slick({
    arrows: true,
    variableWidth: true,
    slidesToScroll: 1,
    adaptiveHeight: false
  });

  $('.fancybox').fancybox({
    autoScale : false,
    fitToView: false,
    autoSize: false,
    buttons: [
      "close"
    ],
  });

  $(".btn-navi").click(function() {
    $(".navi").toggleClass("active");
	  $(this).toggleClass("active");
	});

  var scene = document.getElementById('scene');
  var parallaxInstance = new Parallax(scene);
});

$(document).mouseup(function (e){ // событие клика по веб-документу
	var div = $(".btn-navi"); // тут указываем class элемента
	if (!div.is(e.target) // если клик был не по нашему блоку
	    && div.has(e.target).length === 0) { // и не по его дочерним элементам
    $(".navi").removeClass("active");
    $(".btn-navi").removeClass("active");
	}
});

$( document ).ready(function() {
  $('.choice-slider').slick({
    arrows: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight: false,
    responsive: [
      {
        breakpoint: 1024,
        centerMode: true,
        settings: {
          arrows: false
        }
      }
    ]
  });

  $('.text-slider').slick({
    arrows: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight: false,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false
        }
      }
    ]
  });

  $('.fancy-slider').slick({
    arrows: true,
    variableWidth: true,
    slidesToScroll: 1,
    adaptiveHeight: false
  });

  $('.fancybox').fancybox({
    autoScale : false,
    fitToView: false,
    autoSize: false,
    buttons: [
      "close"
    ],
  });

  $(".btn-navi").click(function() {
    $(".navi").toggleClass("active");
	  $(this).toggleClass("active");
	});

  var scene = document.getElementById('scene');
  var parallaxInstance = new Parallax(scene);
});
